import 'react-native-gesture-handler';
import {AppRegistry, Text} from 'react-native';
import App from './App';
import {name as appName} from './app.json';
import * as TrackPlayer from 'react-native-track-player';
import bgService from 'app/utils/RemoteControlListener';
if (__DEV__) {
  import('./ReactotronConfig').then(() => console.log('Reactotron Configured'));
}

// config default text - disbale font scaling
Text.defaultProps = Text.defaultProps || {};
Text.defaultProps.allowFontScaling = false;
console.disableYellowBox = true;
AppRegistry.registerComponent(appName, () => App);
TrackPlayer.registerPlaybackService(() => bgService);

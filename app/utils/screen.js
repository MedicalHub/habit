import {Dimensions, Platform, StatusBar} from 'react-native';

const X_WIDTH = 375;
const X_HEIGHT = 812;

const XSMAX_WIDTH = 414; // XR, 11 Pro Max, 11 Pro
const XSMAX_HEIGHT = 896;

export const {height: W_HEIGHT, width: W_WIDTH} = Dimensions.get('window');

export const isIPhoneX = () => {
  let check = false;
  if (Platform.OS === 'ios' && !Platform.isPad && !Platform.isTVOS) {
    check =
      (W_WIDTH === X_WIDTH && W_HEIGHT === X_HEIGHT) ||
      (W_WIDTH === XSMAX_WIDTH && W_HEIGHT === XSMAX_HEIGHT);
  }
  return check;
};

export const getStatusBarHeight = () => {
  if (Platform.OS === 'ios') {
    return isIPhoneX() ? 44 : 20;
  }

  if (Platform.OS === 'android') {
    if (StatusBar.currentHeight) {
      return StatusBar.currentHeight;
    }
    return 54;
  }
};

// screen sizes are based on standard iphone 6 (375 x 667) screen mobile device
const BASE_SCREEN_WIDTH = 375;
const BASE_SCREEN_HEIGHT = 812;

const scale = size => {
  const convertSize = (W_WIDTH / BASE_SCREEN_WIDTH) * size;
  return Math.floor(convertSize);
};

const verticalScale = size => {
  const convertSize = (W_HEIGHT / BASE_SCREEN_HEIGHT) * size;
  return Math.floor(convertSize);
};

const moderateScale = (size, factor = 0.5) => {
  const convertSize = size + (scale(size) - size) * factor;
  return Math.floor(convertSize);
};

export {scale, verticalScale, moderateScale};

export const getMediaCenterButtonPadding = size => {
  const BASE_ICON_SIZE = 24;
  const convertSize = (size / BASE_ICON_SIZE) * 2;
  return convertSize;
};

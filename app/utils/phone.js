import PNF from 'google-libphonenumber';

export const formatPhoneNumber = (
  phone,
  type = PNF.PhoneNumberFormat.NATIONAL,
  withSpace = false,
) => {
  //   const number = PNF.PhoneNumberUtil.getInstance().parse(phone, 'VN');
  //   let formattedPhoneNumber = PNF.PhoneNumberUtil.getInstance().format(
  //     number,
  //     type,
  //   );

  //   if (!withSpace) {
  //     formattedPhoneNumber = formattedPhoneNumber.replace(/ /g, '');
  //   }
  let formattedPhoneNumber = String(phone);
  if (formattedPhoneNumber.length > 0 && formattedPhoneNumber[0] != 0) {
    formattedPhoneNumber = `0${formattedPhoneNumber}`;
  }
  return formattedPhoneNumber;
};

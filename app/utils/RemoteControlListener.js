import TrackPlayer from 'react-native-track-player';
import {store} from 'app/store';
import reactotron from 'reactotron-react-native';
import {setCurrentTrack} from 'app/actions/playback';
let flag = false;

export async function backgroundPlayback(track) {
  if (flag) {
    return;
  }
  flag = true;
  setTimeout(() => (flag = false), 250);
  await TrackPlayer.pause();
  await TrackPlayer.skip(track.id);
  TrackPlayer.play();
  store.dispatch({type: 'current_track', payload: track});
  store.dispatch({type: 'set_playback', payload: true});
}

module.exports = async function() {
  console.log('remote');
  TrackPlayer.addEventListener('remote-play', () => {
    console.log('xxxxxx');
    TrackPlayer.play();
    store.dispatch({type: 'set_playback', payload: true});
  });

  TrackPlayer.addEventListener('remote-pause', () => {
    TrackPlayer.pause();
    store.dispatch({type: 'set_playback', payload: false});
  });

  TrackPlayer.addEventListener('remote-next', () => {
    let indexCurrentTrack = store.getState().audio.playback.currentTrack.index;
    let data = store.getState().item_topic.dataAudio;
    let nextTrack = data[indexCurrentTrack + 1];
    if (!nextTrack) {
      return;
    }
    if (nextTrack.is_locked) {
      return;
    }
    store.dispatch(setCurrentTrack(nextTrack));
  });

  TrackPlayer.addEventListener('remote-previous', () => {
    let indexCurrentTrack = store.getState().audio.playback.currentTrack.index;
    let data = store.getState().item_topic.dataAudio;
    let nextTrack = data[indexCurrentTrack - 1];
    reactotron.log('nextTrack', nextTrack);
    if (nextTrack) {
      // TrackPlayer.skipToPrevious();
      // store.dispatch({type: 'current_track', payload: nextTrack});
      // store.dispatch({type: 'set_playback', payload: true});
      store.dispatch(setCurrentTrack(nextTrack));
    }
  });

  TrackPlayer.addEventListener('playback-queue-ended', ({position}) => {
    TrackPlayer.pause();
  });
};

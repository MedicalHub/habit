export default class ModalHolder {
  static modal;
  static setModal(modal) {
    this.modal = modal;
  }

  static show(data) {
    this.modal.show(data);
  }
  static hide() {
    this.modal.hide();
  }
}

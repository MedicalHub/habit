export const color = {
  primary: '#2962FF',
  secondary: '#FF5722',
};

export const fontFamily = {
  default: 'Roboto',
  defaultMedium: 'Roboto-Medium',
  defaultBold: 'Roboto-Bold',
};

export default class AlertHolder {
  static alert;
  static setAlert(alert) {
    this.alert = alert;
  }

  static show(data) {
    this.alert.show(data);
  }
  static hide() {
    this.alert.hide();
  }
}

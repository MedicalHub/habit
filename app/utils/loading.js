export default class LoadingHolder {
  static loading;
  static setLoading(loading) {
    this.loading = loading;
  }

  static start() {
    this.loading.start();
  }
  static stop() {
    this.loading.stop();
  }
}

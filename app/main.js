import React from 'react';
import {StyleSheet} from 'react-native';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {injectIntl} from 'react-intl';
import DropdownAlert from 'react-native-dropdownalert';
import {PersistGate} from 'redux-persist/integration/react';

import AppNavigator from 'app/navigation';
import NavigationService from 'app/navigation/NavigationService';

import DropDownHolder from 'app/utils/dropdownelert';
import Loading from 'app/components/loading';
import LoadingHolder from 'app/utils/loading';

import {persistor} from 'app/store';
import {handleIgnoreAction} from 'app/utils/navigation';
import {determineNavigator} from 'app/actions/navigation';

class Main extends React.PureComponent {
  getActiveRouteName = navigationState => {
    if (!navigationState) {
      return null;
    }
    const route = navigationState.routes[navigationState.index];

    // dive into nested navigators
    if (route.routes) {
      return this.getActiveRouteName(route);
    }
    return route.routeName;
  };
  onNavigationStateChange = async (prevState, currentState, action) => {
    const currentScreen = this.getActiveRouteName(currentState);
    const prevScreen = this.getActiveRouteName(prevState);

    // handleIgnoreAction(this.props.dispatch, action);

    // TODO: store in redux:
    if (prevScreen !== currentScreen) {
      this.props.actions.determineNavigator(prevScreen, currentScreen, action);
    }
  };

  render() {
    const {intl, navigator} = this.props;
    return (
      <PersistGate persistor={persistor} loading={null}>
        <AppNavigator
          screenProps={{intl, navigator}}
          ref={navigatorRef => {
            NavigationService.setTopLevelNavigator(navigatorRef);
          }}
          onNavigationStateChange={this.onNavigationStateChange}
        />
        <DropdownAlert
          ref={ref => DropDownHolder.setDropDown(ref)}
          closeInterval={1000}
          updateStatusBar={false}
          titleStyle={styles.titleStyle}
          titleNumOfLines={2}
        />
        <Loading
          refs={ref => {
            LoadingHolder.setLoading(ref);
          }}
        />
      </PersistGate>
    );
  }
}

const mapStateToProps = state => {
  return {
    navigator: state.navigator,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        determineNavigator,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(injectIntl(Main));

const styles = StyleSheet.create({
  titleStyle: {
    fontWeight: '500',
    color: '#FFF',
  },
});

import {put, takeLatest, call} from 'redux-saga/effects';
import API from 'app/saga/api';
import {TopicTypes} from 'app/action_types';

function* getTopicByCategory(action) {
  try {
    const {result, error} = yield call(
      API.getTopicByCategory,
      action.categoryId,
    );
    if (error) {
      yield put({
        type: TopicTypes.GET_TOPIC_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: TopicTypes.GET_TOPIC_FAILURE,
          error: result.errors,
        });
      } else {
        yield put({
          type: TopicTypes.GET_TOPIC_SUCCESS,
          data: result,
        });
      }
    }
  } catch (error) {
    yield put({
      type: TopicTypes.GET_TOPIC_FAILURE,
      error: error.message,
    });
  }
}

function* watchGetTopicByCategory() {
  yield takeLatest(TopicTypes.GET_TOPIC_REQUEST, getTopicByCategory);
}

export {watchGetTopicByCategory};

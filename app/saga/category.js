import {put, takeLatest, call} from 'redux-saga/effects';
import API from 'app/saga/api';
import {CategoryTypes} from 'app/action_types';

function* getAllCategory(action) {
  try {
    const {result, error} = yield call(API.getAllCategory, action.userId);
    if (error) {
      yield put({
        type: CategoryTypes.GET_ALL_CATEGORY_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: CategoryTypes.GET_ALL_CATEGORY_FAILURE,
          error: result.errors,
        });
      } else {
        yield put({
          type: CategoryTypes.GET_ALL_CATEGORY_SUCCESS,
          data: result,
        });
      }
    }
  } catch (error) {
    yield put({
      type: CategoryTypes.GET_ALL_CATEGORY_FAILURE,
      error: error.message,
    });
  }
}

function* watchGetAllCategory() {
  yield takeLatest(CategoryTypes.GET_ALL_CATEGORY_REQUEST, getAllCategory);
}

export {watchGetAllCategory};

import {all} from 'redux-saga/effects';
import {
  watchLogin,
  watchLoginWithSocial,
  watchRegister,
  watchGetProfileInfo,
  watchUpdateProfile,
  watchUpdatePassword,
  watchResetPassword,
  watchGetStatistics,
} from 'app/saga/authentication';
import {watchGetAllCategory} from 'app/saga/category';
import {watchGetTopicByCategory} from 'app/saga/topic';
import {watchGetItemTopic} from 'app/saga/item_category';

export default function* rootSaga() {
  yield all([
    watchLogin(),
    watchLoginWithSocial(),
    watchRegister(),
    watchGetProfileInfo(),
    watchUpdateProfile(),
    watchUpdatePassword(),
    watchGetAllCategory(),
    watchGetTopicByCategory(),
    watchGetItemTopic(),
    watchResetPassword(),
    watchGetStatistics(),
  ]);
}

import Config from 'react-native-config';
import reactotron from 'reactotron-react-native';

const API_URL = Config.API_URL || 'https://habit.mohinhtuduy.com';
const API_ORDER_URL = Config.API_ORDER_URL || 'https://api.mohinhtuduy.com';

class API {
  constructor() {
    this.token = '';
  }

  urlImage = thumbnail => `https://habitmedia.s3.amazonaws.com/${thumbnail}`;
  getToken = () => {
    return this.token;
  };

  setToken = token => {
    this.token = token;
  };

  callApi = async (endpoint, method, data) => {
    //   const params = {
    //     url: `${Config.API_URL}/${endpoint}`,
    //     method: method,
    //     headers: {'content-type': 'application/x-www-form-urlencoded'},
    //     data: qs.stringify(data),
    //   };
    //   if (endpoint === 'user/login') {
    //     params.headers = {'content-type': 'application/x-www-form-urlencoded'};
    //     params.data = qs.stringify(data);
    //   }
    //   return axios(params);
    // return axios.post(params.url, params.body);

    const URL = endpoint.includes('http') ? endpoint : `${API_URL}/${endpoint}`;
    var requestOptions = {
      method: method,
      headers: {
        Aceppt: 'Content-Type',
        'Content-Type': 'application/json',
      },
    };
    if (this.token) {
      requestOptions.headers['x-access-token'] = this.token;
    }
    if (data) {
      requestOptions.body = JSON.stringify(data);
    }
    // console.log('requestOptions', requestOptions);

    const res = await fetch(URL, requestOptions);
    // console.log('res-text', await res);
    if (res.ok) {
      return await res.json();
    } else if (!res.ok) {
      const resp = await res.json();
      const response = {
        error: true,
        status: res.status,
      };
      if (resp.errors) {
        response.errors = [...resp.errors];
      } else {
        response.errors = [`${res.status}`];
      }
      // console.log('response', response);
      return response;
    }

    return await res.text();
  };

  login = async data => {
    try {
      const result = await this.callApi('user/login', 'POST', data);
      return {result};
    } catch (error) {
      console.log('error-login', error.message);
      return {error};
    }
  };

  loginWithSocial = async data => {
    try {
      const result = await this.callApi('user/login-social', 'POST', data);
      return {result};
    } catch (error) {
      console.log('error-login-social', error.message);
      return {error};
    }
  };

  register = async data => {
    try {
      const result = await this.callApi('user/register', 'POST', data);
      return {result};
    } catch (error) {
      console.log('error-register', error.message);
      return {error};
    }
  };
  getProfileInfo = async userId => {
    try {
      const result = await this.callApi(`user/detail/${userId}`, 'GET');
      return {result};
    } catch (error) {
      console.log('error-get-profile-info', error.message);
      return {error};
    }
  };

  updateProfile = async data => {
    try {
      const result = await this.callApi('user/update', 'PUT', data);
      return {result};
    } catch (error) {
      console.log('error-update', error.message);
      return {error};
    }
  };
  updatePassword = async data => {
    try {
      const result = await this.callApi('user/update-password', 'POST', data);
      return {result};
    } catch (error) {
      console.log('error-update-password', error.message);
      return {error};
    }
  };

  getAllCategory = async userId => {
    try {
      const result = await this.callApi(
        `category/get-by-user/${userId}`,
        'GET',
      );
      return {result};
    } catch (error) {
      console.log('error-get-all-category', error.message);
      return {error};
    }
  };

  getTopicByCategory = async category_id => {
    try {
      const result = await this.callApi(
        `category/get-topics/${category_id}`,
        'GET',
      );
      return {result};
    } catch (error) {
      console.log('error-get-topic-by-category', error.message);
      return {error};
    }
  };

  getItemTopic = async (topic_id, userId) => {
    try {
      const result = await this.callApi(
        `topic/get-items-by-user/${topic_id}/${userId}`,
        'GET',
      );
      let dataAudio = [];
      for (let i = 0; i < result.length; i++) {
        const {
          audio,
          title,
          description,
          thumbnail,
          _id,
          time,
          is_locked,
        } = result[i];
        if (!is_locked) {
          const url = 'https://habitmedia.s3.amazonaws.com/' + audio;
          dataAudio.push({
            id: _id,
            url: encodeURI(url),
            title: title,
            artist: description,
            artwork: `https://habitmedia.s3.amazonaws.com/${thumbnail}`,
            duration: time,
            index: i,
            ...result[i],
          });
        }
      }
      return {
        result,
        dataAudio,
      };
    } catch (error) {
      console.log('error-get-item-topic', error.message);
      return {error};
    }
  };
  // reset password
  resetPassword = async data => {
    const endpoint = 'user/reset-password';
    try {
      const result = await this.callApi(endpoint, 'POST', data);
      return {result};
    } catch (error) {
      console.log('error-reset-password', error.message);
      return {error};
    }
  };

  //update progress

  updateProgress = async data => {
    const endpoint = 'progress/add';
    try {
      const result = await this.callApi(endpoint, 'POST', data);
      return {result};
    } catch (error) {
      console.log('error-reset-password', error.message);
      return {error};
    }
  };

  // statistics
  getStatistics = async userId => {
    const endpoint = `progress/statistics/${userId}`;
    try {
      const result = await this.callApi(endpoint, 'GET');
      return {result};
    } catch (error) {
      console.log('get-statistics', error.message);
      return {error};
    }
  };

  addOrder = async ({fullname, email, address, phone}) => {
    const endpoint = `${API_ORDER_URL}/api/addOrder`;
    try {
      const result = await this.callApi(endpoint, 'POST', {
        fullname,
        email,
        address,
        phone,
      });
      return {result};
    } catch (error) {
      console.log('addOrder', error.message);
      return {error};
    }
  };

  scanQrCode = async code => {
    const endpoint = `${API_ORDER_URL}/api/checkCode`;
    try {
      const result = await this.callApi(endpoint, 'POST', {
        code,
      });
      return {result};
    } catch (error) {
      console.log('checkCode', error.message);
      return {error};
    }
  };

  activeAccount = async ({code, userId}) => {
    const endpoint = 'user/active-code';
    try {
      const result = await this.callApi(endpoint, 'POST', {
        code,
        user_id: userId,
      });
      return {result};
    } catch (error) {
      console.log('checkCode', error.message);
      return {error};
    }
  };
}

export default new API();

import Config from 'react-native-config';
// import axios from 'axios';
// import qs from 'qs';

export default async function callApi(endpoint, method, data) {
  //   const params = {
  //     url: `${Config.API_URL}/${endpoint}`,
  //     method: method,
  //     headers: {'content-type': 'application/x-www-form-urlencoded'},
  //     data: qs.stringify(data),
  //   };
  //   if (endpoint === 'user/login') {
  //     params.headers = {'content-type': 'application/x-www-form-urlencoded'};
  //     params.data = qs.stringify(data);
  //   }
  //   return axios(params);
  // return axios.post(params.url, params.body);

  const URL = `${Config.API_URL}/${endpoint}`;
  var requestOptions = {
    method: 'POST',
    headers: {
      Aceppt: 'Content-Type',
      'Content-Type': 'application/json',
    },
    body: JSON.stringify(data),
  };

  //   console.log('requestOptions', requestOptions);

  return fetch(URL, requestOptions).then(res => res.json());
}

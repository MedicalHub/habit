import {put, takeLatest, call} from 'redux-saga/effects';

import API from 'app/saga/api';
import {UserTypes} from 'app/action_types';

function* login(action) {
  try {
    const {data} = action;
    const {result, error} = yield call(API.login, data);
    // console.log('result', result);
    // console.log('error', error.message);
    if (error) {
      yield put({type: UserTypes.LOGIN_FAILURE, error: error.message});
    } else {
      if (result.auth) {
        if (result.token) {
          API.setToken(result.token);
        }
        yield put({
          type: UserTypes.LOGIN_SUCCESS,
          data: {...result.user, token: result.token},
        });
      } else if (result.errors) {
        yield put({
          type: UserTypes.LOGIN_FAILURE,
          error:
            typeof result.errors === 'string'
              ? result.errors
              : result.errors[0],
        });
      } else {
        yield put({type: UserTypes.LOGIN_FAILURE, error: result.message});
      }
    }
  } catch (error) {
    yield put({type: UserTypes.LOGIN_FAILURE, error: error.message});
  }
}

function* loginWithSocial(action) {
  try {
    const {data} = action;
    const {result, error} = yield call(API.loginWithSocial, data);
    // console.log('result', result);
    // console.log('error', error);
    if (error) {
      yield put({type: UserTypes.LOGIN_SOCIAL_FAILURE, error: error.message});
    } else {
      if (result.auth) {
        if (result.token) {
          API.setToken(result.token);
        }
        yield put({
          type: UserTypes.LOGIN_SOCIAL_SUCCESS,
          data: {...result.user, token: result.token},
        });
      } else if (result.errors) {
        yield put({
          type: UserTypes.LOGIN_SOCIAL_FAILURE,
          error:
            typeof result.errors === 'string'
              ? result.errors
              : result.errors[0],
        });
      } else {
        yield put({
          type: UserTypes.LOGIN_SOCIAL_FAILURE,
          error: result.message,
        });
      }
    }
  } catch (error) {
    yield put({type: UserTypes.LOGIN_SOCIAL_FAILURE, error: error.message});
  }
}

function* register(action) {
  try {
    const {data} = action;
    const {result, error} = yield call(API.register, data);
    // console.log('result', result);
    if (error) {
      yield put({type: UserTypes.REGISTER_FAILURE, error: error.message});
    } else {
      if (result.register) {
        if (result.token) {
          API.setToken(result.token);
        }
        yield put({
          type: UserTypes.REGISTER_SUCCESS,
          data: {...result.user, token: result.token},
        });
      } else if (result.errors) {
        yield put({
          type: UserTypes.REGISTER_FAILURE,
          error:
            typeof result.errors === 'string'
              ? result.errors
              : result.errors[0],
        });
      } else {
        yield put({type: UserTypes.REGISTER_FAILURE, error: result.message});
      }
    }
  } catch (error) {
    yield put({type: UserTypes.REGISTER_FAILURE, error: error.message});
  }
}

function* getProfileInfo(action) {
  try {
    const {result, error} = yield call(API.getProfileInfo, action.userId);
    if (error) {
      yield put({
        type: UserTypes.GET_PROFILE_INFO_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: UserTypes.GET_PROFILE_INFO_FAILURE,
          error: result.errors,
        });
      } else {
        yield put({
          type: UserTypes.GET_PROFILE_INFO_SUCCESS,
          data: result,
        });
      }
    }
  } catch (error) {
    yield put({type: UserTypes.GET_PROFILE_INFO_FAILURE, error: error.message});
  }
}

function* updateProfile(action) {
  try {
    const {data} = action;
    const {result, error} = yield call(API.updateProfile, data);
    // console.log('result', result);
    if (error) {
      yield put({type: UserTypes.UPDATE_PROFILE_FAILURE, error: error.message});
    } else {
      if (result.errors) {
        yield put({
          type: UserTypes.UPDATE_PROFILE_FAILURE,
          error: result.errors,
        });
      } else {
        yield put({
          type: UserTypes.UPDATE_PROFILE_SUCCESS,
          data: result,
        });
      }
    }
  } catch (error) {
    yield put({type: UserTypes.UPDATE_PROFILE_FAILURE, error: error.message});
  }
}

function* updatePassword(action) {
  try {
    const {data} = action;
    const {result, error} = yield call(API.updatePassword, data);
    // console.log('result', result);
    if (error) {
      yield put({
        type: UserTypes.UPDATE_PASSWORD_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: UserTypes.UPDATE_PASSWORD_FAILURE,
          error: result.errors[0],
        });
      } else {
        yield put({
          type: UserTypes.UPDATE_PASSWORD_SUCCESS,
          data: result,
        });
      }
    }
  } catch (error) {
    yield put({type: UserTypes.UPDATE_PASSWORD_FAILURE, error: error.message});
  }
}

function* resetPassword(action) {
  try {
    const {data} = action;
    const {result, error} = yield call(API.resetPassword, data);
    // console.log('result', result);
    if (error) {
      yield put({
        type: UserTypes.RESET_PASSWORD_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: UserTypes.RESET_PASSWORD_FAILURE,
          error: result.errors[0],
        });
      } else {
        yield put({
          type: UserTypes.RESET_PASSWORD_SUCCESS,
          data: {...result.user, token: result.token},
        });
      }
    }
  } catch (error) {
    yield put({type: UserTypes.RESET_PASSWORD_FAILURE, error: error.message});
  }
}
function* getStatistics(action) {
  try {
    const {userId} = action;
    const {result, error} = yield call(API.getStatistics, userId);
    // console.log('result', result);
    if (error) {
      yield put({
        type: UserTypes.GET_STATISTICS_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: UserTypes.GET_STATISTICS_FAILURE,
          error: result.errors[0],
        });
      } else {
        yield put({
          type: UserTypes.GET_STATISTICS_SUCCESS,
          data: result,
        });
      }
    }
  } catch (error) {
    yield put({type: UserTypes.GET_STATISTICS_FAILURE, error: error.message});
  }
}

function* watchLogin() {
  yield takeLatest(UserTypes.LOGIN_REQUEST, login);
}

function* watchLoginWithSocial() {
  yield takeLatest(UserTypes.LOGIN_SOCIAL_REQUEST, loginWithSocial);
}

function* watchRegister() {
  yield takeLatest(UserTypes.REGISTER_REQUEST, register);
}

function* watchGetProfileInfo() {
  yield takeLatest(UserTypes.GET_PROFILE_INFO_REQUEST, getProfileInfo);
}

function* watchUpdateProfile() {
  yield takeLatest(UserTypes.UPDATE_PROFILE_REQUEST, updateProfile);
}

function* watchUpdatePassword() {
  yield takeLatest(UserTypes.UPDATE_PASSWORD_REQUEST, updatePassword);
}

function* watchResetPassword() {
  yield takeLatest(UserTypes.RESET_PASSWORD_REQUEST, resetPassword);
}
function* watchGetStatistics() {
  yield takeLatest(UserTypes.GET_STATISTICS_REQUEST, getStatistics);
}

export {
  watchLogin,
  watchLoginWithSocial,
  watchRegister,
  watchGetProfileInfo,
  watchUpdateProfile,
  watchUpdatePassword,
  watchResetPassword,
  watchGetStatistics,
};

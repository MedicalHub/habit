import {put, takeLatest, call} from 'redux-saga/effects';
import API from 'app/saga/api';
import {ItemTopicTypes} from 'app/action_types';

function* getItemTopic(action) {
  try {
    const {result, error, dataAudio} = yield call(
      API.getItemTopic,
      action.topicId,
      action.userId,
    );
    if (error) {
      yield put({
        type: ItemTopicTypes.GET_ITEM_TOPIC_FAILURE,
        error: error.message,
      });
    } else {
      if (result.errors) {
        yield put({
          type: ItemTopicTypes.GET_ITEM_TOPIC_FAILURE,
          error: result.errors,
        });
      } else {
        yield put({
          type: ItemTopicTypes.GET_ITEM_TOPIC_SUCCESS,
          data: result,
          dataAudio,
        });
      }
    }
  } catch (error) {
    yield put({
      type: ItemTopicTypes.GET_ITEM_TOPIC_FAILURE,
      error: error.message,
    });
  }
}

function* watchGetItemTopic() {
  yield takeLatest(ItemTopicTypes.GET_ITEM_TOPIC_REQUEST, getItemTopic);
}

export {watchGetItemTopic};

import AsyncStorage from '@react-native-community/async-storage';
import {createStore, applyMiddleware, compose} from 'redux';
import {persistStore, persistReducer} from 'redux-persist';

import rootReducer from 'app/reducers';
import initialState from 'app/initial_state';

import createSagaMiddleware from 'redux-saga';
import rootSaga from 'app/saga';
import thunk from 'redux-thunk';
const sagaMiddleware = createSagaMiddleware();

const persistConfig = {
  key: 'root',
  storage: AsyncStorage,
  blacklist: ['request'],
};

const persistedReducer = persistReducer(persistConfig, rootReducer);
const composeEnhancers = window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__ || compose;

const configureStore = (state = initialState) => {
  const store = createStore(
    persistedReducer,
    state,
    composeEnhancers(applyMiddleware(sagaMiddleware, thunk)),
  );

  sagaMiddleware.run(rootSaga);

  return store;
};

export const store = configureStore(initialState);

const persistor = persistStore(store);

export {persistor};
export default configureStore;

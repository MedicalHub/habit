import React from 'react';
import {StyleSheet, TouchableOpacity, ViewPropTypes} from 'react-native';

import PropTypes from 'prop-types';

import Icon from 'app/components/icons';

import {Colors} from 'app/constants';
import {verticalScale, getMediaCenterButtonPadding} from 'app/utils/screen';

const hitSlot = {
  bottom: 10,
  top: 10,
  left: 10,
  right: 10,
};

class MediaCenterButton extends React.PureComponent<Props> {
  static propTypes = {
    centerContainer: ViewPropTypes.style,
    centerIconSize: PropTypes.number,
  };

  static defaultProps = {
    centerIconSize: 16,
    centerIconColor: '#FFF',
    enable: false,
  };
  constructor(props) {
    super(props);
    this.state = {
      statusCenterControlBtn: true,
    };
  }

  onCenterControlBtn = () => {
    this.setState({statusCenterControlBtn: !this.state.statusCenterControlBtn});
  };

  render() {
    const {
      centerContainer,
      centerIconSize,
      centerIconColor,
      enable,
    } = this.props;
    const {statusCenterControlBtn} = this.state;

    let CenterControlBtn = null;

    let iconSize = 40;
    if (centerContainer) {
      iconSize = centerContainer.height || centerContainer.width;
    }

    const paddingLeft = getMediaCenterButtonPadding(iconSize);

    if (statusCenterControlBtn) {
      CenterControlBtn = (
        <TouchableOpacity
          disabled={!enable}
          onPress={this.onCenterControlBtn}
          style={[
            styles.centerContainer,
            centerContainer,
            {
              height: verticalScale(iconSize),
              width: verticalScale(iconSize),
              borderRadius: verticalScale(iconSize) / 2,
              paddingLeft,
            },
          ]}
          hitSlop={hitSlot}>
          <Icon
            type={'System'}
            name={'play-fill'}
            color={centerIconColor}
            size={verticalScale(centerIconSize)}
          />
        </TouchableOpacity>
      );
    } else {
      CenterControlBtn = (
        <TouchableOpacity
          disabled={!enable}
          onPress={this.onCenterControlBtn}
          style={[
            styles.centerContainer,
            centerContainer,
            {
              height: verticalScale(iconSize),
              width: verticalScale(iconSize),
              borderRadius: verticalScale(iconSize) / 2,
            },
          ]}
          hitSlop={hitSlot}>
          <Icon
            type={'System'}
            name={'pause'}
            color={centerIconColor}
            size={verticalScale(centerIconSize)}
          />
        </TouchableOpacity>
      );
    }

    return CenterControlBtn;
  }
}
export default MediaCenterButton;

const styles = StyleSheet.create({
  centerContainer: {
    height: verticalScale(40),
    width: verticalScale(40),
    borderRadius: verticalScale(40) / 2,
    backgroundColor: Colors.PRIMARY,
    justifyContent: 'center',
    alignItems: 'center',
    marginHorizontal: 32,
  },
});

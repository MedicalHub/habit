import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ViewPropTypes,
} from 'react-native';

import PropTypes from 'prop-types';

import Icon from 'app/components/icons';
import SliderMedia from 'app/components/media_play/media_slider';

import {Colors, Screen} from 'app/constants';
import Controls from 'app/components/media_play/controls';
import {verticalScale} from 'app/utils/screen';

export const MEDIA_HEIGHT = 125;
export const MEDIA_WITH_TITLE_HEIGHT = 147;

const hitSlot = {
  bottom: 10,
  top: 10,
  left: 10,
  right: 10,
};

class MediaPlay extends React.PureComponent<Props> {
  static propTypes = {
    title: PropTypes.string,
    containerStyle: ViewPropTypes.style,
    closeBtnVisible: PropTypes.bool,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: true,
      closeBtnVisible: props.closeBtnVisible || false,
    };
  }

  componentDidMount() {
    if (this.props.refs) {
      this.props.refs(this);
    }
  }
  onCloseBtn = () => {
    this.setState({visible: false});
  };

  render() {
    const {visible, closeBtnVisible} = this.state;
    if (!visible) {
      return null;
    }
    const {containerStyle, title} = this.props;
    let CloseBtn = null;
    if (closeBtnVisible) {
      CloseBtn = (
        <TouchableOpacity
          style={styles.closeBtn}
          hitSlop={hitSlot}
          onPress={this.onCloseBtn}>
          <Icon
            type={'System'}
            name={'exit'}
            color={Colors.INACTIVE_TINT}
            size={20}
          />
        </TouchableOpacity>
      );
    }

    let Title = null;

    if (title) {
      Title = (
        <Text
          style={styles.title}
          numberOfLines={1}
          adjustsFontSizeToFit={true}>
          {title}
        </Text>
      );
    }
    return (
      <View
        style={[
          styles.container,
          {
            // height: title
            //   ? verticalScale(MEDIA_WITH_TITLE_HEIGHT)
            //   : verticalScale(MEDIA_HEIGHT),
          },
          containerStyle,
        ]}>
        {Title}
        <SliderMedia />
        <Controls />
        {CloseBtn}
      </View>
    );
  }
}
export default MediaPlay;

const styles = StyleSheet.create({
  container: {
    // height: verticalScale(MEDIA_HEIGHT),
    paddingTop: 8,
    backgroundColor: '#FFF',
    justifyContent: 'flex-end',
    paddingBottom: Screen.PADDING_BOTTOM,
    shadowColor: 'rgba(2,3,9,0.08)',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 30,
  },
  closeBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
  title: {
    fontSize: 14,
    fontFamily: 'iCielHelveticaNowText-Regular',
    color: Colors.PRIMARY_TEXT,
    textAlign: 'center',
    paddingHorizontal: 16,
  },
});

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import Slider from '@react-native-community/slider';
import {Colors} from 'app/constants';
import {verticalScale} from 'app/utils/screen';

class SliderMedia extends React.PureComponent<Props> {
  render() {
    return (
      <View style={styles.container}>
        <Text style={styles.time}>{'30:00'}</Text>
        <Slider
          value={2}
          style={styles.sliderContainer}
          minimumValue={0}
          maximumValue={5}
          minimumTrackTintColor={Colors.PRIMARY}
          maximumTrackTintColor="#F0F0F0"
        />
        <Text style={styles.time}>{'1:40:30'}</Text>
      </View>
    );
  }
}
export default SliderMedia;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignItems: 'center',
  },
  sliderContainer: {
    height: verticalScale(40),
    flex: 1,
    marginHorizontal: 5,
  },
  time: {
    fontFamily: 'iCielHelveticaNowText-Regular',
    fontSize: 12,
    color: Colors.INACTIVE_TINT,
  },
});

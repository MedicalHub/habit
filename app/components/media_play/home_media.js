import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  ViewPropTypes,
  TouchableOpacity,
  DeviceEventEmitter,
} from 'react-native';

import Controls from 'app/components/media_play/controls';
import Icon from 'app/components/icons';

import {Colors} from 'app/constants';

export const HOME_MEDIA_HEIGHT = 60;

class HomeMedia extends React.PureComponent<Props> {
  static propTypes = {
    containerStyle: ViewPropTypes.style,
  };

  constructor(props) {
    super(props);
    this.state = {
      visible: false,
    };
    this._emitter = DeviceEventEmitter.addListener(
      'openHomeMedia',
      this.handleOpenHomeMedia,
    );
  }

  componentWillUnmount() {
    this._emitter.remove();
  }

  handleOpenHomeMedia = () => {
    if (!this.state.visible) {
      this.setState({visible: true});
    }
  };

  onClose = () => {
    this.setState({visible: false});
  };

  render() {
    const {containerStyle} = this.props;
    const {visible} = this.state;
    if (!visible) {
      return null;
    }

    return (
      <View style={[styles.container, containerStyle]}>
        <TouchableOpacity style={styles.closeBtn} onPress={this.onClose}>
          <Icon
            type="System"
            name="exit"
            size={16}
            color={Colors.INACTIVE_TINT}
          />
        </TouchableOpacity>
        <View style={styles.content}>
          <Text
            style={styles.title}
            numberOfLines={1}
            adjustsFontSizeToFit={true}>
            {'Luận giải hôm nay (Quẻ 43)'}
          </Text>
          <Text
            style={styles.time}
            numberOfLines={1}
            adjustsFontSizeToFit={true}>
            {'13/02/2020'}
          </Text>
        </View>
        <Controls
          centerContainer={styles.centerContainer}
          centerIconSize={12}
        />
      </View>
    );
  }
}
export default HomeMedia;

const styles = StyleSheet.create({
  container: {
    height: HOME_MEDIA_HEIGHT,
    backgroundColor: '#FFF',
    shadowColor: 'rgba(2,3,9,0.08)',
    shadowOffset: {
      width: 0,
      height: 0,
    },
    shadowOpacity: 1,
    shadowRadius: 5,
    elevation: 30,
    flexDirection: 'row',
    paddingRight: 8,
  },
  centerContainer: {
    width: 32,
    height: 32,
    borderRadius: 16,
    marginHorizontal: 16,
  },
  closeBtn: {
    height: '100%',
    paddingHorizontal: 16,
    justifyContent: 'center',
    alignItems: 'center',
  },
  content: {
    flex: 1,
    height: '100%',
    marginRight: 8,
    justifyContent: 'center',
  },
  title: {
    color: Colors.PRIMARY_TEXT,
    fontSize: 14,
    fontFamily: 'iCielHelveticaNowText-Medium',
    lineHeight: 20,
  },
  time: {
    color: Colors.PRIMARY,
    fontSize: 12,
    fontFamily: 'iCielHelveticaNowText-Regular',
    lineHeight: 16,
  },
});

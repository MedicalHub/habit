import React from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  ViewPropTypes,
} from 'react-native';

import PropTypes from 'prop-types';

import Icon from 'app/components/icons';

import {Colors} from 'app/constants';
import {verticalScale, getMediaCenterButtonPadding} from 'app/utils/screen';
import MediaCenterButton from 'app/components/media_play/center_button';

export const MEDIA_HEIGHT = 125;
const hitSlot = {
  bottom: 10,
  top: 10,
  left: 10,
  right: 10,
};

class Controls extends React.PureComponent<Props> {
  static propTypes = {
    containerStyle: ViewPropTypes.style,
    centerContainer: ViewPropTypes.style,
    centerIconSize: PropTypes.number,
  };

  static defaultProps = {
    centerIconSize: 16,
  };

  render() {
    const {centerContainer, containerStyle, centerIconSize} = this.props;
    const LeftControl = (
      <TouchableOpacity hitSlop={hitSlot}>
        <Icon
          type={'System'}
          name={'controls-left'}
          color={Colors.INACTIVE_TINT}
          size={24}
        />
      </TouchableOpacity>
    );

    const RightControl = (
      <TouchableOpacity hitSlop={hitSlot}>
        <Icon
          type={'System'}
          name={'controls-right'}
          color={Colors.INACTIVE_TINT}
          size={24}
        />
      </TouchableOpacity>
    );
    return (
      <View style={[styles.controlContainer, containerStyle]}>
        {LeftControl}
        <MediaCenterButton
          centerContainer={centerContainer}
          centerIconSize={centerIconSize}
          enable={true}
        />
        {RightControl}
      </View>
    );
  }
}
export default Controls;

const styles = StyleSheet.create({
  controlContainer: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  closeBtn: {
    position: 'absolute',
    top: 10,
    right: 10,
  },
});

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ThirdPartyLogin from 'app/components/third_party_login/third_party_login';

import {loginWithSocial} from 'app/actions/user';

const mapStateToProps = state => {
  const isLoading = state.request.loginWithSocial.loading;
  const error = state.request.loginWithSocial.error;

  return {
    isLoading,
    error,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        loginWithSocial: loginWithSocial,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ThirdPartyLogin);

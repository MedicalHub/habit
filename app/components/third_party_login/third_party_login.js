/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity, Platform} from 'react-native';

import {AccessToken, LoginManager} from 'react-native-fbsdk';
import {GoogleSignin, statusCodes} from '@react-native-community/google-signin';

import appleAuth, {
  AppleButton,
  AppleAuthRequestOperation,
  AppleAuthRequestScope,
  AppleAuthCredentialState,
} from '@invertase/react-native-apple-authentication';

import FormattedText from 'app/components/formatted_text';
import Icon from 'app/components/icons';
import {Colors} from 'app/constants';
import Touchable from 'app/components/touchable';
import AppIcon from 'app/components/app_icon';
import LoadingHolder from 'app/utils/loading';
import DropDownHolder from 'app/utils/dropdownelert';

GoogleSignin.configure();

class ThirdPartyLogin extends React.PureComponent {
  onFacebookLoginError = error => {
    console.log('error', error);
  };

  loginWithFacebook = received => {
    // console.log('data', JSON.stringify(data));
    const {actions} = this.props;

    const data = {
      userId: received.userID,
      accessToken: received.accessToken,
      type: 'facebook',
    };
    actions.loginWithSocial(data);
  };

  handleLoginFacebook = result => {
    if (result.isCancelled) {
      // console.log('result', result);
    } else {
      AccessToken.getCurrentAccessToken().then(data => {
        this.loginWithFacebook(data);
      });
    }
  };

  onLoginWithFacebook = () => {
    AccessToken.getCurrentAccessToken()
      .then(data => {
        if (data) {
          // TODO
          this.loginWithFacebook(data);
        }
        if (!data) {
          this.handleLoginFacebook();
        }
      })
      .catch(() => {
        LoginManager.logInWithPermissions(['public_profile', 'email'])
          .then(this.handleLoginFacebook)
          .catch(this.onFacebookLoginError);
      });
  };
  onLoginWithGoogle = async () => {
    try {
      await GoogleSignin.hasPlayServices();
      await GoogleSignin.signIn();

      const {accessToken, idToken} = await GoogleSignin.getTokens();

      //   console.log('accessToken', accessToken);
      //   console.log('idToken', idToken);
      const {actions} = this.props;

      const data = {
        idToken,
        accessToken,
        type: 'google',
      };
      actions.loginWithSocial(data);
    } catch (error) {
      console.log('error', error);

      if (error.code === statusCodes.SIGN_IN_CANCELLED) {
        // user cancelled the login flow
      } else if (error.code === statusCodes.IN_PROGRESS) {
        // operation (f.e. sign in) is in progress already
      } else if (error.code === statusCodes.PLAY_SERVICES_NOT_AVAILABLE) {
        // play services not available or outdated
      } else {
        // Alert.alert(
        //     this.props.intl.formatMessage({id: 'error', defaultMessage: 'Error'}),
        //     this.props.intl.formatMessage({id: 'common.error', defaultMessage: 'Something wrong, Please try again.'}),
        //     [
        //         {
        //             text: this.props.intl.formatMessage({id: 'button.ok', defaultMessage: 'Ok'}),
        //         },
        //     ]
        // );
      }
    }
  };

  onLoginWithApple = async () => {
    try {
      // performs login request
      const appleAuthRequestResponse = await appleAuth.performRequest({
        requestedOperation: AppleAuthRequestOperation.LOGIN,
        requestedScopes: [
          AppleAuthRequestScope.EMAIL,
          AppleAuthRequestScope.FULL_NAME,
        ],
      });

      //   console.log(
      //     'appleAuthRequestResponse',
      //     JSON.stringify(appleAuthRequestResponse),
      //   );

      // get current authentication state for user
      const credentialState = await appleAuth.getCredentialStateForUser(
        appleAuthRequestResponse.user,
      );

      // console.log('credentialState', JSON.stringify(credentialState));

      // use credentialState response to ensure the user is authenticated
      if (credentialState === AppleAuthCredentialState.AUTHORIZED) {
        // user is authenticated
        const data = {
          idToken: appleAuthRequestResponse.identityToken,
          type: 'apple',
        };
        this.props.actions.loginWithSocial(data);
      }
    } catch (error) {
      console.log('error', error);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      LoadingHolder.stop();
      //    Nếu đăng nhập có lỗi
      if (this.props.error) {
        DropDownHolder.alert('error', this.props.error);
      } else {
        // Đăng nhập thành công
        const {navigation, handleLogin} = this.props;
        handleLogin();
        navigation.navigate('AppMain');
      }
    }
  }

  render() {
    return (
      <View style={styles.container}>
        <FormattedText
          id="title.orThirdParyLogin"
          defaultMessage="Hoặc"
          style={styles.thirdPartyLogin}
        />

        <View
          style={{
            flexDirection: 'row',
          }}>
          <Touchable style={styles.fbBtn} onPress={this.onLoginWithFacebook}>
            <AppIcon
              source={require('assets/images/fb.png')}
              color={Colors.LIGHT}
            />
            <Text style={styles.fbText}>Facebook</Text>
          </Touchable>
          <View style={{width: 23}} />
          <Touchable style={styles.ggBtn} onPress={this.onLoginWithGoogle}>
            <AppIcon
              source={require('assets/images/google.png')}
              color={Colors.LIGHT}
            />
            <Text style={styles.ggText}> Google</Text>
          </Touchable>
        </View>
        {Platform.OS === 'ios' && (
          <TouchableOpacity
            style={styles.appleBtn}
            onPress={this.onLoginWithApple}>
            <Icon
              type={'AntDesign'}
              name={'apple1'}
              size={24}
              color={Colors.DARK_PRIMARY}
            />
            <Text style={styles.appleText}> Apple</Text>
          </TouchableOpacity>
        )}
      </View>
    );
  }
}
export default ThirdPartyLogin;

const styles = StyleSheet.create({
  container: {
    // flex: 1,
    justifyContent: 'center',
  },
  thirdPartyLogin: {
    lineHeight: 17,
    fontSize: 16,
    textAlign: 'center',
    color: Colors.LIGHT,
    fontWeight: '600',
    marginBottom: 24,
  },
  fbBtn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    backgroundColor: '#4460A0',
    borderRadius: 8,
  },
  fbText: {
    fontSize: 16,
    color: '#FFF',
  },
  ggBtn: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    backgroundColor: '#F93F2D',
    borderRadius: 8,
  },
  ggText: {
    fontSize: 16,
    color: '#FFF',
  },
  appleBtn: {
    marginTop: 24,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
    height: 44,
    backgroundColor: Colors.LIGHT,
    borderRadius: 8,
  },
  appleText: {
    fontSize: 16,
    color: Colors.DARK_PRIMARY,
  },
});

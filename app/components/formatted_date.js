import React from 'react';
import {Text} from 'react-native';

import {injectIntl} from 'react-intl';
import PropTypes from 'prop-types';

import Device from 'app/utils/device';

class FormattedDate extends React.PureComponent<Props> {
  static propTypes = {
    intl: PropTypes.object.isRequired,
    value: PropTypes.any.isRequired,
    format: PropTypes.string,
    children: PropTypes.func,
    style: PropTypes.object,
  };

  render() {
    const {intl, value, style, children, ...props} = this.props;

    // Reflect.deleteProperty(props, 'format');
    delete props.format;

    const formattedDate = intl.formatDate(value, {
      ...this.props,
      locale: Device.locale,
      timeZone: Device.timeZone,
    });

    if (typeof children === 'function') {
      return children(formattedDate);
    }

    return <Text {...props}>{formattedDate}</Text>;
  }
}

export default injectIntl(FormattedDate);

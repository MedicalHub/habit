import React from 'react';
import {Text, StyleSheet, ViewPropTypes, View} from 'react-native';

import PropTypes from 'prop-types';
import Icon from 'app/components/icons';
import Touchable from 'app/components/touchable';

import {Colors} from 'app/constants';

export const BUTTON_HEIGHT = 44;
class Button extends React.PureComponent<Props> {
  static propTypes = {
    label: PropTypes.string.isRequired,
    onPress: PropTypes.func.isRequired,
    containerStyle: ViewPropTypes.style,
    customTextStyle: PropTypes.object,
    iconName: PropTypes.string,
  };

  render() {
    const {
      label,
      onPress,
      containerStyle,
      customTextStyle,
      iconName,
    } = this.props;

    let WithIcon = null;
    if (iconName) {
      WithIcon = (
        <View style={{marginRight: 9}}>
          <Icon
            name={iconName}
            size={24}
            color={Colors.PRIMARY_TEXT}
            type={'System'}
          />
        </View>
      );
    }

    const buttonStyles = {...styles.container, ...containerStyle};
    return (
      <Touchable style={buttonStyles} onPress={onPress}>
        {WithIcon}
        <Text
          numberOfLines={1}
          adjustsFontSizeToFit={true}
          style={[styles.label, customTextStyle]}>
          {label}
        </Text>
      </Touchable>
    );
  }
}
export default Button;

const styles = StyleSheet.create({
  container: {
    height: BUTTON_HEIGHT,
    backgroundColor: Colors.PRIMARY_BUTTON,
    flexDirection: 'row',
    borderRadius: 8,
    paddingHorizontal: 16,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontSize: 16,
    // lineHeight: 24,
    color: Colors.LIGHT,
  },
});

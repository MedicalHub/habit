/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

import PropTypes from 'prop-types';

import FormattedText from 'app/components/formatted_text';
import {Colors, Screen} from 'app/constants';
import {changeOpacity} from 'app/utils/theme';

class ActiveCodeBottom extends React.PureComponent<Props> {
  static propTypes = {
    activeMethod: PropTypes.string,
    onQRCode: PropTypes.func,
    onEnterCode: PropTypes.func,
  };
  static defaultProps = {
    activeMethod: 'code',
    onQRCode: () => {},
    onEnterCode: () => {},
  };
  render() {
    const {activeMethod, onQRCode, onEnterCode} = this.props;

    let activeQRBackground = changeOpacity(Colors.PRIMARY_2, 0.5);
    let activeCodeBackground = Colors.ACTIVE_TINT;
    let disableCode = false;
    let disableQR = false;

    if (activeMethod === 'code') {
      disableCode = true;
      activeQRBackground = 'rgba(0, 0, 0, 0)';
      activeCodeBackground = Colors.ACTIVE_TINT;
    } else {
      disableQR = true;
      activeQRBackground = Colors.ACTIVE_TINT;
      activeCodeBackground = 'rgba(0, 0, 0, 0)';
    }
    return (
      <View style={styles.container}>
        <TouchableOpacity
          onPress={onQRCode}
          disabled={disableQR}
          style={[
            styles.button,
            {
              backgroundColor: activeQRBackground,
            },
          ]}>
          <FormattedText
            numberOfLines={2}
            id="scanQR"
            defaultMessage="Quét QR"
            style={styles.text}
          />
        </TouchableOpacity>
        <TouchableOpacity
          onPress={onEnterCode}
          disabled={disableCode}
          style={[
            styles.button,
            {
              backgroundColor: activeCodeBackground,
            },
          ]}>
          <FormattedText
            numberOfLines={2}
            id="enterCode"
            defaultMessage="Nhập mã"
            style={styles.text}
          />
        </TouchableOpacity>
      </View>
    );
  }
}
export default ActiveCodeBottom;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    bottom: Screen.PADDING_BOTTOM,
    backgroundColor: changeOpacity(Colors.PRIMARY_2, 0.5),
    position: 'absolute',
    left: 24,
    right: 24,
    borderRadius: 8,
    overflow: 'hidden',
    padding: 2,
  },
  button: {
    flex: 1,
    borderRadius: 8,
    height: 40,
    justifyContent: 'center',
  },
  text: {
    fontSize: 14,
    fontFamily: 'iCielHelveticaNowText-Medium',
    color: Colors.LIGHT,
    textAlign: 'center',
  },
});

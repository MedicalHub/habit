import React from 'react';
import {View, StyleSheet, ViewPropTypes} from 'react-native';

import PropTypes from 'prop-types';

import {isIPhoneX} from 'app/utils/screen';
import {Colors} from 'app/constants';
import Button from 'app/components/button';

class AbsoluteBtn extends React.PureComponent<Props> {
  static propTypes = {
    onPress: PropTypes.func,
    label: PropTypes.string,
    iconName: PropTypes.string,
    buttonStyle: ViewPropTypes.style,
    containerStyle: ViewPropTypes.style,
  };

  static defaultProps = {
    onPress: () => {},
  };
  render() {
    const {onPress, label, buttonStyle, iconName, containerStyle} = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        <Button
          iconName={iconName}
          label={label}
          onPress={onPress}
          containerStyle={{...styles.button, ...buttonStyle}}
        />
      </View>
    );
  }
}
export default AbsoluteBtn;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    height: isIPhoneX() ? 64 + 34 : 64,
    position: 'absolute',
    bottom: 0,
    left: 0,
    right: 0,
    shadowColor: 'rgba(0,0,0,0.1)',
    shadowOpacity: 2,
    shadowRadius: 3,
    shadowOffset: {
      width: 0,
      height: 0,
    },
  },
  button: {
    flexDirection: 'row',
    height: 48,
    marginHorizontal: 24,
    marginTop: isIPhoneX() ? 12 : 8,
    backgroundColor: Colors.PRIMARY_BUTTON,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Medium',
  },
});

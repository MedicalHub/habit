import React from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';

import PropTypes from 'prop-types';

import FormattedText from 'app/components/formatted_text';
import {Colors} from 'app/constants';
import Icon from 'app/components/icons';

class CheckBox extends React.PureComponent {
  static propTypes = {
    titleId: PropTypes.string.isRequired,
    titleDefMsg: PropTypes.string,
    onPress: PropTypes.func,
    selected: PropTypes.bool,
  };

  render() {
    const {titleId, titleDefMsg, onPress, selected} = this.props;

    const checkboxIcon = selected ? (
      <Icon
        type={'System'}
        name={'radio_selected'}
        size={24}
        color={Colors.PRIMARY}
      />
    ) : (
      <Icon
        type={'System'}
        name={'outline'}
        size={24}
        color={Colors.INACTIVE_TINT}
      />
    );

    const titleComponent = selected ? (
      <FormattedText
        id={titleId}
        defaultMessage={titleDefMsg}
        style={[styles.text, {color: Colors.PRIMARY}]}
      />
    ) : (
      <FormattedText
        id={titleId}
        defaultMessage={titleDefMsg}
        style={[styles.text, {color: Colors.INACTIVE_TINT}]}
      />
    );
    return (
      <TouchableOpacity
        style={styles.container}
        onPress={() => onPress()}
        hitSlop={{
          top: 20,
          bottom: 20,
          left: 20,
          right: 20,
        }}>
        {checkboxIcon}
        <View style={{paddingLeft: 4}}>{titleComponent}</View>
      </TouchableOpacity>
    );
  }
}

export default CheckBox;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
  },
  text: {
    fontFamily: 'iCielHelveticaNowText-Regular',
    fontSize: 14,
  },
});

import React from 'react';
import {StyleSheet, FlatList, ViewPropTypes} from 'react-native';

import PropTypes from 'prop-types';

import Separator from 'app/components/separator';
import CheckBox from './checkbox';

class GroupCheckBox extends React.PureComponent<Props> {
  static propTypes = {
    data: PropTypes.any,
    listStyle: ViewPropTypes.style,
  };

  constructor(props) {
    super(props);
    const selectedIndex = [];
    props.data.forEach((item, index) => {
      if (item.selected) {
        selectedIndex.push(index);
      }
    });

    // console.log('selectedIndex', selectedIndex);
    this.state = {
      selected: new Map([[props.data[selectedIndex[0] || 0].id, true]]),
    };
  }

  handleSelected = item => {
    this.setState(state => {
      // copy the map rather than modifying state.
      const selected = new Map();
      selected.set(item.id, !selected.get(item.id)); // toggle
      return {selected};
    });
    this.props.onSelect(item);
  };

  renderItem = ({item}) => {
    return (
      <CheckBox
        titleId={item.titleId}
        titleDefMsg={item.titleDefMsg}
        onPress={() => this.handleSelected(item)}
        selected={this.state.selected.get(item.id)}
      />
    );
  };

  render() {
    const {data, listStyle} = this.props;
    return (
      <FlatList
        data={data}
        extraData={data}
        horizontal={true}
        keyExtractor={(item, index) => index.toString()}
        ItemSeparatorComponent={() => <Separator width={40} />}
        renderItem={this.renderItem}
        scrollEnabled={false}
        style={[styles.container, listStyle]}
      />
    );
  }
}

export default GroupCheckBox;

const styles = StyleSheet.create({
  container: {
    flexGrow: 0,
  },
});

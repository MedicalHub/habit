import React from 'react';
import {View, StyleSheet, TouchableOpacity, Platform} from 'react-native';

import FastImage from 'react-native-fast-image';
import ImagePicker from 'react-native-image-picker';
import ImageResizer from 'react-native-image-resizer';

import {Colors, General} from 'app/constants';

import Icon from 'app/components/icons';
import api from 'app/saga/api';
import Loading from 'app/components/loading';

const options = {
  title: 'Chọn ảnh',
  cancelButtonTitle: 'Huỷ',
  takePhotoButtonTitle: 'Máy ảnh',
  chooseFromLibraryButtonTitle: 'Thư viện',
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
  cameraType: 'front',
  mediaType: 'photo',
  permissionDenied: {
    title: 'Ứng dụng chưa được cấp quyền.',
    text: 'Vui lòng cấp quyền để sử dụng tính năng này',
    reTryTitle: '',
    okTitle: 'Đồng ý',
  },
};

export async function uploadImage(method: string, url: string, file: any) {
  return new Promise((resolve, reject) => {
    const xhr = new XMLHttpRequest();
    xhr.open(method, url);
    xhr.setRequestHeader('Content-Type', file.type);
    xhr.onload = () => {
      // console.log('xhr.status', xhr.status);
      if (xhr.status !== 200) {
        reject(
          new Error(
            `Request failed. Status: ${xhr.status}. Content: ${
              xhr.responseText
            }`,
          ),
        );
      }
      resolve('success');
    };
    xhr.send(file);
  });
}

const IMAGE_RESIZE = 200;
class Avatar extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      uploading: false,
    };
  }

  onAvatar = () => {
    ImagePicker.showImagePicker(options, async response => {
      // console.log('Response = ', response);
      if (response.didCancel) {
        // console.log('User cancelled image picker');
      } else if (response.error) {
        // console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        // console.log('User tapped custom button: ', response.customButton);
      } else {
        this.setState({
          uploading: true,
        });

        const source = {uri: response.uri};
        // resize
        let imageResize = IMAGE_RESIZE;
        if (response.height && response.width) {
          imageResize =
            response.height < response.width ? response.height : response.width;
          if (imageResize >= 3000) {
            imageResize = Math.round(imageResize / 10);
          } else if (imageResize >= 1500) {
            imageResize = Math.round(imageResize / 5);
          } else if (imageResize >= 600) {
            imageResize = Math.round(imageResize / 3);
          } else {
            imageResize = IMAGE_RESIZE;
          }
        }
        const resizeResponse = await ImageResizer.createResizedImage(
          source.uri,
          imageResize,
          imageResize,
          'PNG',
          100,
        );

        // console.log('resizeResponse', resizeResponse);
        if (resizeResponse && resizeResponse.uri) {
          source.uri = resizeResponse.uri;
        }

        const fileName = `${source.uri.split('/').pop()}`;
        const data = {
          targetFile: `upload/userfiles/${
            this.props.userId
          }/images/${fileName}`,
          fileType: response.type,
        };

        // console.log('data', data);
        try {
          const result = await api.callApi('upload-media/upload', 'POST', data);
          // console.log('result', result);
          if (result.success) {
            const path = source.uri;

            const FILE =
              Platform.OS === 'ios' ? path.replace('file://', '') : path;
            const file = {
              uri: FILE,
              type: response.type,
              name: FILE.split('/').pop(),
            };

            // console.log('file', file);

            uploadImage('PUT', result.data.returnData.signedRequest, file)
              .then(async resp => {
                // console.log('resp', resp);
                const {userId, actions} = this.props;
                const updateData = {
                  _id: userId,
                  avatar: data.targetFile,
                };
                // console.log('data', data);

                const {result: updateResult} = await api.updateProfile(
                  updateData,
                );

                // update profile in store - redux
                if (updateResult && updateResult.avatar) {
                  // console.log('updateResult', updateResult);
                  actions.updateAvatar(updateResult.avatar);
                }
              })
              .catch(error => {
                // console.log('error', error);
              });
          }
        } catch (error) {
          console.log('error', error);
        }
      }
    });
  };
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.avatar !== prevProps.avatar &&
      this.state.uploading === true
    ) {
      this.setState({uploading: false});
    }
  }

  render() {
    const {uploading} = this.state;
    let avatarSource = require('assets/images/avatar-profile-default.png');

    if (this.props.avatar) {
      avatarSource = {uri: `${General.STORAGE_DOMAIN}/${this.props.avatar}`};
    }

    let Image = (
      <FastImage
        source={avatarSource}
        resizeMode="cover"
        style={styles.image}
      />
    );

    if (uploading) {
      Image = (
        <View style={[styles.image]}>
          <FastImage
            source={avatarSource}
            resizeMode="cover"
            style={styles.image}
          />
          <Loading isLoading={uploading} />
        </View>
      );
    }
    return (
      <TouchableOpacity style={styles.avatar} onPress={this.onAvatar}>
        {Image}
        {/* <View style={styles.cameraIcon}>
          <Icon
            type="FontAwesome"
            name="camera"
            size={24}
            color={Colors.LIGHT}
          />
        </View> */}
      </TouchableOpacity>
    );
  }
}
export default Avatar;

const IMAGE_SIZE = 80;
const styles = StyleSheet.create({
  avatar: {
    height: IMAGE_SIZE,
    width: IMAGE_SIZE,
    borderRadius: 8,
    marginRight: 25,
    backgroundColor: Colors.PRIMARY_1,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
    width: null,
    height: null,
  },
  cameraIcon: {
    position: 'absolute',
    bottom: 0,
    right: 0,
  },
});

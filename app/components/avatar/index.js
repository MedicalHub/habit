import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Avatar from './avatar';
import {getUserInfo} from 'app/selectors/user';
import {updateProfile, updateAvatar} from 'app/actions/user';

const mapStateToProps = state => {
  const user = getUserInfo(state);

  return {
    avatar: user.avatar,
    userId: user._id,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        updateProfile: updateProfile,
        updateAvatar: updateAvatar,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Avatar);

import React from 'react';
import {
  Text,
  StyleSheet,
  TouchableOpacity,
  ViewPropTypes,
  View,
} from 'react-native';

import PropTypes from 'prop-types';
import {Colors} from 'app/constants';

class TouchableText extends React.PureComponent<Props> {
  static propTypes = {
    label: PropTypes.string.isRequired,
    hitSlop: PropTypes.object,
    onPress: PropTypes.func.isRequired,
    style: PropTypes.object,
    position: PropTypes.oneOf(['flex-start', 'center', 'flex-end']),
  };

  static defaultProps = {
    position: 'flex-start',
  };
  render() {
    const {
      label,
      onPress,
      hitSlop,
      style,
      position,
      styleContainer,
    } = this.props;

    return (
      <View
        style={[
          styles.container,
          {
            alignItems: position,
          },
          styleContainer,
        ]}>
        <TouchableOpacity
          onPress={() => onPress()}
          hitSlop={{
            top: 10,
            bottom: 10,
            left: 10,
            right: 10,
            ...hitSlop,
          }}>
          <Text style={[styles.label, style]}>{label}</Text>
        </TouchableOpacity>
      </View>
    );
  }
}
export default TouchableText;

const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
  },
  label: {
    fontSize: 14,
    color: Colors.ACTIVE_TINT,
    fontWeight: '500',
  },
});

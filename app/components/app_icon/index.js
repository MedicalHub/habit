import React from 'react';

import PropTypes from 'prop-types';

import {View, Text, StyleSheet, Image} from 'react-native';
import {Colors} from 'app/constants';

class AppIcon extends React.PureComponent<Props> {
  static propTypes = {
    source: PropTypes.node,
    color: PropTypes.string,
  };

  static defaultProps = {
    color: Colors.ICON_PRIMARY,
  };
  render() {
    const {source, color, containerStyle} = this.props;
    return (
      <View style={[styles.container, containerStyle]}>
        <Image
          source={source}
          resizeMode={'contain'}
          style={[styles.image, {tintColor: color}]}
        />
      </View>
    );
  }
}
export default AppIcon;

const styles = StyleSheet.create({
  container: {
    height: 24,
    width: 24,
  },
  image: {
    height: null,
    width: null,
    flex: 1,
  },
});

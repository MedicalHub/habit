import React, {PureComponent} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import Modal from 'react-native-modal';

class ModalAlert extends PureComponent {
  state = {
    indexCheck: -1,
    isShowModal: false,
  };

  onSelect = index => {
    this.setState({indexCheck: index});
  };

  onShow = () => this.setState({isShowModal: true});
  onHide = () => {
    this.setState({isShowModal: false});
    this.props.onHide && this.props.onHide();
  };
  onEdit = () => {
    this.setState({isShowModal: false}, () => {
      this.props.navigation.goBack();
    });
  };

  onContinue = () => {
    this.setState({isShowModal: false}, () => {
      this.props.onContinue && this.props.onContinue();
    });
  };

  onCancel = () => {
    this.setState({isShowModal: false}, () => {
      this.props.onCancel && this.props.onCancel();
    });
  };

  render() {
    const {isShowModal} = this.state;
    const {
      title,
      content,
      type,
      titleCancel,
      titleAccept,
      showButton,
    } = this.props;
    return (
      <Modal isVisible={isShowModal}>
        <View style={styles.modal}>
          <View style={styles.contentModal}>
            <TouchableOpacity style={styles.btnCancel} onPress={this.onHide}>
              <Image
                source={require('assets/images/exit.png')}
                style={styles.edit}
              />
            </TouchableOpacity>

            <Text style={styles.titleModal}>{title}</Text>
            <Image
              style={styles.image}
              source={
                type === 'fail'
                  ? require('assets/images/icon_refuse.png')
                  : require('assets/images/icon_success.png')
              }
            />
            <Text style={styles.desModal}>{content}</Text>
            {showButton && (
              <View style={styles.bottom}>
                <TouchableOpacity
                  style={styles.btnContinue}
                  onPress={this.onContinue}>
                  <Text style={styles.listenContinue}>{titleAccept}</Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.btnEdit} onPress={this.onCancel}>
                  <Text style={styles.txtEdit}>{titleCancel}</Text>
                </TouchableOpacity>
              </View>
            )}
          </View>
        </View>
      </Modal>
    );
  }
}
export default ModalAlert;

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    ...StyleSheet.absoluteFillObject,
  },
  edit: {
    width: 24,
    height: 24,
    resizeMode: 'cover',
  },
  contentModal: {
    width: '90%',
    paddingBottom: 40,
    backgroundColor: '#182651',
    borderRadius: 8,
  },
  titleModal: {
    fontWeight: '500',
    fontSize: 24,
    lineHeight: 32,
    textAlign: 'center',
    color: '#FFFFFF',
    marginTop: 22,
  },
  btnCancel: {
    position: 'absolute',
    right: 6,
    top: 6,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  desModal: {
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'center',
    color: '#8798CD',
    marginHorizontal: '8%',
    marginTop: 12,
  },
  bottom: {
    marginTop: 16,
    flexDirection: 'row',
    justifyContent: 'space-around',
  },
  btnContinue: {
    width: '40%',
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#8798CD',
  },
  listenContinue: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#8798CD',
  },
  btnEdit: {
    width: '40%',
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: '#FF326F',
  },
  txtEdit: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#FFF',
  },
  groupCheckBox: {
    flex: 1,
    paddingLeft: 24,
    justifyContent: 'center',
  },
  image: {alignSelf: 'center', marginVertical: 8},
});

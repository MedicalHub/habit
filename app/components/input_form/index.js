/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  StyleSheet,
  TextInput,
  ViewPropTypes,
  TouchableOpacity,
  Text,
} from 'react-native';

import PropTypes from 'prop-types';

import Icon from 'app/components/icons';
import {Colors} from 'app/constants';

const hitSlop = {
  top: 10,
  bottom: 10,
  left: 10,
  right: 10,
};
class InputForm extends React.PureComponent<Props> {
  static propTypes = {
    refs: PropTypes.any,
    label: PropTypes.string,
    placeholder: PropTypes.string,
    placeholderTextColor: PropTypes.string,
    inputContainerStyle: ViewPropTypes.style,
    isPasswordForm: PropTypes.bool,
    returnKeyType: PropTypes.string,
    blurOnSubmit: PropTypes.bool,
    enablesReturnKeyAutomatically: PropTypes.bool,
    textContentType: PropTypes.string,
    keyboardType: PropTypes.string,
    onSubmitEditing: PropTypes.any,
    isRequired: PropTypes.bool,
    defaultValue: PropTypes.any,
    clearButtonMode: PropTypes.string,
    noLabel: PropTypes.bool,
    containerStyle: PropTypes.object,
    autoFocus: PropTypes.bool,
  };

  static defaultProps = {
    placeholderTextColor: Colors.PRIMARY_TEXT,
    isPasswordForm: false,
    blurOnSubmit: false,
    textContentType: 'none',
    keyboardType: 'default',
    enablesReturnKeyAutomatically: true,
    noLabel: true,
    showClearButton: true,
    isRequired: false,
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue || '',
      formPasswordToggle: props.isPasswordForm,
      formPasswordToggleColor: props.defaultValue
        ? Colors.PRIMARY_TEXT
        : Colors.INACTIVE_TINT,

      isFocused: false,
    };
  }

  componentDidMount = () => {
    if (this.props.refs) {
      this.props.refs(this);
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (this.props.isPasswordForm && prevState.value !== this.state.value) {
      if (this.state.value !== '') {
        this.setState({formPasswordToggleColor: Colors.PRIMARY_TEXT});
      }
      if (this.state.value === '') {
        this.setState({
          formPasswordToggleColor: Colors.INACTIVE_TINT,
          formPasswordToggle: true,
        });
      }
    }
  }

  onFocus = () => {
    this.setState({isFocused: true});
  };

  onBlur = () => {
    this.setState({isFocused: false});
  };

  onClear = () => {
    this.textInput.clear();
    this.setState({value: ''});
  };

  focus = () => {
    this.textInput.focus();
  };

  onTogglePassword = () => {
    this.setState(state => ({formPasswordToggle: !state.formPasswordToggle}));
  };

  onChangeText = text => {
    this.setState({value: text});
  };
  render() {
    const {
      placeholder,
      inputContainerStyle,
      isPasswordForm,
      returnKeyType,
      onSubmitEditing,
      blurOnSubmit,
      textContentType,
      keyboardType,
      enablesReturnKeyAutomatically,
      isRequired,
      clearButtonMode,
      showClearButton,
      containerStyle,
      autoFocus,
    } = this.props;

    const {
      value,
      formPasswordToggle,
      formPasswordToggleColor,
      isFocused,
    } = this.state;
    let TogglePasswordBtn = null;

    if (isPasswordForm) {
      TogglePasswordBtn = (
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.togglePassword}
          onPress={this.onTogglePassword}
          disabled={!value}>
          <Icon
            type={'System'}
            name={'eye'}
            size={20}
            color={formPasswordToggleColor}
          />
        </TouchableOpacity>
      );
      if (!formPasswordToggle) {
        TogglePasswordBtn = (
          <TouchableOpacity
            hitSlop={hitSlop}
            style={styles.togglePassword}
            onPress={this.onTogglePassword}>
            <Icon
              type={'Feather'}
              name={'eye-off'}
              size={20}
              color={formPasswordToggleColor}
            />
          </TouchableOpacity>
        );
      }
    }

    let ClearBtn = null;
    if (value && showClearButton && isFocused) {
      ClearBtn = (
        <TouchableOpacity
          hitSlop={hitSlop}
          style={styles.clearBtn}
          onPress={this.onClear}>
          <Icon
            type="System"
            name="exit"
            size={14}
            color={Colors.PRIMARY_TEXT}
          />
        </TouchableOpacity>
      );
    }

    let Placeholder = null;
    let Title = <View style={styles.titleCtn} />;
    if (!isFocused && !value) {
      Placeholder = (
        <View style={styles.placeholderCtn} pointerEvents={'none'}>
          <Text style={styles.placeholder}>
            <Text>{placeholder}</Text>
            {isRequired && <Text style={styles.requiredText}> *</Text>}
          </Text>
        </View>
      );
    } else {
      Title = (
        <View style={styles.titleCtn} pointerEvents={'none'}>
          <Text style={styles.title}>
            <Text>{placeholder}</Text>
            {isRequired && <Text style={styles.requiredText}> *</Text>}
          </Text>
        </View>
      );
    }

    return (
      <View style={[styles.formContainer, containerStyle]}>
        {Title}
        <View style={[styles.inputContainer, inputContainerStyle]}>
          <TextInput
            ref={ref => (this.textInput = ref)}
            textContentType={textContentType}
            keyboardType={keyboardType}
            value={value}
            style={styles.textInput}
            // placeholder={placeholder}
            // placeholderTextColor={placeholderTextColor}
            onChangeText={this.onChangeText}
            secureTextEntry={formPasswordToggle}
            returnKeyType={returnKeyType}
            blurOnSubmit={blurOnSubmit}
            onSubmitEditing={() => {
              if (onSubmitEditing) {
                onSubmitEditing();
              }
            }}
            enablesReturnKeyAutomatically={enablesReturnKeyAutomatically}
            clearButtonMode={clearButtonMode}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
            selectionColor={Colors.ACTIVE_TINT}
            autoFocus={autoFocus}
          />
          {Placeholder}
          {ClearBtn}
          {TogglePasswordBtn}
        </View>
      </View>
    );
  }
}
export default InputForm;

const styles = StyleSheet.create({
  formContainer: {
    height: 48,
  },
  formLabel: {
    color: Colors.PRIMARY_TEXT,
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Medium',
    lineHeight: 24,
  },
  inputContainer: {
    height: 32,
    overflow: 'hidden',
    flexDirection: 'row',
    borderBottomColor: Colors.BORDER_PRIMARY,
    borderBottomWidth: 1,
  },
  textInput: {
    flex: 1,
    fontSize: 16,
    color: Colors.LIGHT,
    includeFontPadding: false,
    // textAlignVertical: 'bottom',
    paddingVertical: 0,
  },
  togglePassword: {
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 12,
  },
  isRequired: {
    color: Colors.REQUIRED,
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Regular',
    lineHeight: 24,
  },
  clearBtn: {
    height: '100%',
    justifyContent: 'center',
    marginRight: 12,
  },
  placeholderCtn: {
    flexDirection: 'row',
    position: 'absolute',
    left: 0,
    bottom: 12,
    // top: 5,
  },
  requiredText: {
    color: Colors.ACTIVE_TINT,
  },
  placeholder: {
    fontSize: 16,
    color: Colors.PRIMARY_TEXT,
  },
  titleCtn: {height: 16},
  title: {
    fontSize: 12,
    color: Colors.PRIMARY_TEXT,
  },
});

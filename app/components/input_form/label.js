import React from 'react';
import {Text, StyleSheet} from 'react-native';

import PropTypes from 'prop-types';

import {Colors} from 'app/constants';

class FormLabel extends React.PureComponent<Props> {
  static propTypes = {
    label: PropTypes.string,
    isRequired: PropTypes.bool,
  };

  render() {
    const {label, isRequired} = this.props;

    let IsRequired = null;
    if (isRequired) {
      IsRequired = <Text style={styles.isRequired}>*</Text>;
    }

    return (
      <Text style={styles.formLabel}>
        {label}
        {IsRequired}
      </Text>
    );
  }
}
export default FormLabel;

const styles = StyleSheet.create({
  formLabel: {
    color: Colors.PRIMARY_TEXT,
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Medium',
    lineHeight: 24,
    marginBottom: 8,
  },
  isRequired: {
    color: Colors.REQUIRED,
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Regular',
    lineHeight: 24,
  },
});

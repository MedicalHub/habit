import React, {Component} from 'react';
import {View, StyleSheet, Platform, StatusBar} from 'react-native';
import {getStatusBarHeight, isIphoneX} from 'react-native-iphone-x-helper';

class Header extends Component {
  render() {
    const {style, children} = this.props;
    return <View style={[styles.container, style]}>{children}</View>;
  }
}

export default Header;
const APPBAR_HEIGHT = Platform.OS === 'ios' ? 45 : 51;

export const HEIGHT_HEADER = getStatusBarHeight([]) + APPBAR_HEIGHT;
const styles = StyleSheet.create({
  container: {
    width: '100%',
    height: HEIGHT_HEADER,
    flexDirection: 'row',
    alignItems: 'center',
    paddingTop: getStatusBarHeight(),
    justifyContent: 'space-between',
    // backgroundColor: 'red',
  },
});

import React, {PureComponent} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';

class CheckBox extends PureComponent {
  onPress = () => {
    let {index, onPress} = this.props;
    onPress(index);
  };

  render() {
    let {isSelected, des} = this.props;

    const image = isSelected ? (
      <FastImage
        source={require('assets/images/check_box.png')}
        style={styles.image}
        resizeMode={'cover'}
      />
    ) : (
      <FastImage
        source={require('assets/images/un_check_box.png')}
        style={styles.image}
        resizeMode={'cover'}
      />
    );
    const styleText = isSelected ? styles.des : styles._des;
    return (
      <TouchableOpacity style={styles.container} onPress={this.onPress}>
        {image}
        <Text style={styleText}>{des}</Text>
      </TouchableOpacity>
    );
  }
}

export default CheckBox;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    alignItems: 'center',
    marginBottom: 18,
  },
  image: {
    width: 24,
    height: 24,
  },
  des: {
    fontSize: 16,
    lineHeight: 19,
    color: '#FF326F',
    marginLeft: 12,
  },
  _des: {
    fontSize: 16,
    lineHeight: 19,
    color: '#8798CD',
    marginLeft: 12,
  },
});

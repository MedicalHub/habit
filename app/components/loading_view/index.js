import React from 'react';
import {View, StyleSheet, ActivityIndicator} from 'react-native';

const LoadingView = () => {
  return (
    <View style={styles.container}>
      <ActivityIndicator />
    </View>
  );
};
export default LoadingView;
const styles = StyleSheet.create({
  container: {
    justifyContent: 'center',
    alignItems: 'center',
    flex: 1,
  },
});

import React from 'react';
import {ActivityIndicator, StyleSheet, View, ViewPropTypes} from 'react-native';

import PropTypes from 'prop-types';
import Spinner from 'react-native-spinkit';

import {changeOpacity} from 'app/utils/theme';
import {Colors} from 'app/constants';

export default class Loading extends React.PureComponent {
  static propTypes = {
    color: PropTypes.string,
    isLoading: PropTypes.any,
    refs: PropTypes.any,
    size: PropTypes.oneOfType([PropTypes.string, PropTypes.number]),
    style: PropTypes.object,
    containerStyle: PropTypes.oneOfType([
      ViewPropTypes.style,
      PropTypes.object,
      PropTypes.array,
    ]),
    type: PropTypes.string,
  };

  static defaultProps = {
    size: 50,
    color: Colors.ACTIVE_TINT,
    style: {},
    type: 'ThreeBounce',
  };

  constructor(props) {
    super(props);
    this.state = {
      loading: props.isLoading || false,
    };
  }

  componentDidMount() {
    if (this.props.refs) {
      this.props.refs(this);
    }
  }

  componentWillUnmount() {
    if (this.state.loading) {
      this.stop();
    }
  }

  start = () => {
    this.setState({loading: true});
  };

  stop = () => {
    this.setState({loading: false});
  };

  render() {
    const {color, containerStyle, type, size} = this.props;
    const {loading} = this.state;
    if (!loading) {
      return null;
    }

    let Content = <ActivityIndicator size={size} color={color} />;
    if (type !== 'original') {
      Content = (
        <Spinner isVisible={true} type={type} color={color} size={size} />
      );
    }
    return (
      <View pointerEvents="box-only" style={[styles.container, containerStyle]}>
        {Content}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    ...StyleSheet.absoluteFillObject,
    justifyContent: 'center',
    alignItems: 'center',
  },

  contentContainer: {
    borderRadius: 8,
    backgroundColor: changeOpacity('#FFF', 0.8),
    padding: 15,
    elevation: undefined,
  },

  text: {
    color: changeOpacity('#FFF', 1),
  },
});

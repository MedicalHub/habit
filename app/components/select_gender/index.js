import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import GroupCheckBox from 'app/components/checkbox/group_checkbox';

const GENDER_DATA = [
  {
    id: 'M',
    titleId: 'form.gender.male',
    titleDefMsg: 'Nam',
  },
  {
    id: 'F',
    titleId: 'form.gender.female',
    titleDefMsg: 'Nữ',
  },
];

class SelectGender extends React.PureComponent<Props> {
  constructor(props) {
    super(props);

    this.GENDER_DATA = [
      {
        id: 'M',
        titleId: 'form.gender.male',
        titleDefMsg: 'Nam',
        selected: props.defaultValue === 'M' ? true : false,
      },
      {
        id: 'F',
        titleId: 'form.gender.female',
        titleDefMsg: 'Nữ',
        selected: props.defaultValue === 'F' ? true : false,
      },
    ];

    this.state = {
      gender: props.defaultValue || 'M',
    };
  }

  componentDidMount() {
    if (this.props.refs) {
      this.props.refs(this);
    }
  }

  selectGender = item => {
    this.setState({gender: item.id});
  };
  render() {
    return (
      <GroupCheckBox
        onSelect={this.selectGender}
        listStyle={styles.container}
        data={this.GENDER_DATA}
      />
    );
  }
}
export default SelectGender;

const styles = StyleSheet.create({
  container: {
    paddingBottom: 16,
  },
});

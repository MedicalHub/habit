import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

import PropTypes from 'prop-types';

import {Colors} from 'app/constants';
import Icon from 'app/components/icons';

class PickerForm extends React.PureComponent<Props> {
  static propTypes = {
    label: PropTypes.string,
    rightIcon: PropTypes.any,
    onPress: PropTypes.func,
  };
  static defaultProps = {
    onPress: () => {},
  };

  static defaultProps = {
    rightIcon: (
      <Icon
        type={'Ionicons'}
        name={'ios-arrow-down'}
        size={20}
        color={Colors.INACTIVE_TINT}
      />
    ),
  };
  render() {
    const {label, rightIcon, onPress} = this.props;
    return (
      <TouchableOpacity
        style={styles.container}
        activeOpacity={0.7}
        onPress={onPress}>
        <Text style={styles.label}>{label}</Text>
        {rightIcon}
      </TouchableOpacity>
    );
  }
}
export default PickerForm;

const styles = StyleSheet.create({
  container: {
    backgroundColor: '#FFFFFF',
    flexDirection: 'row',
    paddingHorizontal: 12,
    alignItems: 'center',
    height: 44,
    borderWidth: 1,
    borderRadius: 8,
    borderColor: Colors.BORDER_PRIMARY,
    justifyContent: 'space-between',
  },
  label: {
    fontFamily: 'iCielHelveticaNowText-Regular',
    fontSize: 14,
    color: Colors.INACTIVE_TINT,
  },
});

import React from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';

import PropTypes from 'prop-types';

import AppIcon from './app_icon';
import {Colors} from 'app/constants';

export default function HeaderRight(props) {
  const {source, color, onPress} = props;
  return (
    <TouchableOpacity style={styles.container} onPress={onPress}>
      <AppIcon source={source} color={color} />
    </TouchableOpacity>
  );
}

HeaderRight.propTypes = {
  color: PropTypes.string,
  source: PropTypes.any,
  onPress: PropTypes.func,
};

HeaderRight.defaultProps = {
  source: require('assets/images/arrow-left.png'),
  color: Colors.PRIMARY_TEXT,
  onPress: () => {},
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
});

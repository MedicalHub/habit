import React from 'react';
import {View, StyleSheet} from 'react-native';

import PropTypes from 'prop-types';

import AppIcon from './app_icon';
import {Colors} from 'app/constants';

export default function HeaderLeft(props) {
  const {source, color} = props;
  return (
    <View style={styles.container}>
      <AppIcon source={source} color={color} />
    </View>
  );
}

HeaderLeft.propTypes = {
  color: PropTypes.string,
  source: PropTypes.any,
};

HeaderLeft.defaultProps = {
  source: require('assets/images/arrow-left.png'),
  color: Colors.PRIMARY_TEXT,
};

const styles = StyleSheet.create({
  container: {
    height: '100%',
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
});

import React from 'react';
import {View, StyleSheet, ViewPropTypes} from 'react-native';

import PropTypes from 'prop-types';

class Separator extends React.PureComponent<Props> {
  static propTypes = {
    customStyle: ViewPropTypes.style,
    width: PropTypes.number,
    height: PropTypes.number,
    backgroundColor: PropTypes.string,
  };
  render() {
    const {height, width, customStyle, backgroundColor} = this.props;
    return (
      <View
        style={[styles.style, {height, width, backgroundColor}, customStyle]}
      />
    );
  }
}
export default Separator;

const styles = StyleSheet.create({
  container: {
    height: 16,
  },
});

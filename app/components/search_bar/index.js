import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity,
} from 'react-native';

import PropTypes from 'prop-types';

import {Colors} from 'app/constants';
import Icon from 'app/components/icons';

class SearchBar extends React.PureComponent<Props> {
  static propTypes = {
    placeholder: PropTypes.string,
  };
  static defaultProps = {
    placeholder: 'Nhập nội dung tìm kiếm',
  };

  constructor(props) {
    super(props);
    this.state = {
      value: props.defaultValue || '',
      isFocused: false,
    };
  }

  onFocus = () => {
    this.setState({isFocused: true});
  };

  onBlur = () => {
    this.setState({isFocused: false});
  };
  // TODO:
  onClear = () => {
    this.searchBarTextInput.clear();
    this.setState({value: ''});
  };

  // TODO:
  onChangeText = text => {
    this.setState({value: text});
    this.props.onChangeText(text);
  };

  render() {
    const {value, isFocused} = this.state;
    const {placeholder, style, noEditable} = this.props;

    let ClearBtn = null;
    if (value && isFocused) {
      ClearBtn = (
        <TouchableOpacity style={styles.clearBtn} onPress={this.onClear}>
          <Icon
            type="System"
            name="exit"
            size={16}
            color={Colors.INACTIVE_TINT}
          />
        </TouchableOpacity>
      );
    }
    return (
      <View style={[styles.container, style]}>
        <Icon
          type="System"
          name="search"
          size={16}
          color={Colors.INACTIVE_TINT}
        />

        {!noEditable ? (
          <TextInput
            ref={ref => (this.searchBarTextInput = ref)}
            style={styles.textInput}
            value={value}
            onChangeText={this.onChangeText}
            placeholder={placeholder}
            placeholderTextColor={Colors.INACTIVE_TINT}
            onFocus={this.onFocus}
            onBlur={this.onBlur}
          />
        ) : (
          <Text style={styles.placeholder}>{placeholder}</Text>
        )}
        {ClearBtn}
      </View>
    );
  }
}

export default SearchBar;

const styles = StyleSheet.create({
  container: {
    height: 44,
    borderRadius: 8,
    backgroundColor: '#FFF',
    borderWidth: 1,
    borderColor: Colors.BORDER_PRIMARY,
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 12,
  },
  textInput: {
    height: '100%',
    flex: 1,
    color: Colors.PRIMARY_TEXT,
    fontSize: 14,
    fontFamily: 'iCielHelveticaNowText-Regular',
    paddingLeft: 12,
    margin: 0,
    padding: 0,
  },
  clearBtn: {
    height: '100%',
    paddingHorizontal: 12,
    justifyContent: 'center',
  },
  placeholder: {
    paddingLeft: 12,
    color: Colors.INACTIVE_TINT,
  },
});

import React from 'react';
import {TouchableOpacity, View} from 'react-native';

import PropTypes from 'prop-types';

import {Colors} from 'app/constants';
import {pSBC, colourNameToHex} from 'app/utils/colors';

class Touchable extends React.PureComponent<Props> {
  static propTypes = {
    onPress: PropTypes.func,
    shade: PropTypes.number,
  };

  static defaultProps = {
    onPress: () => {},
  };

  constructor(props) {
    super(props);
    let backgroundColor = 'transparent';
    if (props.style && props.style.backgroundColor) {
      let color = props.style.backgroundColor;
      if (typeof color === 'string') {
        if (color[0] !== 'r' && color[0] !== '#') {
          color = colourNameToHex(color);
        }
      }
      backgroundColor = color;
    }

    this.defaultColor = backgroundColor;
    this.state = {
      backgroundColor,
    };
  }

  onPressIn = () => {
    if (this.state.backgroundColor === 'transparent') {
      this.setState({
        backgroundColor: pSBC(this.props.shade || -0.2, Colors.UNDERLIGHT),
      });
    } else {
      this.setState({
        backgroundColor: pSBC(
          this.props.shade || -0.6,
          this.state.backgroundColor,
        ),
      });
    }
  };
  onPressOut = () => {
    this.setState({
      backgroundColor: this.defaultColor,
    });
  };

  render() {
    const {onPress, children, style} = this.props;
    const {backgroundColor} = this.state;
    return (
      <TouchableOpacity
        {...this.props}
        activeOpacity={1}
        onPress={onPress}
        onPressIn={this.onPressIn}
        onPressOut={this.onPressOut}
        style={[
          style,
          {
            backgroundColor,
          },
        ]}>
        {children}
      </TouchableOpacity>
    );
  }
}
export default Touchable;

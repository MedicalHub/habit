/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ViewPropTypes,
} from 'react-native';

import PropTypes from 'prop-types';

import {Colors} from 'app/constants';

import {verticalScale} from 'app/utils/screen';
import AppIcon from 'app/components/app_icon';

class ProfileItem extends React.PureComponent<Props> {
  static propTypes = {
    iconSource: PropTypes.node.isRequired,
    label: PropTypes.string.isRequired,
    contentStyle: ViewPropTypes.style,
    onPress: PropTypes.func,
    isBorder: PropTypes.bool,
  };

  static defaultProps = {
    isBorder: true,
    onPress: () => {},
  };
  render() {
    const {label, iconSource, contentStyle, onPress, isBorder} = this.props;
    return (
      <TouchableOpacity style={styles.container} onPress={onPress}>
        <View
          style={[
            styles.content,
            contentStyle,
            {
              borderBottomWidth: isBorder ? 1 : 0,
            },
          ]}>
          <AppIcon source={iconSource} color={Colors.INACTIVE_TINT} />
          <View style={styles.body}>
            <Text
              adjustsFontSizeToFit={true}
              numberOfLines={2}
              style={styles.label}>
              {label}
            </Text>
          </View>
          <AppIcon
            source={require('assets/images/arrow-right.png')}
            color={Colors.INACTIVE_TINT}
          />
        </View>
      </TouchableOpacity>
    );
  }
}
export default ProfileItem;

const styles = StyleSheet.create({
  container: {
    paddingHorizontal: 16,
    // backgroundColor: '#FFFFFF',
  },
  content: {
    flexDirection: 'row',
    alignItems: 'center',
    height: verticalScale(56),
    borderBottomColor: Colors.BORDER_PRIMARY,
    borderBottomWidth: 1,
  },
  body: {
    flex: 1,
    height: '100%',
    justifyContent: 'center',
    paddingLeft: 16,
    paddingRight: 10,
  },
  label: {
    fontSize: 16,
    color: Colors.PRIMARY_TEXT,
  },
});

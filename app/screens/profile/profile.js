import React from 'react';
import {
  StyleSheet,
  ScrollView,
  RefreshControl,
  InteractionManager,
} from 'react-native';

import {LoginManager, AccessToken} from 'react-native-fbsdk';
import {GoogleSignin} from '@react-native-community/google-signin';

import ProfileItem from 'app/screens/profile/profile_item';
import Separator from 'app/components/separator';
import TouchableText from 'app/components/touchable_text';
import HeaderProfile from './header';

import {verticalScale} from 'app/utils/screen';
import {Colors} from 'app/constants';
import ModalAlert from 'app/components/modal_alert';
import moment from 'moment';

class Profile extends React.PureComponent<Props> {
  componentDidMount() {
    this.props.actions.getStatistics(this.props.userId);
  }
  onLogout = async () => {
    const {navigation, actions} = this.props;

    try {
      /// facebook
      const data = await AccessToken.getCurrentAccessToken();
      if (data) {
        LoginManager.logOut(); // logout facebook
      }

      // google
      const {accessToken, idToken} = await GoogleSignin.getTokens();
      if (accessToken || idToken) {
        await GoogleSignin.revokeAccess();
        await GoogleSignin.signOut();
      }
    } catch (error) {
      console.log('error-logout', error);
    } finally {
      actions.logout();
      navigation.navigate('Login');
    }
  };
  onRefresh = () => {
    const {actions, userId} = this.props;

    InteractionManager.runAfterInteractions(() => {
      /*     // 2: Component is done animating */
      /*     // 3: Start fetching the team */
      actions.getProfileInfo(userId);
      actions.getStatistics(userId);
    });
  };

  showOrder = () => {
    const isExpired = this.props.expiredTime
      ? new Date(this.props.expiredTime).getTime() > new Date().getTime()
        ? true
        : false
      : false;

    if (isExpired) {
      this.ModalRef.onShow();
    } else {
      this.props.navigation.navigate('OrderStack');
    }
  };

  render() {
    const {screenProps, navigation, isLoading, expiredTime} = this.props;
    const {intl} = screenProps;
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contenContainer}
        refreshControl={
          <RefreshControl
            tintColor={Colors.LIGHT}
            refreshing={isLoading}
            onRefresh={this.onRefresh}
          />
        }>
        <HeaderProfile expiredTime={expiredTime} />
        <Separator height={16} />
        <ProfileItem
          iconSource={require('assets/images/cart.png')}
          label={intl.formatMessage({
            id: 'activeAccount',
            defaultMessage: 'Kích hoạt tài khoản',
          })}
          onPress={() => navigation.navigate('ActiveCode')}
        />
        <ProfileItem
          iconSource={require('assets/images/user.png')}
          label={intl.formatMessage({
            id: 'profileInfo',
            defaultMessage: 'Thông tin cá nhân',
          })}
          onPress={() => navigation.navigate('ProfileInfo')}
        />
        <ProfileItem
          iconSource={require('assets/images/history.png')}
          label={intl.formatMessage({
            id: 'transactionHistory',
            defaultMessage: 'Lịch sử giao dịch',
          })}
          onPress={() => navigation.navigate('TransactionHistory')}
        />
        <ProfileItem
          iconSource={require('assets/images/lock.png')}
          label={intl.formatMessage({
            id: 'changePassword',
            defaultMessage: 'Đổi mật khẩu',
          })}
          // isBorder={false}
          onPress={() => navigation.navigate('ChangePassword')}
        />
        <ProfileItem
          iconSource={require('assets/images/cart.png')}
          label={intl.formatMessage({
            id: 'order',
            defaultMessage: 'Tiến hành đặt hàng',
          })}
          onPress={this.showOrder}
          isBorder={false}
        />
        <TouchableText
          label={intl.formatMessage({
            id: 'logout',
            defaultMessage: 'Đăng xuất',
          })}
          style={styles.logoutText}
          position={'center'}
          onPress={this.onLogout}
        />
        <ModalAlert
          ref={ref => (this.ModalRef = ref)}
          title={'Thông báo'}
          type="success"
          content={`Bạn đã hích hoạt tài khoản, han sử dụng đến ${moment(
            expiredTime,
          ).format('DD/MM/YYYY')}`}
        />
      </ScrollView>
    );
  }
}

export default Profile;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },
  contenContainer: {
    paddingVertical: 8,
    paddingBottom: 16,
  },
  logoutText: {
    fontSize: 16,
    textTransform: 'uppercase',
    fontWeight: '500',
    marginTop: verticalScale(40),
  },
});

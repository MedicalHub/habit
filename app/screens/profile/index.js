import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getUserInfo} from 'app/selectors/user';
import {logout, getProfileInfo, getStatistics} from 'app/actions/user';

import Profile from 'app/screens/profile/profile';

const mapStateToProps = state => {
  const isLoading = state.request.getProfileInfo.loading;
  const error = state.request.getProfileInfo.error;
  const userId = getUserInfo(state)._id;
  const expiredTime = getUserInfo(state)?.expired_time || null;

  return {
    isLoading,
    error,
    userId,
    expiredTime,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getProfileInfo: getProfileInfo,
        logout: logout,
        getStatistics,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Profile);

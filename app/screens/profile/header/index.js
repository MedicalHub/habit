import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import Header from 'app/screens/profile/header/header';

import {getUserInfo} from 'app/selectors/user';

const mapStateToProps = state => {
  const user = getUserInfo(state);

  const completedTopicCount = state.user.statistics.completedTopicCount;
  const completedTopicItemCount = state.user.statistics.completedTopicItemCount;
  return {
    fullname: user.fullname,
    avatar: user.avatar,
    completedTopicCount,
    completedTopicItemCount,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({}, dispatch),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Header);

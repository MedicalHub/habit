import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {Colors, General} from 'app/constants';
import Separator from 'app/components/separator';
import FormattedText from 'app/components/formatted_text';
import Avatar from 'app/components/avatar';
import moment from 'moment';

class HeaderProfile extends React.PureComponent<Props> {
  render() {
    const {
      fullname,
      completedTopicCount,
      completedTopicItemCount,
      expiredTime,
    } = this.props;

    const isExpired = expiredTime
      ? new Date(expiredTime).getTime() > new Date().getTime()
        ? true
        : false
      : false;

    return (
      <View style={styles.container}>
        <View style={styles.topCtn}>
          <Avatar />
          <View style={styles.topRight}>
            <Text style={styles.name}>{fullname || General.DEFAULT_NAME}</Text>
            <Separator height={8} />
            <Text style={styles.expired}>
              <FormattedText id="expired" defaultMessage="Hết hạn" />
              <Text>: </Text>
              {isExpired ? (
                <Text>{moment(expiredTime).format('DD/MM/YYYY')}</Text>
              ) : (
                <Text style={styles.inActive}>Chưa kích hoạt</Text>
              )}
            </Text>
          </View>
        </View>
        <Separator customStyle={styles.separator} />
        <View style={styles.bottomCtn}>
          <View style={styles.bottom}>
            <FormattedText
              id="habitCompleted"
              defaultMessage="Thói quen hoàn thành"
              style={styles.btmHeader}
            />

            <Text style={styles.count}>{completedTopicCount}</Text>
          </View>
          <View style={styles.bottom}>
            <FormattedText
              id="practiceTotal"
              defaultMessage="Tổng ngày luyện tập"
              style={styles.btmHeader}
            />

            <Text style={styles.count}>{completedTopicItemCount}</Text>
          </View>
        </View>
      </View>
    );
  }
}
export default HeaderProfile;

const styles = StyleSheet.create({
  container: {
    height: 218,
    borderRadius: 8,
    backgroundColor: Colors.PRIMARY_2,
    marginHorizontal: 16,
    paddingHorizontal: 16,
    paddingVertical: 20,
  },
  topCtn: {
    flexDirection: 'row',
  },
  bottomCtn: {
    flexDirection: 'row',
    paddingHorizontal: 22,
    justifyContent: 'space-between',
  },
  topRight: {
    justifyContent: 'center',
  },
  name: {
    color: Colors.LIGHT,
    fontSize: 18,
    fontWeight: '600',
  },
  expired: {
    fontSize: 14,
    color: '#FFBE00',
  },
  avaProfile: {
    height: 80,
    width: 80,
    borderRadius: 8,
    marginRight: 25,
  },
  separator: {
    height: 1,
    marginVertical: 16,
    backgroundColor: Colors.BORDER_PRIMARY,
  },
  bottom: {
    alignItems: 'center',
  },
  btmHeader: {
    fontSize: 12,
    color: Colors.PRIMARY_TEXT,
    lineHeight: 14,
    marginBottom: 12,
  },
  count: {
    fontSize: 36,
    fontWeight: '600',
    color: '#FFBE00',
    lineHeight: 43,
  },
  inActive: {fontSize: 14, color: Colors.LIGHT},
});

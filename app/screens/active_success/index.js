import React from 'react';
import {View, Text, StyleSheet, ImageBackground} from 'react-native';

import Button from 'app/components/button';
import {Colors, Screen} from 'app/constants';
import FormattedText from 'app/components/formatted_text';
import moment from 'moment';

class ActiveSuccess extends React.PureComponent<Props> {
  render() {
    const {navigation, screenProps} = this.props;
    const {intl} = screenProps;

    let title = '';
    let message = '';
    const orderSuccess = navigation.getParam('orderSuccess');
    const resultQrcode = navigation.getParam('resultQrcode');
    console.log('resultQrcode ', resultQrcode);
    let expiredDate = resultQrcode?.expired_time || null;

    if (orderSuccess) {
      title = intl.formatMessage({
        id: 'orderSuccess',
        defaultMessage: 'Đặt hàng thành công',
      });
      message = intl.formatMessage({
        id: 'orderSuccessMessage',
        defaultMessage:
          'Chúc mừng bạn đã đặt hàng thành công chúng tôi sẽ kiểm tra và liên lạc với trong thời gian sớm nhất. Xin cảm ơn!',
      });
    } else {
      title = intl.formatMessage({
        id: 'activeSucces',
        defaultMessage: 'Kích hoạt thành công',
      });
      message = intl.formatMessage({
        id: 'activeSuccesMessage',
        defaultMessage:
          'Chúc mừng bạn đã kích hoạt tài khoản thành công tài khoản của bạn có thời gian sử dụng đến hết ngày',
      });
      expiredDate = (
        <Text style={styles.expiredDate}>
          {' '}
          {moment(expiredDate).format('DD/MM/YYYY')}
        </Text>
      );
    }
    return (
      <View style={styles.container}>
        <ImageBackground
          style={styles.imageContainer}
          resizeMode="contain"
          source={require('assets/images/active-success-bg.png')}>
          <Text style={styles.activeSucces}>{title}</Text>
          <Text style={styles.activeSuccesMessage}>
            {message}
            {expiredDate}
          </Text>
        </ImageBackground>
        <Button
          label={intl.formatMessage({
            id: 'comeBackHome',
            defaultMessage: 'Trang Chủ',
          })}
          onPress={() => navigation.goBack(null)}
          containerStyle={styles.comebackHome}
        />
      </View>
    );
  }
}
export default ActiveSuccess;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },
  imageContainer: {
    flex: 1,
    width: Screen.SCREEN_WIDTH,
    height: Screen.SCREEN_HEIGHT,
    justifyContent: 'center',
    alignItems: 'center',
    paddingTop: 24,
    paddingHorizontal: 24,
  },
  activeSucces: {
    fontSize: 22,
    color: Colors.SUCCESS_PRIMARY,
    fontWeight: '700',
    textTransform: 'capitalize',
  },
  activeSuccesMessage: {
    fontSize: 16,
    color: Colors.PRIMARY_TEXT,
    textAlign: 'center',
    marginTop: 16,
    lineHeight: 24,
  },
  expiredDate: {
    fontSize: 14,
    color: Colors.TIME_ACTIVE_ACCOUNT,
  },
  comebackHome: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    right: 24,
    left: 24,
  },
});

import React, {memo, useCallback} from 'react';
import {
  ImageBackground,
  StyleSheet,
  Text,
  TouchableOpacity,
  View,
} from 'react-native';
import API from 'app/saga/api';
import {Colors} from 'app/constants';

const ItemCategorySearch = memo(props => {
  const onDetailCategoryItem = useCallback(() => {
    const {id, navigation, title} = props;
    navigation.navigate('DetailCategory', {
      categoryId: id,
      title: title,
    });
  }, [props]);

  return (
    <TouchableOpacity onPress={onDetailCategoryItem}>
      <ImageBackground
        source={{uri: API.urlImage(props.thumbnail)}}
        resizeMode="cover"
        style={styles.container}>
        <View style={styles.rectangle}>
          <Text style={styles.title}>{props.title}</Text>
        </View>
      </ImageBackground>
    </TouchableOpacity>
  );
});

export default ItemCategorySearch;

const styles = StyleSheet.create({
  container: {
    height: 100,
    borderRadius: 8,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    color: Colors.LIGHT,
    fontWeight: '500',
    marginHorizontal: 16,
  },
  rectangle: {
    backgroundColor: 'rgba(15, 27, 61,0.35)',
    flex: 1,
    justifyContent: 'center',
  },
});

import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet} from 'react-native';
import HeaderLeft from 'app/components/header_left';
import {Colors} from 'app/constants';
import SearchBar from 'app/components/search_bar';
import Header from 'app/components/header';

class HeaderSearch extends Component {
  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    return (
      <Header>
        <TouchableOpacity onPress={this.goBack}>
          <HeaderLeft
            source={require('assets/images/arrow-left.png')}
            color={Colors.PRIMARY_TEXT}
          />
        </TouchableOpacity>
        <SearchBar
          style={styles.searchBar}
          onChangeText={this.props.onChangeText}
        />
      </Header>
    );
  }
}
export default HeaderSearch;
const styles = StyleSheet.create({
  searchBar: {
    backgroundColor: Colors.PRIMARY_2,
    flex: 1,
    marginRight: 16,
    height: 34,
  },
});

import React, {Component} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {Colors} from 'app/constants';
import Separator from 'app/components/separator';
import HeaderSearch from 'app/screens/search_category/header';
import {connect} from 'react-redux';
import _ from 'lodash';
import ItemCategorySearch from 'app/screens/search_category/item';

class SearchCategory extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: this.props.data,
    };
  }

  onChangeText = _.debounce(text => {
    const _data = this.props.data.filter(element => {
      let delete_accents_title = delete_accents(element.title.toUpperCase());
      let delete_accents_text = delete_accents(text.toUpperCase());
      return delete_accents_title.includes(delete_accents_text);
    });
    if (_data) {
      this.setState({data: _data});
    } else {
      this.setState({data: []});
    }
  }, 1000);

  renderItem = ({item}) => {
    const {navigation} = this.props;
    return (
      <ItemCategorySearch
        title={item.title}
        thumbnail={item.thumbnail}
        id={item._id}
        navigation={navigation}
      />
    );
  };

  keyExtractor = (item, index) => index.toString();

  renderSeparator = () => <Separator height={16} />;

  render() {
    const {navigation} = this.props;
    return (
      <View style={styles.container}>
        <HeaderSearch
          navigation={navigation}
          onChangeText={this.onChangeText}
        />
        <FlatList
          data={this.state.data}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.renderSeparator}
          style={styles.containerFlatList}
        />
      </View>
    );
  }
}

export const delete_accents = str => {
  str = str.replace(/à|á|ạ|ả|ã|â|ầ|ấ|ậ|ẩ|ẫ|ă|ằ|ắ|ặ|ẳ|ẵ/g, 'a');
  str = str.replace(/è|é|ẹ|ẻ|ẽ|ê|ề|ế|ệ|ể|ễ/g, 'e');
  str = str.replace(/ì|í|ị|ỉ|ĩ/g, 'i');
  str = str.replace(/ò|ó|ọ|ỏ|õ|ô|ồ|ố|ộ|ổ|ỗ|ơ|ờ|ớ|ợ|ở|ỡ/g, 'o');
  str = str.replace(/ù|ú|ụ|ủ|ũ|ư|ừ|ứ|ự|ử|ữ/g, 'u');
  str = str.replace(/ỳ|ý|ỵ|ỷ|ỹ/g, 'y');
  str = str.replace(/đ/g, 'd');
  str = str.replace(/À|Á|Ạ|Ả|Ã|Â|Ầ|Ấ|Ậ|Ẩ|Ẫ|Ă|Ằ|Ắ|Ặ|Ẳ|Ẵ/g, 'A');
  str = str.replace(/È|É|Ẹ|Ẻ|Ẽ|Ê|Ề|Ế|Ệ|Ể|Ễ/g, 'E');
  str = str.replace(/Ì|Í|Ị|Ỉ|Ĩ/g, 'I');
  str = str.replace(/Ò|Ó|Ọ|Ỏ|Õ|Ô|Ồ|Ố|Ộ|Ổ|Ỗ|Ơ|Ờ|Ớ|Ợ|Ở|Ỡ/g, 'O');
  str = str.replace(/Ù|Ú|Ụ|Ủ|Ũ|Ư|Ừ|Ứ|Ự|Ử|Ữ/g, 'U');
  str = str.replace(/Ỳ|Ý|Ỵ|Ỷ|Ỹ/g, 'Y');
  str = str.replace(/Đ/g, 'D');
  return str;
};

const mapStateToProps = state => {
  const data = state.category.data;

  return {
    data,
  };
};

export default connect(mapStateToProps)(SearchCategory);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },

  containerFlatList: {
    marginHorizontal: 16,
    paddingTop: 16,
  },
});

import React from 'react';
import {View, Text, StyleSheet, Image, ScrollView} from 'react-native';

import TouchableText from 'app/components/touchable_text';

import {Colors, Screen} from 'app/constants';
import FormattedText from 'app/components/formatted_text';

class SplashIntro extends React.PureComponent<Props> {
  static navigationOptions = ({navigation, screenProps}) => {
    const headerRight = () => (
      <TouchableText
        label={screenProps.intl.formatMessage({
          id: 'title.skip',
          defaultMessage: 'Bỏ qua',
        })}
        onPress={() => navigation.navigate('Login')}
        style={{marginRight: 16}}
      />
    );
    return {
      headerRight,
    };
  };
  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}>
        <View style={styles.imageContainer}>
          <Image
            source={require('assets/images/splash_intro.png')}
            resizeMode={'cover'}
            style={styles.image}
          />
        </View>
        <View style={styles.content}>
          <FormattedText
            id="splashIntro.title"
            defaultMessage="Cùng bạn vươn tới thành công"
            style={styles.title}
          />
          <Text style={styles.contentText}>{CONTENT}</Text>
        </View>
      </ScrollView>
    );
  }
}
export default SplashIntro;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentContainer: {
    paddingBottom: Screen.PADDING_BOTTOM,
  },
  imageContainer: {
    width: Screen.SCREEN_WIDTH,
  },
  image: {
    width: null,
    flex: 1,
  },
  content: {
    paddingHorizontal: 24,
  },
  title: {
    fontSize: 22,
    fontWeight: '500',
    color: '#FFBE00',
    marginTop: 40,
  },
  contentText: {
    fontSize: 14,
    color: Colors.PRIMARY_TEXT,
    marginTop: 16,
    lineHeight: 24,
  },
});

const CONTENT = `Ứng dụng Thói quen là ứng dụng hỗ trợ người dùng hình thành và phát triển các thói quen mới sau 30 ngày. 
Người dùng có thể lựa chọn để phát triển các thói quen theo nhóm: nhóm thói quen học tập (thói quen đọc sách, thói quen học tiếng Anh, thói quen học cái mới…), nhóm thói quen phát triển bản thân (thói quen dậy sớm, thói quen kiên nhẫn…), nhóm thói quen hạnh phúc (thói quen sống tích cực, thói quen thiền định…), nhóm thói quen công việc (thói quen quản lý thời gian, thói quen lập kế hoạch), nhóm thói quen triệu phú (thói quen tiết kiệm, thói quen chi tiêu hiệu quả…) hoặc lựa chọn theo từng thói quen riêng lẻ.
Ứng dụng thói quen cung cấp cho bạn:
• Hướng dẫn người dùng cách hình thành một thói quen sau 30 ngày
• Tạo lập các mục tiêu theo ngày để người dùng làm quen với thói quen 
• Khen thưởng người dùng mỗi khi người dùng hoàn thành một mục tiêu theo ngày
• Hỗ trợ người dùng chia sẻ với bạn bè, người thân`;

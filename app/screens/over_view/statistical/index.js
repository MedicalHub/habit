import React, {Component} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import {Colors} from 'app/constants';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import LinearGradient from 'react-native-linear-gradient';
import {Circle} from 'react-native-svg';

class Statistical extends Component {
  renderCap = ({center}) => (
    <Circle cx={center.x} cy={center.y} r="14" fill={'#FFBE00'} />
  );
  render() {
    const {progress, number_not_complete, number_complete} = this.props;

    return (
      <LinearGradient
        colors={['rgba(19,26,64,0.9)', 'rgba(24,38,81,0.36)']}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}>
        <View style={styles.overView}>
          <View style={styles.circle}>
            <AnimatedCircularProgress
              size={148}
              width={10}
              fill={progress}
              tintColor="#FFBE00"
              rotation={360}
              // onAnimationComplete={() => console.log('onAnimationComplete')}
              backgroundColor={Colors.PRIMARY_3}
              padding={10}
              renderCap={this.renderCap}>
              {fill => <Text style={styles.process}>{progress}%</Text>}
            </AnimatedCircularProgress>
          </View>
          <View>
            <Text style={styles.dayComplete}>Ngày hoàn thành</Text>
            <Text style={styles.numberActive}>
              {number_complete} <Text style={styles.unit}>ngày</Text>
            </Text>
            <View style={styles.line} />
            <Text style={styles.dayComplete}>Ngày đã bỏ</Text>
            <Text style={styles.numberInActive}>
              {number_not_complete} <Text style={styles.unit}>ngày</Text>
            </Text>
          </View>
        </View>
      </LinearGradient>
    );
  }
}
export default Statistical;
const styles = StyleSheet.create({
  process: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#FFC500',
  },
  circle: {
    width: 168,
    height: 168,
    borderRadius: 84,
    backgroundColor: 'rgba(135,152,205,0.05)',
    justifyContent: 'center',
    alignItems: 'center',
    marginRight: 32,
  },
  dayComplete: {
    color: '#8798CD',
    fontSize: 10,
    lineHeight: 12,
    textTransform: 'uppercase',
  },
  numberActive: {
    fontWeight: '500',
    fontSize: 22,
    lineHeight: 30,
    color: '#00DA4A',
    marginTop: 6,
  },
  unit: {
    fontSize: 14,
    color: '#8798CD',
  },
  line: {
    width: 120,
    height: 1,
    backgroundColor: '#182651',
    marginVertical: 16,
  },
  numberInActive: {
    fontWeight: '500',
    fontSize: 22,
    lineHeight: 30,
    color: '#FF0000',
    marginTop: 6,
  },
  overView: {
    flexDirection: 'row',
    alignItems: 'center',
    padding: 16,
  },
});

import React, {PureComponent} from 'react';
import {View, StyleSheet, ScrollView} from 'react-native';

class Table extends PureComponent {
  constructor(props) {
    super(props);
    const {length} = this.props;
    this.numberHeight = 5;
    if (length > 34) {
      this.numberHeight = parseInt(length / 7) + 1;
    }
  }

  renderLine = () => {
    let view = [];
    for (let i = 1; i < this.numberHeight; i++) {
      view.push(<View style={[styles.line1, {top: top * i}]} />);
    }
    return view;
  };

  render() {
    return (
      <View style={styles.table}>
        <View
          style={[
            styles.containerStyle,
            {
              height: top * this.numberHeight,
            },
          ]}>
          {this.props.children}

          <View style={styles.column} />
          <View style={styles.column1} />
          <View style={styles.column2} />
          <View style={styles.column3} />
          <View style={styles.column4} />
          <View style={styles.column5} />
          {this.renderLine()}
        </View>
      </View>
    );
  }
}

export default Table;

const left = 100 / 7;
export const top = 70; //height
const styles = StyleSheet.create({
  containerStyle: {
    overflow: 'hidden',
    flexDirection: 'row',
    flexWrap: 'wrap',
    flex: 1,
    width: '100%',
  },
  table: {
    // height: 245,
    backgroundColor: '#182651',
    marginHorizontal: 16,
    borderRadius: 8,
  },
  column: {
    height: '100%',
    width: 1,
    backgroundColor: '#243361',
    left: `${left}%`,
    position: 'absolute',
  },
  column1: {
    height: '100%',
    width: 1,
    backgroundColor: '#243361',
    left: `${left * 2}%`,
    position: 'absolute',
  },
  column2: {
    height: '100%',
    width: 1,
    backgroundColor: '#243361',
    left: `${left * 3}%`,
    position: 'absolute',
  },
  column3: {
    height: '100%',
    width: 1,
    backgroundColor: '#243361',
    left: `${left * 4}%`,
    position: 'absolute',
  },
  column4: {
    height: '100%',
    width: 1,
    backgroundColor: '#243361',
    left: `${left * 5}%`,
    position: 'absolute',
  },
  column5: {
    height: '100%',
    width: 1,
    backgroundColor: '#243361',
    left: `${left * 6}%`,
    position: 'absolute',
  },
  line1: {
    width: '100%',
    height: 1,
    backgroundColor: '#243361',
    position: 'absolute',
    top: top,
  },
  line2: {
    width: '100%',
    height: 1,
    backgroundColor: '#243361',
    position: 'absolute',
    top: top * 2,
  },
  line3: {
    width: '100%',
    height: 1,
    backgroundColor: '#243361',
    position: 'absolute',
    top: top * 3,
  },
  line4: {
    width: '100%',
    height: 1,
    backgroundColor: '#243361',
    position: 'absolute',
    top: top * 4,
  },
  line5: {
    width: '100%',
    height: 1,
    // backgroundColor: '#243361',
    backgroundColor: 'red',
    position: 'absolute',
    top: top * 5,
  },
});

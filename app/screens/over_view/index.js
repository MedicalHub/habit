import React, {Component} from 'react';
import {
  View,
  Text,
  StyleSheet,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import {Colors} from 'app/constants';
import HeaderOverView from 'app/screens/over_view/header_over_view';
import Table from 'app/screens/over_view/table';
import Day from 'app/screens/over_view/day';
import Statistical from 'app/screens/over_view/statistical';
import {connect} from 'react-redux';
import moment from 'moment';
import {getBottomSpace} from 'react-native-iphone-x-helper';

class OverView extends Component {
  onContinue = () => {
    this.props.navigation.navigate('DetailHabit');
  };
  onHome = () => {
    this.props.navigation.navigate('AppTab');
  };

  getDayFirstComplete = () => {
    const {dataTopic} = this.props;
    let firstTimeComplete = null;
    for (let i = 0; i < dataTopic.length; i++) {
      if (dataTopic[i].completed_time) {
        firstTimeComplete = dataTopic[i].completed_time;
        break;
      }
    }
    return firstTimeComplete;
  };

  getInfoProgress = () => {
    let firstTimeComplete = this.getDayFirstComplete();
    console.log('firstTimeComplete', firstTimeComplete);
    let arrayProgress = []; // mang thong ke
    let number_complete = 0;
    let number_not_complete = 0;
    if (!firstTimeComplete) {
      // dont have item complete
      return {arrayProgress, number_complete, number_not_complete};
    }
    let start = moment(firstTimeComplete).startOf('day');
    let end = moment().startOf('day');
    let days = end.diff(start, 'days') + 1;

    const {dataTopic} = this.props;

    if (days > dataTopic.length) {
      days = dataTopic.length;
    }
    let j = 0;
    for (let i = 0; i < days; i++) {
      if (dataTopic[j].completed_time) {
        let time = moment(dataTopic[j].completed_time).startOf('day');
        if (time.diff(start, 'days') === i) {
          arrayProgress.push({
            id: i,
            active: !!dataTopic[j].completed_time,
            completed_time: dataTopic[j].completed_time,
            numberActive: j + 1,
          });
          j = j + 1;
        } else {
          arrayProgress.push({
            id: i,
            active: false,
            completed_time: null,
          });
        }
      } else {
        arrayProgress.push({
          id: i,
          active: false,
          completed_time: null,
        });
      }
      if (dataTopic[i].completed_time) {
        number_complete = number_complete + 1;
      } else {
        number_not_complete = number_not_complete + 1;
      }
    }
    return {arrayProgress, number_complete, number_not_complete};
  };

  render() {
    const {navigation, dataTopic} = this.props;
    let {
      arrayProgress,
      number_not_complete,
      number_complete,
    } = this.getInfoProgress();
    let progress = 0;
    if (dataTopic.length !== 0) {
      // eslint-disable-next-line radix
      progress = parseInt((number_complete / dataTopic.length) * 100);
    }

    let length = dataTopic.length;

    return (
      <View style={styles.container}>
        <HeaderOverView navigation={navigation} />
        <View style={styles.container}>
          <ScrollView showsVerticalScrollIndicator={false}>
            <Statistical
              progress={progress}
              length={length}
              number_complete={number_complete}
              number_not_complete={number_not_complete}
            />
            <Text style={styles.title}>Chi tiết {length} ngày</Text>

            <Table length={arrayProgress.length}>
              {arrayProgress.map(item => {
                return (
                  <Day
                    day={item.id}
                    active={item.active}
                    key={item.id}
                    numberActive={item.numberActive}
                    completed_time={item.completed_time}
                  />
                );
              })}
            </Table>
          </ScrollView>
        </View>
        <View style={styles.bottom}>
          <TouchableOpacity
            onPress={this.onContinue}
            style={styles.btnContinue}>
            <Text style={styles.txtBtn}>Tiếp Tục</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={this.onHome} style={styles.btnHome}>
            <Text style={styles.txtBtn}>Trang Chủ</Text>
          </TouchableOpacity>
        </View>
      </View>
    );
  }
}

const mapStateToProps = state => {
  const dataTopic = state.item_topic.data || [];
  return {
    dataTopic,
  };
};

export default connect(mapStateToProps)(OverView);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },
  title: {
    fontSize: 16,
    lineHeight: 19,
    color: '#8798CD',
    marginTop: 24,
    marginLeft: 16,
    marginBottom: 20,
  },

  btnContinue: {
    borderWidth: 1,
    marginHorizontal: 24,
    height: 40,
    width: '90%',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#FFFFFF',
    alignSelf: 'center',
    marginTop: 16,
  },
  btnHome: {
    width: '90%',
    height: 40,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    backgroundColor: '#FF326F',
    alignSelf: 'center',
    marginBottom: getBottomSpace(),
  },
  txtBtn: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#FFFFFF',
  },
  bottom: {
    width: '100%',
    backgroundColor: Colors.PRIMARY_1,
  },
});

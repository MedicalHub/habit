import React, {PureComponent} from 'react';
import {View, StyleSheet, Text} from 'react-native';
import FastImage from 'react-native-fast-image';
import {top} from 'app/screens/over_view/table';
import moment from 'moment';
import reactotron from 'reactotron-react-native';

class Day extends PureComponent {
  render() {
    const {day, active, completed_time, numberActive} = this.props;
    const image = active ? (
      <FastImage
        source={require('assets/images/small_check.png')}
        style={styles.image}
        resizeMode={'cover'}
      />
    ) : (
      <FastImage
        source={require('assets/images/small_un_check.png')}
        style={styles.image}
        resizeMode={'cover'}
      />
    );
    const styleDay = active ? styles.dayActive : styles.dayInActive;

    return (
      <View style={styles.item}>
        <Text style={styleDay}>{active ? numberActive : ''}</Text>
        {image}
        <Text style={styleDay}>
          {active && moment(completed_time).format('D/M')}
        </Text>
      </View>
    );
  }
}

export default Day;

const styles = StyleSheet.create({
  item: {
    width: `${100 / 7}%`,
    height: top,
    justifyContent: 'center',
    alignItems: 'center',
  },
  image: {
    width: 16,
    height: 16,
  },
  dayActive: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    color: '#00DA4A',
    marginBottom: 3,
  },
  dayInActive: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    color: '#FF0000',
    marginBottom: 3,
  },
});

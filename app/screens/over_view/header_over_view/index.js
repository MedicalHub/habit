import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity} from 'react-native';
import {Colors} from 'app/constants';
import HeaderLeft from 'app/components/header_left';
import HeaderRight from 'app/components/header_right';
import Header from 'app/components/header';
import LinearGradient from 'react-native-linear-gradient';

class HeaderOverView extends Component {
  goBack = () => this.props.navigation.navigate('DetailHabit');
  onShare = () => {};
  render() {
    return (
      <LinearGradient
        colors={['#0F1B3D', 'rgba(19,26,64,0.9)']}
        start={{x: 0, y: 0}}
        end={{x: 0, y: 1}}>
        <Header>
          <TouchableOpacity onPress={this.goBack}>
            <HeaderLeft
              source={require('assets/images/arrow-left.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
          <Text style={styles.title}>Tổng quan</Text>
          <TouchableOpacity onPress={this.onShare}>
            <HeaderRight
              source={require('assets/images/share.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
        </Header>
      </LinearGradient>
    );
  }
}
export default HeaderOverView;
const styles = StyleSheet.create({
  process: {
    fontSize: 40,
    fontWeight: 'bold',
    color: '#FFC500',
  },
  title: {
    color: Colors.LIGHT,
    fontFamily: 'iCielHelveticaNowText-Medium',
    fontSize: 18,
    textAlign: 'center',
  },
});

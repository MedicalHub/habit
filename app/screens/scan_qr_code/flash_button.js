import React from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';

import {RNCamera} from 'react-native-camera';

import Icon from 'app/components/icons';
import {Colors} from 'app/constants';

const {FlashMode} = RNCamera.Constants;

class FlashButton extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      flashBg: 'rgba(0,0,0,0)',
      flashIcon: '#FFF',
    };
  }

  onPress = () => {
    const {onPress, flashMode} = this.props;
    onPress();
    if (flashMode === FlashMode.off) {
      this.setState({
        flashBg: '#FFF',
        flashIcon: Colors.PRIMARY_TEXT,
      });
    } else {
      this.setState({
        flashBg: 'rgba(0,0,0,0)',
        flashIcon: '#FFF',
      });
    }
  };
  render() {
    const {flashBg, flashIcon} = this.state;
    return (
      <TouchableOpacity
        style={[
          styles.flash,
          {
            backgroundColor: flashBg,
          },
        ]}
        onPress={this.onPress}>
        <Icon type="System" name="lighting" size={24} color={flashIcon} />
      </TouchableOpacity>
    );
  }
}
export default FlashButton;

const styles = StyleSheet.create({
  flash: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#FFF',
    marginTop: 32,
  },
});

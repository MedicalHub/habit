import React from 'react';
import {View, StyleSheet, Alert} from 'react-native';

import {RNCamera} from 'react-native-camera';
import BarcodeMask from 'react-native-barcode-mask';

import FormattedText from 'app/components/formatted_text';
import {Screen} from 'app/constants';
import ActiveCodeBottom from 'app/components/button/active_code_bottom';
import FlashButton from 'app/screens/scan_qr_code/flash_button';
import PermissonDenied from 'app/screens/scan_qr_code/permission_denied';
import {getUserInfo} from 'app/selectors/user';
import {updateProfile} from 'app/actions/user';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import api from 'app/saga/api';

const {FlashMode} = RNCamera.Constants;

class ScanQRCode extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      focusedScreen: false,
      flashMode: FlashMode.off,
    };
  }

  toggleFlash = () => {
    if (this.state.flashMode === FlashMode.torch) {
      this.setState({
        flashMode: FlashMode.off,
      });
    } else {
      this.setState({
        flashMode: FlashMode.torch,
      });
    }
  };

  handleQRCode = async event => {
    if (event) {
      const code = event.data;
      this.props.navigation.replace('InputActiveCode', {
        code,
      });
    }
    //TODO: connect API
  };

  onEnterCode = () => {
    this.props.navigation.replace('InputActiveCode');
  };

  renderTool = () => {
    return (
      <View style={styles.toolContainer}>
        <BarcodeMask
          backgroundColor={'rgba(2,3,9,0.2)'}
          edgeBorderWidth={3}
          showAnimatedLine={false}
        />

        <View style={styles.tool}>
          <FormattedText
            id="scanQRCodeTurorial"
            defaultMessage="Căn cho mã quét vào đúng ô vuông"
            style={styles.scanQRCodeTurorial}
          />
          <FlashButton
            onPress={this.toggleFlash}
            flashMode={this.state.flashMode}
          />
        </View>
        <ActiveCodeBottom activeMethod="qr" onEnterCode={this.onEnterCode} />
      </View>
    );
  };

  render() {
    const {flashMode} = this.state;
    const {screenProps, navigation} = this.props;
    const {intl} = screenProps;
    return (
      <RNCamera
        ref={ref => {
          this.camera = ref;
        }}
        captureAudio={false}
        flashMode={flashMode}
        style={styles.preview}
        androidCameraPermissionOptions={{
          title: intl.formatMessage({
            id: 'android.permission.camera.title',
            defaultMessage: 'Permission to use camera',
          }),
          message: intl.formatMessage({
            id: 'android.permission.camera.message',
            defaultMessage: 'Permission to use camera',
          }),
          buttonPositive: intl.formatMessage({
            id: 'button.ok',
            defaultMessage: 'Ok',
          }),
          buttonNegative: intl.formatMessage({
            id: 'button.cancel',
            defaultMessage: 'Cancel',
          }),
        }}
        onBarCodeRead={event => {
          this.handleQRCode(event);
        }}
        // barCodeTypes={['qr']}
        notAuthorizedView={
          <PermissonDenied navigation={navigation} intl={intl} />
        }>
        {({status}) => {
          if (status === 'NOT_AUTHORIZED') {
            return <PermissonDenied navigation={navigation} intl={intl} />;
          }
          if (status === 'READY') {
            return this.renderTool();
          }
          return null;
        }}
      </RNCamera>
    );
  }
}
const mapStateToProps = state => {
  const userId = getUserInfo(state)._id;

  return {
    userId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        updateProfile: updateProfile,
      },
      dispatch,
    ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ScanQRCode);

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  preview: {
    ...StyleSheet.absoluteFillObject,
  },
  scanQRCodeTurorial: {
    color: '#FFF',
    fontSize: 14,
    textAlign: 'center',
    marginTop: 16,
    fontFamily: 'iCielHelveticaNowText-Regular',
  },
  toolContainer: {
    flex: 1,
  },
  tool: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM + 70,
    justifyContent: 'center',
    alignItems: 'center',
    left: 0,
    right: 0,
  },
  qrContainer: {
    width: 239,
    height: 239,
    justifyContent: 'center',
    alignItems: 'center',
  },
  qr: {
    height: 219,
    width: 219,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#FFF',
  },
  flash: {
    height: 40,
    width: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#FFF',
    marginTop: 32,
  },
});

import React from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';

import PropTypes from 'prop-types';
import {openSettings} from 'react-native-permissions';

import {Colors} from 'app/constants';
import FormattedText from 'app/components/formatted_text';
import TouchableText from 'app/components/touchable_text';

class PermissonDenied extends React.PureComponent<Props> {
  onOpenSettings = async () => {
    try {
      await openSettings();
    } catch (error) {
      console.log('error', error);
    }
  };
  render() {
    const {intl} = this.props;
    return (
      <View style={styles.container}>
        <FormattedText
          numberOfLines={1}
          adjustsFontSizeToFit={true}
          style={styles.message}
          id="shareOn"
          defaultMessage="Chia sẻ với"
        />
        <FormattedText
          numberOfLines={2}
          adjustsFontSizeToFit={true}
          style={styles.descriptionMsg}
          id="shareOnDesc"
          defaultMessage="Kích hoạt quyền truy nhập máy ảnh để quét mã QR"
        />
        <TouchableText
          style={styles.enableCameraAccess}
          onPress={this.onOpenSettings}
          label={intl.formatMessage({
            id: 'enableCameraAccess',
            defaultMessage: 'Kích hoạt quyền truy cập máy ảnh',
          })}
          position="center"
        />
      </View>
    );
  }
}

PermissonDenied.propTypes = {
  intl: PropTypes.object,
  navigation: PropTypes.object,
};
export default PermissonDenied;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    // backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  message: {
    color: '#FFF',
    fontSize: 18,
    paddingHorizontal: 16,
    textAlign: 'center',
    fontFamily: 'iCielHelveticaNowText-Medium',
  },
  descriptionMsg: {
    color: '#FFF',
    fontSize: 14,
    paddingHorizontal: 14,
    marginTop: 10,
    fontFamily: 'iCielHelveticaNowText-Regular',
  },
  enableCameraAccess: {
    color: '#3897F0',
    fontSize: 16,
    paddingHorizontal: 16,
    marginTop: 20,
    fontFamily: 'iCielHelveticaNowText-Medium',
  },
});

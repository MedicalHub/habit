import React, {memo} from 'react';
import {Text, StyleSheet} from 'react-native';
import {useTrackPlayerProgress} from 'react-native-track-player';
import moment from 'moment';
const Duration = memo(() => {
  const {duration} = useTrackPlayerProgress();
  return (
    <Text style={styles.time}>{moment(duration * 1000).format('mm:ss')}</Text>
  );
});
export default Duration;
const styles = StyleSheet.create({
  time: {
    fontSize: 12,
    lineHeight: 18,
    color: '#8798CD',
    width: 45,
    textAlign: 'center',
  },
});

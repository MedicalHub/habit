import React, {memo, useCallback, useEffect, useMemo, useState} from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import {Colors} from 'app/constants';
import TrackPlayer, {useTrackPlayerProgress} from 'react-native-track-player';
import {useDispatch, useSelector} from 'react-redux';
import API from 'app/saga/api';
import {getItemTopic} from 'app/actions/item_topic';
import reactotron from 'reactotron-react-native';

const ButtonDone = memo(props => {
  const dispatch = useDispatch();
  const id = useMemo(() => props.currentTrack?._id, [props.currentTrack]);
  const congratulation_sound = useMemo(
    () => props.currentTrack?.congratulation_sound,
    [props.currentTrack],
  );
  const topic_id = useMemo(() => props.currentTrack?.topic_id, [
    props.currentTrack,
  ]);
  const index = useMemo(() => props.currentTrack?.index, [props.currentTrack]);

  const [isDone, setIsDone] = useState(!!props.currentTrack?.is_completed);

  useEffect(() => {
    // if (isDone) {
    //   return;
    // }
    setIsDone(!!props.currentTrack?.is_completed);
  }, [props.currentTrack]);

  const {position, duration} = useTrackPlayerProgress();

  const userId = useSelector(state => state.user.info._id);

  const onDone = useCallback(async () => {
    await TrackPlayer.pause();
    props.navigation.navigate('CongratulationDay', {
      userId,
      topic_id,
      congratulation_sound:
        'https://habitmedia.s3.amazonaws.com/' +
        congratulation_sound.split(' ').join('%20'),
      index,
    });
  }, [congratulation_sound, index, props.navigation, topic_id, userId]);

  useEffect(() => {
    if (isDone) {
      return;
    }
    if (duration === 0) {
      return;
    }
    // eslint-disable-next-line radix
    if (
      parseInt(position) === parseInt(duration) ||
      parseInt(position) === parseInt(duration - 1)
    ) {
      reactotron.log('done', userId, id, props.currentTrack);
      setIsDone(true);
      //complete
      API.updateProgress({
        user_id: userId,
        topic_item_id: id,
      }).then(dispatch(getItemTopic(topic_id, userId)));
    }
  }, [
    props.currentTrack,
    dispatch,
    duration,
    id,
    isDone,
    position,
    topic_id,
    userId,
  ]);

  return (
    <TouchableOpacity
      style={[styles.styleContainer, {opacity: isDone ? 1 : 0.5}]}
      onPress={onDone}
      disabled={!isDone}>
      <Text style={styles.txtDone}>Đã hoàn thành</Text>
    </TouchableOpacity>
  );
});

export default ButtonDone;

const styles = StyleSheet.create({
  styleContainer: {
    backgroundColor: '#FF326F',
    width: 200,
    alignSelf: 'center',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginTop: 16,
  },
  txtDone: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: Colors.LIGHT,
  },
});

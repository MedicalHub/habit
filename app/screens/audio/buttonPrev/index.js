import React, {memo, useCallback, useMemo} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {setCurrentTrack} from 'app/actions/playback';
import reactotron from 'reactotron-react-native';

const ButtonPrev = memo(props => {
  const data = useSelector(state => state.item_topic.dataAudio);
  const indexCurrentTrack = useSelector(
    state => state.audio.playback.currentTrack.index,
  );
  const dispatch = useDispatch();
  const opacity = useMemo(() => {
    if (indexCurrentTrack === 0) {
      return 0.5;
    } else {
      return 1;
    }
  }, [indexCurrentTrack]);

  const onPrev = useCallback(() => {
    const nextTrack = data[indexCurrentTrack - 1];
    if (!nextTrack) {
      return;
    }
    dispatch(setCurrentTrack(nextTrack));
  }, [data, dispatch, indexCurrentTrack]);
  return (
    <TouchableOpacity
      onPress={onPrev}
      style={[styles.btnNext, {opacity: opacity}]}
      disabled={opacity === 0.5}>
      <FastImage
        source={require('assets/images/prev.png')}
        resizeMode="cover"
        style={styles.prev}
      />
    </TouchableOpacity>
  );
});

export default ButtonPrev;

const styles = StyleSheet.create({
  btnNext: {
    padding: 5,
  },
  prev: {
    width: 36,
    height: 36,
  },
});

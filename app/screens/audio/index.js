import React, {Component} from 'react';
import {StyleSheet, Text, TouchableOpacity, View} from 'react-native';
import {Colors} from 'app/constants';
import HeaderRight from 'app/components/header_right';
import ContentModal from 'app/screens/audio/content_modal';
import HeaderLeft from 'app/components/header_left';
import Header from 'app/components/header';
import {RFPercentage} from 'react-native-responsive-fontsize';
import {connect} from 'react-redux';
import {getUserInfo} from 'app/selectors/user';
import ButtonPlay from 'app/screens/audio/buttonPlay';
import ButtonDone from 'app/screens/audio/buttonDone';
import TrackPlayer from 'react-native-track-player';
import ButtonNext from 'app/screens/audio/buttonNext';
import ButtonPrev from 'app/screens/audio/buttonPrev';
import Progress from 'app/screens/audio/progress';
import CurrentDuration from 'app/screens/audio/currentDuration';
import Duration from 'app/screens/audio/duration';
import {setCurrentTrack, _setCurrentTrack} from 'app/actions/playback';
import WrapThumbnail from 'app/screens/audio/thumbnail';
class Audio extends Component {
  constructor(props) {
    super(props);
    const {data, navigation} = this.props;
    const {index} = navigation.state.params;
    this.data = data;
    this.currentTrack = this.data[index];
  }

  componentDidMount = async () => {
    this.props.navigation.setParams({
      onShare: this.onShare,
    });
    this.props._setCurrentTrack(this.currentTrack); //set redux
    await this.initPlayer();
  };

  componentWillUnmount = async () => {
    await TrackPlayer.reset();
    this.props._setCurrentTrack({});
    this.onChange.remove();
  };

  initPlayer = async () => {
    await TrackPlayer.setupPlayer();
    TrackPlayer.updateOptions({
      stopWithApp: true,
      capabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
        TrackPlayer.CAPABILITY_SKIP_TO_NEXT,
        TrackPlayer.CAPABILITY_SKIP_TO_PREVIOUS,
      ],
      compactCapabilities: [
        TrackPlayer.CAPABILITY_PLAY,
        TrackPlayer.CAPABILITY_PAUSE,
      ],
    });
    await TrackPlayer.reset();
    await TrackPlayer.add([...this.props.data]);
    await this.props.setCurrentTrack(this.currentTrack);

    this.onChange = TrackPlayer.addEventListener(
      'playback-track-changed',
      async props => {
        const idCurrentTrack = await TrackPlayer.getCurrentTrack();
        const idTrackReducer = this.props.currentTrack.id;
        if (idCurrentTrack !== idTrackReducer) {
          const result = this.props.data.find(
            element => element._id === idCurrentTrack,
          );
          this.props._setCurrentTrack(result);
        }
      },
    );
  };

  pause = async () => {
    await TrackPlayer.pause();
  };

  play = async () => {
    await TrackPlayer.play();
  };

  onShare = () => {};

  goBack = () => {
    this.ModalRef.onShow();
    this.pause().then();
  };

  render() {
    const {navigation, currentTrack} = this.props;
    if (!currentTrack) {
      return null;
    }
    const {title = '', description, thumbnail, time} = currentTrack;
    return (
      <View style={styles.container}>
        <Header>
          <TouchableOpacity onPress={this.goBack}>
            <HeaderLeft
              source={require('assets/images/arrow-left.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={this.goBack}>
            <HeaderRight
              source={require('assets/images/share.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
        </Header>
        <View style={styles.songImage}>
          <WrapThumbnail spinValue={this.spinValue} thumbnail={thumbnail} />
        </View>
        <View style={styles.song}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.content}>{description}</Text>
          <ButtonDone navigation={navigation} currentTrack={currentTrack} />
        </View>
        <View style={styles.song}>
          <View style={styles.containerSlider}>
            <CurrentDuration />
            <Progress maximumValue={time} />
            <Duration />
          </View>
          <View style={styles.controller}>
            <ButtonPrev />
            <ButtonPlay />
            <ButtonNext />
          </View>
        </View>

        <ContentModal
          ref={ref => (this.ModalRef = ref)}
          navigation={navigation}
          onHide={this.play}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const data = state.item_topic.dataAudio;
  const user = getUserInfo(state);
  return {
    data,
    userId: user._id,
    currentTrack: state.audio.playback.currentTrack,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    setCurrentTrack: currentTrack => dispatch(setCurrentTrack(currentTrack)),
    _setCurrentTrack: currentTrack => dispatch(_setCurrentTrack(currentTrack)),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Audio);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },

  headerStyle: {
    backgroundColor: Colors.PRIMARY_1,
    borderBottomWidth: 0,
    elevation: 0,
    shadowColor: 'rgba(0,0,0,0)',
  },
  title: {
    fontWeight: '500',
    fontSize: RFPercentage(3),
    color: Colors.LIGHT,
    textAlign: 'center',
  },
  content: {
    fontSize: RFPercentage(2),
    lineHeight: 24,
    textAlign: 'center',
    color: '#8798CD',
    marginHorizontal: 70,
    marginTop: 16,
  },
  styleContainer: {
    backgroundColor: '#FF326F',
    width: 200,
    alignSelf: 'center',
    height: 40,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    marginTop: 16,
  },
  block: {
    width: 200,
    height: 40,
    marginTop: 16,
  },
  txtDone: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: Colors.LIGHT,
  },
  slider: {
    height: '100%',
    flex: 1,
    marginHorizontal: 7,
  },
  time: {
    fontSize: 12,
    lineHeight: 18,
    color: '#8798CD',
    width: 45,
    textAlign: 'center',
  },
  containerSlider: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 16,
  },
  controller: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    marginTop: 30,
  },
  play: {
    width: 64,
    height: 64,
    borderRadius: 32,
    backgroundColor: '#243361',
    marginHorizontal: 40,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 7,
  },
  prev: {
    width: 36,
    height: 36,
  },
  next: {
    width: 36,
    height: 36,
  },
  song: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
  },
  songImage: {
    flex: 1.5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  btnNext: {
    padding: 5,
  },
});

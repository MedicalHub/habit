import React, {memo, useCallback, useMemo} from 'react';
import {View, StyleSheet, TouchableOpacity} from 'react-native';
import FontAwesome from 'react-native-vector-icons/FontAwesome';

import {useDispatch, useSelector} from 'react-redux';
import {setPlayback} from 'app/actions/playback';
import {useIsPlaying} from 'app/screens/audio/hooks';

const ButtonPlay = memo(props => {
  const isPlaying = useIsPlaying();

  const dispatch = useDispatch();
  const onPress = useCallback(async () => {
    dispatch(setPlayback(!isPlaying));
  }, [dispatch, isPlaying]);

  return (
    <View style={styles.container}>
      {!isPlaying ? (
        <TouchableOpacity style={styles.touchPlay} onPress={onPress}>
          <FontAwesome name={'play'} size={24} color="#8798CD" />
        </TouchableOpacity>
      ) : (
        <TouchableOpacity style={styles.touchPause} onPress={onPress}>
          <FontAwesome name={'pause'} size={24} color="#8798CD" />
        </TouchableOpacity>
      )}
    </View>
  );
});

export default ButtonPlay;

const styles = StyleSheet.create({
  container: {
    width: 64,
    height: 64,
    borderRadius: 32,
    backgroundColor: '#243361',
    marginHorizontal: 40,
    overflow: 'hidden',
  },
  touchPlay: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    paddingLeft: 7,
  },
  touchPause: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
});

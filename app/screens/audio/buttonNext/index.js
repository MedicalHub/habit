import React, {memo, useCallback, useMemo} from 'react';
import {StyleSheet, TouchableOpacity} from 'react-native';
import FastImage from 'react-native-fast-image';
import {useDispatch, useSelector} from 'react-redux';
import {setCurrentTrack} from 'app/actions/playback';
import reactotron from 'reactotron-react-native';

const ButtonNext = memo(props => {
  const data = useSelector(state => state.item_topic.dataAudio || []);
  const indexCurrentTrack = useSelector(
    state => state.audio.playback.currentTrack.index,
  );

  const dispatch = useDispatch();

  const opacity = useMemo(() => {
    const track = data[indexCurrentTrack + 1];
    if (!track) {
      return 0.5;
    }
    if (track.is_locked) {
      return 0.5;
    }
    return 1;
  }, [data, indexCurrentTrack]);

  const onNext = useCallback(() => {
    const nextTrack = data[indexCurrentTrack + 1];
    if (!nextTrack) {
      return;
    }
    if (nextTrack.is_locked) {
      return;
    }
    dispatch(setCurrentTrack(nextTrack));
  }, [data, dispatch, indexCurrentTrack]);
  return (
    <TouchableOpacity
      onPress={onNext}
      style={[styles.btnNext, {opacity: opacity}]}
      disabled={opacity === 0.5}>
      <FastImage
        source={require('assets/images/next.png')}
        resizeMode="cover"
        style={styles.next}
      />
    </TouchableOpacity>
  );
});

export default ButtonNext;

const styles = StyleSheet.create({
  btnNext: {
    padding: 5,
  },
  next: {
    width: 36,
    height: 36,
  },
});

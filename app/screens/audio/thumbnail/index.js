import React, {memo, useEffect, useMemo, useRef} from 'react';
import API from 'app/saga/api';
import FastImage from 'react-native-fast-image';
import {Animated, StyleSheet, Easing, View} from 'react-native';
import Device from 'app/utils/device';
import {useSelector} from 'react-redux';
import {useIsPlaying} from 'app/screens/audio/hooks';

const AnimatedFastImage = Animated.createAnimatedComponent(FastImage);

const DURATION = 20000;
class Thumbnail extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      spinAnimation: new Animated.Value(0),
    }; // Initial value for opacity: 0
    this.spinAnimationValue = 0;
  }

  componentDidMount() {
    if (this.props.refs) {
      this.props.refs(this);
    }
    this.state.spinAnimation.addListener(state => {
      // console.log('value', state.value);
      this.spinAnimationValue = state.value;
    });
    // this.runAnimation();
  }

  runAnimation(value) {
    let nextVale = 1;
    if (value) {
      this.state.spinAnimation.setValue(value);
      nextVale = nextVale + value;
    } else {
      this.state.spinAnimation.setValue(0);
      nextVale = 1;
    }
    Animated.timing(this.state.spinAnimation, {
      toValue: nextVale,
      duration: DURATION,
      easing: Easing.linear,
      useNativeDriver: true,
    }).start(results => {
      if (results.finished) {
        this.runAnimation(this.spinAnimationValue);
      }
    });
  }

  startSpin = () => {
    this.runAnimation(this.spinAnimationValue);
  };

  resetSpin = () => {
    this.runAnimation(0);
  };

  stopSpin = () => {
    this.state.spinAnimation.stopAnimation();
  };

  render() {
    const {spinAnimation} = this.state;
    const spin = spinAnimation.interpolate({
      inputRange: [0, 1],
      outputRange: ['0deg', '360deg'],
    });
    return (
      <View style={styles.container}>
        <AnimatedFastImage
          source={{uri: API.urlImage(this.props.thumbnail)}}
          resizeMode="cover"
          // style={[styles.image, {transform: [{rotate: spin}]}]}
          style={{...styles.image, transform: [{rotate: spin}]}}>
          <FastImage
            source={require('assets/images/oval_linear.png')}
            resizeMode="cover"
            style={styles.ovalLinear}
          />
          <FastImage
            source={require('assets/images/oval.png')}
            resizeMode="cover"
            style={styles.oval}
          />
        </AnimatedFastImage>
      </View>
    );
  }
}

const WrapThumbnail = memo(props => {
  const refThumbnail = useRef();
  const currentTrack = useSelector(state => state.audio.playback.currentTrack);
  const isPlaying = useIsPlaying();

  useEffect(() => {
    if (isPlaying) {
      refThumbnail.current.startSpin();
    } else {
      refThumbnail.current.stopSpin();
      // refThumbnail.current.resetSpin();
    }
  }, [isPlaying]);

  return <Thumbnail thumbnail={currentTrack.thumbnail} ref={refThumbnail} />;
});

export default WrapThumbnail;

const styles = StyleSheet.create({
  container: {
    width: Device.screen.height * 0.3,
    height: Device.screen.height * 0.3,
    borderRadius: (Device.screen.height * 0.3) / 2,
    overflow: 'hidden',
  },
  image: {
    width: Device.screen.height * 0.3,
    height: Device.screen.height * 0.3,
    // borderRadius: (Device.screen.height * 0.3) / 2,
    justifyContent: 'center',
    alignItems: 'center',
    overflow: 'hidden',
  },
  oval: {
    width: 48,
    height: 48,
  },
  ovalLinear: {
    ...StyleSheet.absoluteFillObject,
    position: 'absolute',
  },
});

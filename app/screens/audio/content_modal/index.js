import React, {PureComponent} from 'react';
import {
  Text,
  TouchableOpacity,
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Image,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import CheckBox from 'app/components/check_box';
import Modal from 'react-native-modal';
const dataCheckBox = [
  {titleId: 'dontLikeTone', titleDefMsg: 'Tôi không thích giọng đọc'},
  {titleId: 'dontLikeContent', titleDefMsg: 'Tôi không thích nội dung này'},
  {titleId: 'dontHaveTime', titleDefMsg: 'Tôi chưa có thời gian nghe hết'},
];

class ContentModal extends PureComponent {
  state = {
    indexCheck: -1,
    isShowModal: false,
  };

  onSelect = index => {
    this.setState({indexCheck: index});
  };

  onShow = () => this.setState({isShowModal: true});
  onHide = () => {
    this.setState({isShowModal: false});
    this.props.onHide();
  };
  onEdit = () => {
    this.setState({isShowModal: false}, () => {
      this.props.navigation.goBack();
    });
  };

  render() {
    const {indexCheck, isShowModal} = this.state;
    return (
      <Modal isVisible={isShowModal}>
        <View style={styles.modal}>
          <View style={styles.contentModal}>
            <TouchableOpacity style={styles.btnCancel} onPress={this.onHide}>
              <Image
                source={require('assets/images/exit.png')}
                style={styles.edit}
              />
            </TouchableOpacity>

            <Text style={styles.titleModal}>Xác nhận thoát</Text>
            <Text style={styles.desModal}>
              Bạn chưa nghe xong, bạn có chắc chắn muốn thoát không?
            </Text>

            <View style={styles.groupCheckBox}>
              {dataCheckBox.map((item, index) => {
                let isSelected = indexCheck === index;
                return (
                  <CheckBox
                    des={item.titleDefMsg}
                    isSelected={isSelected}
                    onPress={this.onSelect}
                    key={index}
                    index={index}
                  />
                );
              })}
            </View>

            <View style={styles.bottom}>
              <TouchableOpacity style={styles.btnContinue} onPress={this.onHide}>
                <Text style={styles.listenContinue}>Nghe Tiếp</Text>
              </TouchableOpacity>
              <TouchableOpacity style={styles.btnEdit} onPress={this.onEdit}>
                <Text style={styles.txtEdit}>Thoát</Text>
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </Modal>
    );
  }
}
export default ContentModal;

const styles = StyleSheet.create({
  modal: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    ...StyleSheet.absoluteFillObject,
  },
  edit: {
    width: 24,
    height: 24,
    resizeMode: 'cover',
  },
  contentModal: {
    width: '90%',
    height: 334,
    backgroundColor: '#182651',
    borderRadius: 8,
  },
  titleModal: {
    fontWeight: '500',
    fontSize: 24,
    lineHeight: 32,
    textAlign: 'center',
    color: '#FFFFFF',
    marginTop: 22,
  },
  btnCancel: {
    position: 'absolute',
    right: 6,
    top: 6,
    width: 30,
    height: 30,
    justifyContent: 'center',
    alignItems: 'center',
    zIndex: 1,
  },
  desModal: {
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'center',
    color: '#8798CD',
    marginHorizontal: '10%',
    marginTop: 12,
  },
  bottom: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    marginBottom: 16,
  },
  btnContinue: {
    width: '40%',
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#8798CD',
  },
  listenContinue: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#8798CD',
  },
  btnEdit: {
    width: '40%',
    height: 44,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 8,
    backgroundColor: '#FF326F',
  },
  txtEdit: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#FFF',
  },
  groupCheckBox: {
    flex: 1,
    paddingLeft: 24,
    justifyContent: 'center',
  },
});

import Animated, {
  and,
  Clock,
  Easing,
  not,
  useCode,
} from 'react-native-reanimated';
import {useEffect, useMemo, useRef, useState} from 'react';

const {
  set,
  cond,
  startClock,
  clockRunning,
  block,
  timing,
  Value,
  concat,
} = Animated;
const pauseFlag = new Value(0);

const runTiming = (clock: any, value: number, dest: number, auto: boolean) => {
  const state = {
    finished: new Value(0),
    position: new Value(0),
    time: new Value(0),
    frameTime: new Value(0),
    auto: new Value(auto ? 1 : 0),
    paused: new Value(0),
    started: new Value(0),
  };
  const config = {
    duration: 8000,
    toValue: new Value(0),
    easing: Easing.linear,
  };

  return block([
    cond(
      clockRunning(clock),
      [
        // if the clock is already running we update the toValue, in case a new dest has been passed in
        set(config.toValue, dest),
      ],
      [
        // if the clock isn't running we reset all the animation params and start the clock
        set(state.finished, 0),
        set(state.time, 0),
        set(state.position, value),
        set(state.frameTime, 0),
        set(config.toValue, dest),
        startClock(clock),
      ],
    ),
    // if the animation is over we stop the clock
    cond(and(state.finished, state.auto), [
      set(state.finished, 0),
      set(state.time, 0),
      set(state.position, value),
      set(state.frameTime, 0),
      set(config.toValue, dest),
      startClock(clock),
    ]),
    cond(
      clockRunning(clock),
      [
        cond(
          pauseFlag, //See useEffect.
          [
            set(state.paused, 1),
            set(state.time, clock), //<== THIS SOLVED THE ISSUE
          ],
          [set(state.paused, 0), set(config.toValue, dest)],
        ),
      ],
      [cond(not(state.started), [set(state.started, 1), startClock(clock)])],
    ),
    // we run the step here that is going to update position
    timing(clock, state, config),
    // we made the block return the updated position
    state.position,
  ]);
};

const useRotationAnimation = (value = 0, dest = 360, auto = true) => {
  const [play, setPlay] = useState(false);
  const {clock, timing} = useMemo(
    () => ({
      clock: new Clock(),
      timing: new Value(0),
    }),
    [],
  );
  const rotation = useMemo(() => {
    return concat(timing, 'deg');
  }, [timing]);

  useEffect(() => {
    if (!play) {
      pauseFlag.setValue(1);
    } else {
      pauseFlag.setValue(0);
    }
  }, [play]);

  useCode(
    () =>
      block([
        startClock(clock),
        set(timing, runTiming(clock, value, dest, auto)),
      ]),
    [clock],
  );

  return {rotation, setPlay};
};

export default useRotationAnimation;

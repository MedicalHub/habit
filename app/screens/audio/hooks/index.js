import {useMemo, useRef} from 'react';
import {
  STATE_PLAYING,
  TrackPlayerEvents,
  usePlaybackState,
  useTrackPlayerEvents,
} from 'react-native-track-player';

const events = [TrackPlayerEvents.PLAYBACK_QUEUE_ENDED];

const useIsFinish = () => {
  const isFinish = useRef(false);
  useTrackPlayerEvents(events, event => {
    isFinish.current = event.type === TrackPlayerEvents.PLAYBACK_QUEUE_ENDED;
  });
  return isFinish.current;
};

const useIsPlaying = () => {
  const state = usePlaybackState();
  const isPlaying = useMemo(() => state === STATE_PLAYING, [state]);
  return isPlaying;
};

export {useIsFinish, useIsPlaying};

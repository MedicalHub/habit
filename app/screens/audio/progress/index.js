import React, {memo, useCallback, useEffect, useRef, useState} from 'react';
import Slider from '@react-native-community/slider';
import {StyleSheet} from 'react-native';
import TrackPlayer, {useTrackPlayerProgress} from 'react-native-track-player';
import {useIsPlaying} from 'app/screens/audio/hooks';
import _ from 'lodash';
import reactotron from 'reactotron-react-native';

const Progress = memo(props => {
  const {position} = useTrackPlayerProgress();
  const isPlaying = useIsPlaying();
  const [value, setValue] = useState(props.position);
  const isSeeking = useRef(false);

  const onValueChange = useCallback(value => {
    setValue(value);
    isSeeking.current = true;
  }, []);

  const onDebounce = _.debounce(async seconds => {
    isSeeking.current = false;
    await TrackPlayer.seekTo(seconds);
    if (!isPlaying) {
      // isSeeking.current = false;
      await TrackPlayer.play();
    }
  }, 300);

  const onSlidingComplete = useCallback(
    seconds => {
      onDebounce(seconds);
    },
    [onDebounce],
  );

  useEffect(() => {
    reactotron.log('isSeeking.current', isSeeking.current);
    if (!isSeeking.current) {
      setValue(position);
    }
  }, [position]);
  reactotron.log('value', value);
  return (
    <Slider
      style={styles.slider}
      minimumValue={0}
      maximumValue={props.maximumValue}
      minimumTrackTintColor={'#00ACEB'}
      maximumTrackTintColor={'#8798CD'}
      thumbTintColor={'#00ACEB'}
      value={value}
      onValueChange={onValueChange}
      onSlidingComplete={onSlidingComplete}
    />
  );
});

export default Progress;

const styles = StyleSheet.create({
  slider: {
    height: '100%',
    flex: 1,
    marginHorizontal: 7,
  },
});

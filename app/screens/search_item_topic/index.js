import React, {Component} from 'react';
import {View, StyleSheet, FlatList} from 'react-native';
import {Colors} from 'app/constants';
import Separator from 'app/components/separator';
import HeaderSearch from 'app/screens/search_category/header';
import {connect} from 'react-redux';
import _ from 'lodash';
import {delete_accents} from 'app/screens/search_category';
import DetailCategoryItem from 'app/screens/detail_category/detail_category_item';

class SearchItemTopic extends Component {
  constructor(props) {
    super(props);
    this.state = {
      dataTopic: [],
    };
    this._dataTopic = [];
  }

  componentDidMount() {
    let {data} = this.props; //data category
    if (!data) {
      return;
    }
    let topics = [];
    for (let i = 0; i < data.length; i++) {
      let _topics = data[i].topics; //mang topics
      if (!_topics) {
        return;
      }
      topics = [...topics, ..._topics];
    }
    this._dataTopic = topics;
    // eslint-disable-next-line react/no-did-mount-set-state
    this.setState({
      dataTopic: topics,
    });
  }

  onChangeText = _.debounce(text => {
    const _data = this._dataTopic.filter(element => {
      let delete_accents_title = delete_accents(element.title.toUpperCase());
      let delete_accents_text = delete_accents(text.toUpperCase());
      return delete_accents_title.includes(delete_accents_text);
    });
    if (_data) {
      this.setState({dataTopic: _data});
    } else {
      this.setState({dataTopic: []});
    }
  }, 1000);

  renderItem = ({item}) => {
    const {navigation} = this.props;

    return (
      <DetailCategoryItem
        thumbnail={item.thumbnail}
        title={item.title}
        navigation={navigation}
        id={item._id}
      />
    );
  };

  keyExtractor = (item, index) => index.toString();

  renderSeparator = () => <Separator height={16} />;

  render() {
    const {navigation} = this.props;

    return (
      <View style={styles.container}>
        <HeaderSearch
          navigation={navigation}
          onChangeText={this.onChangeText}
        />
        <FlatList
          data={this.state.dataTopic}
          keyExtractor={this.keyExtractor}
          renderItem={this.renderItem}
          ItemSeparatorComponent={this.renderSeparator}
          style={styles.containerFlatList}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const data = state.category.data;
  return {
    data,
  };
};

export default connect(mapStateToProps)(SearchItemTopic);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },

  containerFlatList: {
    marginHorizontal: 16,
    paddingTop: 16,
  },
});

import React, {Component, PureComponent} from 'react';
import {
  View,
  StyleSheet,
  TouchableOpacity,
  Text,
  InteractionManager,
} from 'react-native';
import FastImage from 'react-native-fast-image';
import HeaderRight from 'app/components/header_right';
import {Colors} from 'app/constants';
import HeaderLeft from 'app/components/header_left';
import Header from 'app/components/header';
import LinearGradient from 'react-native-linear-gradient';
import {getBottomSpace} from 'react-native-iphone-x-helper';
import {bindActionCreators} from 'redux';
import {getItemTopic} from 'app/actions/item_topic';
import {connect} from 'react-redux';
import Device from 'app/utils/device';
import Sound from 'react-native-sound';
import {getAllCategory} from 'app/actions/category';

Sound.setCategory('Playback');

class Congratulation extends Component {
  goBack = () => {
    this.props.navigation.goBack();
    InteractionManager.runAfterInteractions(() => {
      this.getCurrentTime && clearInterval(this.getCurrentTime);
    });
    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
  };
  state = {
    progress: 0,
    playState: 'paused', //playing, paused
    playSeconds: 0,
    duration: 0,
  };

  onShowing = () => {
    this.props.navigation.navigate('OverView');
    this.getCurrentTime && clearInterval(this.getCurrentTime);
    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
  };
  onContinue = () => {
    this.props.navigation.navigate('OverView');
    this.getCurrentTime && clearInterval(this.getCurrentTime);
    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
  };
  componentDidMount = async () => {
    const {userId, topic_id} = this.props.navigation.state.params;
    this.props.actions.getItemTopic(topic_id, userId);
    this.props.actions.getAllCategory(userId);
    await this.play();
    this.timeout = setInterval(() => {
      if (
        this.sound &&
        this.sound.isLoaded() &&
        this.state.playState === 'playing'
      ) {
        this.sound.getCurrentTime(seconds => {
          this.setState({playSeconds: seconds});
        });
      }
    }, 100);
  };

  componentWillUnmount() {
    if (this.sound) {
      this.sound.release();
      this.sound = null;
    }
    if (this.timeout) {
      clearInterval(this.timeout);
    }
  }
  play = async () => {
    if (this.sound) {
      this.sound.play();
      this.setState({playState: 'playing'});
    } else {
      const {congratulation_sound} = this.props.navigation.state.params;
      this.sound = new Sound(congratulation_sound, '', error => {
        if (error) {
          console.log('failed to load the sound', error);
          this.setState({playState: 'paused'});
        } else {
          this.setState({
            playState: 'playing',
            duration: this.sound?.getDuration() || 0,
          });
          this.sound?.play();
        }
      });
    }
  };

  renderProgress = () => {
    const {duration, playSeconds} = this.state;
    let progress;
    if (duration === 0) {
      progress = 0;
    } else {
      progress = (playSeconds / duration) * 100;
    }
    return (
      <View style={styles.containerProgress}>
        <View style={[styles.progress, {width: `${progress}%`}]} />
      </View>
    );
  };
  render() {
    const {source, onShare, title, des} = this.props;
    return (
      <LinearGradient
        colors={['#C638FE', '#5A50D9']}
        style={styles.container}
        start={{x: 0, y: 0}}
        end={{x: 1, y: 1}}>
        <Header>
          <TouchableOpacity onPress={this.goBack}>
            <HeaderLeft
              source={require('assets/images/arrow-left.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
          <TouchableOpacity onPress={onShare}>
            <HeaderRight
              source={require('assets/images/share.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
        </Header>
        {this.renderProgress()}
        <FastImage source={source} resizeMode="cover" style={styles.img} />
        <View style={styles.content}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.des}>{des}</Text>
        </View>
        <TouchableOpacity onPress={this.onShowing} style={styles.btnShare}>
          <Text style={styles.share}>Khoe hàng</Text>
        </TouchableOpacity>
        <TouchableOpacity onPress={this.onContinue} style={styles.btnContinue}>
          <Text style={styles.share}>Tiếp tục</Text>
        </TouchableOpacity>
      </LinearGradient>
    );
  }
}

class WrapCongratulationDay extends Component {
  render() {
    const {navigation} = this.props;
    const index = navigation.getParam('index');
    return (
      <Congratulation
        source={require('assets/images/congratulation_day.png')}
        title={`Đạt mục tiêu ngày ${index + 1}`}
        des={'Chúc mừng bạn đã xuất sắc hoàn thành nhiệm vụ ngày đầu tiên.'}
        navigation={navigation}
        actions={this.props.actions}
      />
    );
  }
}

class WrapCongratulationMonth extends PureComponent {
  render() {
    const {navigation} = this.props;
    return (
      <Congratulation
        source={require('assets/images/congratulation_month.png')}
        title={'Đạt mục tiêu 30 ngày'}
        des={'Chúc mừng bạn đã xuất sắc hoàn thành thói quen.'}
        navigation={navigation}
        actions={this.props.actions}
      />
    );
  }
}

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getItemTopic: getItemTopic,
        getAllCategory: getAllCategory,
      },
      dispatch,
    ),
  };
};
const CongratulationDay = connect(
  null,
  mapDispatchToProps,
)(WrapCongratulationDay);
const CongratulationMonth = connect(
  null,
  mapDispatchToProps,
)(WrapCongratulationMonth);
export {CongratulationDay, CongratulationMonth};

const styles = StyleSheet.create({
  container: {
    flex: 1,
  },
  img: {
    width: Device.screen.width * 0.8,
    height: Device.screen.width * 0.8,
    alignSelf: 'center',
  },
  headerStyle: {
    backgroundColor: 'transparent',
    borderBottomWidth: 0,
    elevation: 0,
    shadowColor: 'rgba(0,0,0,0)',
  },
  title: {
    fontWeight: '600',
    fontSize: 24,
    lineHeight: 32,
    textAlign: 'center',
    color: '#FFFFFF',
  },
  des: {
    fontSize: 16,
    lineHeight: 24,
    textAlign: 'center',
    color: '#FFFFFF',
    marginHorizontal: 40,
    marginTop: 12,
  },
  share: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#FFFFFF',
  },
  btnShare: {
    borderWidth: 1,
    marginHorizontal: 24,
    height: 40,
    width: '90%',
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    borderColor: '#FFFFFF',
    position: 'absolute',
    bottom: getBottomSpace() + 72,
    alignSelf: 'center',
  },
  btnContinue: {
    width: '90%',
    height: 40,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    marginTop: 16,
    backgroundColor: '#FF326F',
    position: 'absolute',
    bottom: getBottomSpace() + 16,
    alignSelf: 'center',
  },
  content: {
    flex: 1,
    alignItems: 'center',
    paddingBottom: getBottomSpace() + 72,
  },
  containerProgress: {
    height: 1,
    width: '100%',
    backgroundColor: 'rgba(255,255,255,0.2)',
  },
  progress: {
    height: 1,
    backgroundColor: '#FFF',
  },
});

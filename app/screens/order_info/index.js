import React from 'react';
import {StyleSheet, Keyboard, Alert} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import validator from 'validator';

import FormattedText from 'app/components/formatted_text';
import Separator from 'app/components/separator';
import Button, {BUTTON_HEIGHT} from 'app/components/button';

import {Colors, Screen} from 'app/constants';
import InputForm from 'app/components/input_form';
import DropDownHolder from 'app/utils/dropdownelert';
import api from 'app/saga/api';
import LoadingHolder from 'app/utils/loading';

class OrderInfo extends React.PureComponent<Props> {
  validate = ({fullname, email, phone, address}) => {
    if (!fullname) {
      DropDownHolder.alert('error', 'Vui lòng nhập tên');
      return false;
    }
    if (!phone) {
      DropDownHolder.alert('error', 'Vui lòng nhập số điện thoại');
      return false;
    }

    if (!validator.isMobilePhone(phone)) {
      DropDownHolder.alert('error', 'Vui lòng nhập đúng số điện thoại');
      return false;
    }

    return true;
  };
  submit = async () => {
    const {navigation} = this.props;

    const fullname = this.orderFullnameRef.state.value;
    const phone = this.orderPhoneRef.state.value;
    const email = this.orderEmailRef.state.value;
    const address = this.orderAddressRef.state.value;

    if (!this.validate({fullname, email, phone, address})) {
      return;
    }
    LoadingHolder.start();

    try {
      const {result} = await api.addOrder({
        fullname,
        email,
        address,
        phone,
      });
      // console.log('result', result);

      if (result.message === 'SUCCESS') {
        navigation.replace('OrderSuccess', {
          orderSuccess: true,
        });
      } else {
        Alert.alert('Xảy ra lỗi', 'Hệ thống xảy ra lỗi, vui lòng thử lại sau');
      }
    } catch (error) {
      console.log('addOrder', error.message);
      Alert.alert(
        'Xảy ra lỗi',
        'Hệ thống xảy ra lỗi, vui lòng thử lại sau' + error.message,
      );
    } finally {
      LoadingHolder.stop();
    }
  };

  render() {
    const {screenProps, navigation} = this.props;
    const {intl} = screenProps;
    return (
      <>
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={styles.listStyle}>
          <FormattedText
            id={'orderEnterInfo'}
            defaultMessage={
              'Vui lòng nhập đầy đủ thông tin cửa bạn để đặt hàng. Xin cảm ơn! '
            }
            style={styles.orderEnterInfo}
          />
          <Separator height={30} />
          <InputForm
            isRequired={true}
            // defaultValue={General.DEFAULT_NAME}
            label={intl.formatMessage({
              id: 'fullname',
              defaultMessage: 'Họ và tên',
            })}
            placeholder={intl.formatMessage({
              id: 'fullname',
              defaultMessage: 'Họ và tên',
            })}
            returnKeyType="next"
            refs={ref => (this.orderFullnameRef = ref)}
            onSubmitEditing={() => {
              this.orderPhoneRef.focus();
            }}
          />
          <Separator height={24} />

          <InputForm
            isRequired={true}
            // defaultValue={General.DEFAULT_NAME}
            label={intl.formatMessage({
              id: 'enterPhone',
              defaultMessage: 'Nhập số điện thoại',
            })}
            placeholder={intl.formatMessage({
              id: 'enterPhone',
              defaultMessage: 'Nhập số điện thoại',
            })}
            keyboardType="number-pad"
            returnKeyType="next"
            refs={ref => (this.orderPhoneRef = ref)}
            onSubmitEditing={() => {
              this.orderEmailRef.focus();
            }}
          />
          <Separator height={24} />

          <InputForm
            // defaultValue={'Test1@gmail.com'}
            // isRequired={true}
            label={intl.formatMessage({
              id: 'form.email',
              defaultMessage: 'Email',
            })}
            placeholder={intl.formatMessage({
              id: 'form.email',
              defaultMessage: 'Email',
            })}
            keyboardType="email-address"
            textContentType="emailAddress"
            returnKeyType="next"
            refs={ref => (this.orderEmailRef = ref)}
            onSubmitEditing={() => {
              this.orderAddressRef.focus();
            }}
          />
          <Separator height={24} />
          <InputForm
            // isRequired={true}
            // defaultValue={General.DEFAULT_NAME}
            label={intl.formatMessage({
              id: 'address',
              defaultMessage: 'Địa chỉ',
            })}
            placeholder={intl.formatMessage({
              id: 'address',
              defaultMessage: 'Địa chỉ',
            })}
            returnKeyType="done"
            refs={ref => (this.orderAddressRef = ref)}
            onSubmitEditing={() => {
              Keyboard.dismiss();
            }}
          />
        </KeyboardAwareScrollView>
        <Button
          label={intl.formatMessage({
            id: 'orderNow',
            defaultMessage: 'Đặt hàng ngay',
          })}
          onPress={() => {
            this.submit();
          }}
          containerStyle={styles.button}
        />
      </>
    );
  }
}
export default OrderInfo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  listStyle: {
    paddingHorizontal: 32,
    paddingTop: 16,
    paddingBottom: Screen.PADDING_BOTTOM + BUTTON_HEIGHT,
  },
  orderEnterInfo: {
    color: Colors.LIGHT,
    fontSize: 16,
    lineHeight: 24,
  },
  button: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    left: 24,
    right: 24,
  },
});

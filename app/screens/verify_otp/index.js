import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';

import SmoothPinCodeInput from 'react-native-smooth-pincode-input';
import {Colors} from 'app/constants';
import Button from 'app/components/button';
import api from 'app/saga/api';
import DropDownHolder from 'app/utils/dropdownelert';
import {RESPONSED_MSG} from 'app/constants/server';

const LIMIT_TIME_SEND_NEW_REQUEST = 30;

class VerifyOtp extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      code: '',
    };

    const {email, otp, token} = props.navigation.state.params;
    this.email = email;
    this.otp = String(otp);
    this.token = token;
  }

  componentDidMount() {
    this.handleNewRequest();
  }

  componentWillUnmount() {
    if (this.countdownTime) {
      clearInterval(this.countdownTime);
    }
    if (this.handleError) {
      clearTimeout(this.handleError);
    }
  }

  requestForgotPassword = async email => {
    try {
      if (email) {
        const data = {
          email: String(email)
            .toLowerCase()
            .trim(),
        };
        const result = await api.callApi('user/forgot-password', 'POST', data);
        // console.log('result', result);

        if (result.token && result.otp) {
          this.token = result.token;
          this.otp = String(result.otp);
        } else if (result.error) {
          const {intl} = this.props.screenProps;
          const messageError =
            RESPONSED_MSG[result.errors[0]] || RESPONSED_MSG.UNDEFINED_ERROR;
          const message = intl.formatMessage({
            id: messageError,
            defaultMessage: 'Xảy ra lỗi, vui lòng thử lại sau.',
          });
          DropDownHolder.alert('error', message);
        }
      }
    } catch (error) {
      // handle error
      console.log('error-requestForgotPassword', error);
    }
  };

  _handleError = () => {
    this.props.navigation.goBack();
  };

  handleNewRequest = () => {
    // new limit time
    if (this.countdownTime) {
      clearInterval(this.countdownTime);
    }
    this.setState({limitTime: LIMIT_TIME_SEND_NEW_REQUEST}, () => {
      // count down time to new request
      this.countdownTime = setInterval(() => {
        const {limitTime} = this.state;
        if (limitTime >= 1) {
          this.setState({limitTime: this.state.limitTime - 1});
        }
      }, 1000);
    });
  };
  onResendOtp = async () => {
    this.requestForgotPassword(this.email);
    this.handleNewRequest();
  };
  getCoundownTime = () => {
    const {limitTime} = this.state;
    if (limitTime < 10) {
      return `00:0${limitTime}.`;
    }
    return `00:${limitTime}.`;
  };

  checkCode = code => {
    // console.log('code', code);
    // console.log('this.otp', this.otp);
    const {screenProps, navigation} = this.props;

    if (!code) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'otpRequired',
          defaultMessage: 'Vui lòng nhập mã OTP',
        }),
      );
    } else if (this.otp && code !== this.otp) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'otpFailure',
          defaultMessage: 'Mã OTP không đúng',
        }),
      );
    } else if (this.otp && this.otp === code) {
      navigation.navigate('ResetPassword', {
        token: this.token,
      });
    }
  };

  render() {
    const {intl} = this.props.screenProps;
    const {code, limitTime} = this.state;
    let Resend = (
      <Text
        // numberOfLines={2}
        adjustsFontSizeToFit={true}
        style={styles.waitNewRequest}
        onPress={this.onResendOtp}>
        {`Chúng tôi đã gửi cho bạn một mã xác nhận.\n Email chứa mã OPT có thể ở trong mục spam hoặc quảng cáo.\nYêu cầu mã mới sau ${this.getCoundownTime()}`}
      </Text>
    );
    if (limitTime === 0) {
      Resend = (
        <Text style={styles.resendTextCtn}>
          {'Nếu không nhận được, '}
          <Text
            numberOfLines={1}
            adjustsFontSizeToFit={true}
            style={styles.resendText}
            onPress={this.onResendOtp}>
            {'Nhấn vào đây'}
          </Text>
        </Text>
      );
    }

    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboardShouldPersistTaps="handled">
        <View style={styles.topContainer}>
          <View style={styles.inputContainer}>
            <SmoothPinCodeInput
              ref={this.pinInput}
              value={code}
              onTextChange={inputCode => this.setState({code: inputCode})}
              onFulfill={this.checkCode}
              onBackspace={this._focusePrevInput}
              cellSpacing={8}
              cellStyleFocused={styles.cellStyleFocused}
              cellSize={48}
              textStyle={styles.codeText}
              cellStyle={styles.cellStyle}
              autoFocus={true}
            />
          </View>
          <View style={styles.resendContainer}>{Resend}</View>
        </View>

        <View style={styles.bottomContainer}>
          <Button
            label={intl.formatMessage({
              id: 'title.continue',
              defaultMessage: 'Tiếp tục',
            })}
            onPress={() => this.checkCode(code)}
            containerStyle={styles.continueBtn}
          />
        </View>
      </ScrollView>
    );
  }
}
export default VerifyOtp;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentContainer: {
    flex: 1,
    paddingHorizontal: 16,
  },
  continueBtn: {
    // marginTop: 24,
    // marginBottom: 32,
    alignSelf: 'stretch',
  },
  topContainer: {
    flex: 1,
    // justifyContent: 'center',
    // alignItems: 'center',
  },
  inputContainer: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
  },
  bottomContainer: {
    flex: 1,
  },
  resendContainer: {
    // position: 'absolute',
    // bottom: 16,
    // right: 0,
    // left: 0,
    justifyContent: 'center',
    alignItems: 'center',
    paddingVertical: 16,
  },
  waitNewRequest: {
    fontSize: 14,
    fontFamily: 'iCielHelveticaNowText-Regular',
    color: Colors.PRIMARY_TEXT,
    textAlign: 'center',
  },
  resendTextCtn: {
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Medium',
    color: Colors.PRIMARY_TEXT,
  },
  resendText: {
    color: Colors.PRIMARY,
  },
  codeText: {
    fontSize: 28,
    fontFamily: 'iCielHelveticaNowText-Medium',
    color: Colors.PRIMARY,
  },
  cellStyle: {
    borderWidth: 0,
    backgroundColor: '#F0F0F0',
    padding: 0,
    justifyContent: 'center',
    alignItems: 'center',
    height: 60,
  },
  cellStyleFocused: {
    borderColor: Colors.PRIMARY,
    borderWidth: 1,
  },
});

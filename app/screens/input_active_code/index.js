import React from 'react';
import {View, Text, StyleSheet, Alert} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import {Colors} from 'app/constants';
import FormattedText from 'app/components/formatted_text';
import InputForm from 'app/components/input_form';
import Button from 'app/components/button';
import DropDownHolder from 'app/utils/dropdownelert';
import ActiveCodeBottom from 'app/components/button/active_code_bottom';
import api from 'app/saga/api';
import LoadingHolder from 'app/utils/loading';
import {getUserInfo} from 'app/selectors/user';
import ModalAlert from 'app/components/modal_alert';
import {updateProfile} from 'app/actions/user';

class InputActiveCode extends React.PureComponent<Props> {
  constructor(props) {
    super(props);
    this.state = {
      errMessage: '',
    };
  }
  onContinue = async () => {
    const {screenProps, navigation, userId} = this.props;
    const activeCode = this.enterActiveCodeRef.state.value;
    // check account and password field
    if (!activeCode) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'acctiveCodeRequired',
          defaultMessage: 'Vui lòng nhập mã kích hoạt',
        }),
      );
    } else {
      // console.log('activeCode', activeCode);
      LoadingHolder.start();

      try {
        const data = {
          userId,
          code: activeCode,
        };
        console.log('data', data);

        const {result} = await api.activeAccount(data);
        console.log('result', result);

        if (result.error === true) {
          // Error
          let errorMsg = '';

          if (result.errors[0] === 'ERROR_CODE_LENGHT') {
            errorMsg = 'Chiều dài mã kích hoạt không đúng, vui lòng thử lại.';
          } else {
            errorMsg =
              'Mã kích hoạt không đúng hoặc đã được kích hoạt, vui lòng thử lại.';
          }
          // Totifice error
          this.showError(errorMsg);
        } else {
          console.log('result ', result);
          this.props.actions.updateProfile(result);
          navigation.replace('ActiveSuccess', {
            resultQrcode: result,
          });
        }
      } catch (error) {
        console.log('activeAccount', error.message);
        // Alert.alert(
        //   'Xảy ra lỗi',
        //   'Hệ thống xảy ra lỗi, vui lòng thử lại sau' + error.message,
        // );
        this.showError(
          'Hệ thống xảy ra lỗi, vui lòng thử lại sau' + error.message,
        );
      } finally {
        LoadingHolder.stop();
      }
    }
  };

  onQRCode = () => {
    this.props.navigation.replace('ScanQRCode');
  };
  showError = errMessage => {
    this.setState({errMessage}, () => this.ModalRef.onShow());
  };

  render() {
    const {screenProps, navigation} = this.props;
    const {errMessage} = this.state;
    const code = navigation.getParam('code') || '';

    return (
      <View style={styles.container}>
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={styles.contentStyle}
          keyboardShouldPersistTaps={'handled'}>
          <FormattedText
            numberOfLines={2}
            id="enterActiveCodeBehindBook"
            defaultMessage="Nhập mã kích hoạt tài khoản\nđược ghi ở bìa sau sách"
            style={styles.enterActiveCodeBehindBook}
          />
          <InputForm
            noLabel={true}
            defaultValue={code}
            placeholder={screenProps.intl.formatMessage({
              id: 'form.enterActiveCode',
              defaultMessage: 'Mã kích hoạt',
            })}
            returnKeyType="done"
            refs={ref => (this.enterActiveCodeRef = ref)}
            onSubmitEditing={this.onContinue}
          />
          <Button
            label={screenProps.intl.formatMessage({
              id: 'title.continue',
              defaultMessage: 'Tiếp tục',
            })}
            onPress={this.onContinue}
            containerStyle={{marginTop: 40}}
          />
        </KeyboardAwareScrollView>
        <ActiveCodeBottom onQRCode={this.onQRCode} />
        <ModalAlert
          ref={ref => (this.ModalRef = ref)}
          title={'Đã có lỗi xảy ra!'}
          type="fail"
          content={errMessage}
          navigation={navigation}
          showButton={false}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const userId = getUserInfo(state)._id;

  return {
    userId,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        updateProfile: updateProfile,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(InputActiveCode);
const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentStyle: {
    padding: 24,
    paddingTop: 88 + 24,
  },
  enterActiveCodeBehindBook: {
    fontSize: 16,
    color: Colors.PRIMARY_TEXT,
    textAlign: 'center',
    marginBottom: 56,
  },
});

import React from 'react';
import {
  View,
  Text,
  StyleSheet,
  FlatList,
  TouchableOpacity,
  RefreshControl,
  Image,
} from 'react-native';
import {Colors, General} from 'app/constants';
import ItemCategory from './item_category';
import SearchBar from 'app/components/search_bar';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllCategory} from 'app/actions/category';
import {getUserInfo} from 'app/selectors/user';
import LoadingView from 'app/components/loading_view';

class Category extends React.PureComponent<Props> {
  static navigationOptions = ({navigation, screenProps}) => {
    let title =
      (navigation.state.params && navigation.state.params.fullname) || '';
    return {
      title: title,
      headerLayoutPreset: 'left',
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
    };
  }

  componentDidMount = () => {
    this.props.navigation.setParams({
      fullname: this.props.fullname,
    });
    this.props.actions.getAllCategory(this.props.userId);
  };

  onSearchCategory = () => {
    this.props.navigation.navigate('SearchCategory');
  };

  onRefresh = () => {
    this.props.actions.getAllCategory();
  };

  renderItem = ({item, index}) => {
    return (
      <ItemCategory
        item={item}
        index={index}
        navigation={this.props.navigation}
        title={item.title}
        thumbnail={item.thumbnail}
        id={item._id}
      />
    );
  };

  headerList = () => {
    const {data} = this.props;
    return (
      <>
        <TouchableOpacity onPress={this.onSearchCategory}>
          <SearchBar style={styles.searchBar} noEditable />
        </TouchableOpacity>
        {data.length !== 0 ? (
          <Text style={styles.titleCategory}>Danh mục thói quen</Text>
        ) : null}
      </>
    );
  };
  renderEmpty = () => {
    return (
      <View style={styles.emptyView}>
        <Image
          source={require('assets/images/empty.png')}
          style={styles.imgEmpty}
        />
      </View>
    );
  };

  keyExtractor = (item, index) => index.toString();

  render() {
    const {isLoading, error, data, fullname} = this.props;
    const {isRefreshing} = this.state;

    return (
      <View style={styles.container}>
        {isLoading && !data && <LoadingView />}
        {data && (
          <FlatList
            data={data}
            renderItem={this.renderItem}
            numColumns={2}
            keyExtractor={this.keyExtractor}
            contentContainerStyle={{flex: 1}}
            style={styles.containerFlatList}
            showsVerticalScrollIndicator={false}
            ListHeaderComponent={this.headerList}
            refreshControl={
              <RefreshControl
                tintColor={Colors.LIGHT}
                refreshing={isRefreshing}
                onRefresh={this.onRefresh}
              />
            }
            ListEmptyComponent={this.renderEmpty}
          />
        )}
      </View>
    );
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  searchBar: {
    marginTop: 20,
    backgroundColor: Colors.PRIMARY_2,
  },
  titleCategory: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 17,
    marginTop: 32,
    marginBottom: 23,
    color: Colors.PRIMARY_TEXT,
  },
  containerFlatList: {
    marginHorizontal: 24,
  },
  emptyView: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  imgEmpty: {
    width: 300,
    height: 150,
    resizeMode: 'cover',
  },
});

const mapStateToProps = state => {
  const isLoading = state.category.loading;
  const error = state.category.error;
  const data = state.category.data;
  const user = getUserInfo(state);

  return {
    isLoading,
    error,
    data,
    fullname: user.fullname || General.DEFAULT_NAME,
    userId: user._id,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getAllCategory: getAllCategory,
      },
      dispatch,
    ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Category);

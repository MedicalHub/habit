import React, {Component} from 'react';
import {TouchableOpacity, StyleSheet, Text} from 'react-native';
import {Screen} from 'app/constants';
import PropTypes from 'prop-types';
import FastImage from 'react-native-fast-image';
import API from 'app/saga/api';
import LinearGradient from 'react-native-linear-gradient';
import generate from 'app/utils/gradientColors';

class ItemCategory extends Component {
  colors = generate();

  onDetailCategory = () => {
    const {id, navigation, title} = this.props;
    navigation.navigate('DetailCategory', {
      categoryId: id,
      title: title,
    });
  };

  render() {
    const {index, title, thumbnail} = this.props;
    return (
      <TouchableOpacity
        style={[
          styles.container,
          {
            marginLeft: index % 2 === 1 ? MARGIN_ITEM : 0,
          },
        ]}
        onPress={this.onDetailCategory}>
        <FastImage
          source={{uri: API.urlImage(thumbnail)}}
          resizeMode="cover"
          style={styles.image}
        />
        <LinearGradient
          colors={this.colors}
          style={{
            ...StyleSheet.absoluteFillObject,
          }}
          start={{x: 0.2, y: 0.2}}
          end={{x: 0.2, y: 1}}
        />

        <Text
          style={styles.title}
          numberOfLines={1}
          adjustsFontSizeToFit={true}>
          {title}
        </Text>
      </TouchableOpacity>
    );
  }
}

export default ItemCategory;
ItemCategory.propTypes = {
  imageSource: PropTypes.any,
  title: PropTypes.string,
};

ItemCategory.defaultProps = {
  title: 'Học tập',
  imageSource: require('assets/images/category_item_1.png'),
};
const MARGIN_HORIZONTAL = 24;
const MARGIN_ITEM = 20;

const styles = StyleSheet.create({
  container: {
    width: (Screen.SCREEN_WIDTH - MARGIN_ITEM - MARGIN_HORIZONTAL * 2) / 2,
    height: 140,
    marginBottom: 20,
    borderRadius: 8,
    justifyContent: 'flex-end',
    overflow: 'hidden',
  },
  title: {
    marginLeft: 12,
    marginRight: 5,
    bottom: 14,
    fontSize: 16,
    color: '#fff',
    fontWeight: '500',
    position: 'absolute',
    zIndex: 2,
    fontFamily: 'iCielHelveticaNowText-Medium',
  },
  image: {
    flex: 1,
    opacity: 0.9,
  },
});

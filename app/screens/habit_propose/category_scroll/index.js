import React, {Component} from 'react';
import {
  View,
  ScrollView,
  StyleSheet,
  Text,
  TouchableOpacity,
  Image,
} from 'react-native';
import {Colors} from 'app/constants';
import FastImage from 'react-native-fast-image';
import TouchableText from 'app/components/touchable_text';
import API from 'app/saga/api';

class CategoryScroll extends Component {
  onDetailHabit = (title, _id, thumbnail) => {
    this.props.navigation.navigate('DetailHabit', {
      topicId: _id,
      title: title,
      thumbnail: thumbnail,
    });
  };
  onDetailCategory = () => {
    const {categoryId, navigation, title} = this.props;
    navigation.navigate('DetailCategory', {
      categoryId,
      title: title,
    });
  };
  render() {
    const {category, title} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.header}>
          <Text style={styles.textLeft}>{title}</Text>
          <TouchableText
            label={'Tất cả'}
            style={styles.textRight}
            onPress={this.onDetailCategory}
          />
        </View>
        <ScrollView
          horizontal
          contentContainerStyle={styles.contentContainerStyle}
          showsHorizontalScrollIndicator={false}>
          {category.map(item => {
            const {title, _id, thumbnail, completed_count, item_count} = item;
            let complete = completed_count === item_count;
            return (
              <TouchableOpacity
                onPress={() => this.onDetailHabit(title, _id, thumbnail)}>
                <FastImage
                  source={{uri: API.urlImage(item.thumbnail)}}
                  resizeMode="cover"
                  style={styles.image}
                  key={item.id}>
                  <View style={styles.block} />
                  <Text style={styles.title} numberOfLines={2}>
                    {title}
                  </Text>
                  {complete && (
                    <Image
                      source={require('assets/images/fill-check.png')}
                      style={styles.fillCheck}
                    />
                  )}
                  {Boolean(completed_count) && (
                    <Text style={styles.date}>
                      {`${completed_count}/${item_count} ngày`}
                    </Text>
                  )}
                </FastImage>
              </TouchableOpacity>
            );
          })}
        </ScrollView>
      </View>
    );
  }
}

export default CategoryScroll;

const styles = StyleSheet.create({
  container: {
    backgroundColor: Colors.PRIMARY_2,
    paddingTop: 24,
    marginBottom: 2,
  },
  image: {
    width: 153,
    height: 140,
    marginRight: 12,
    borderRadius: 8,
  },
  contentContainerStyle: {
    paddingLeft: 16,
    paddingBottom: 8,
  },
  title: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 19,
    color: Colors.LIGHT,
    position: 'absolute',
    bottom: 12,
    marginLeft: 12,
    marginRight: 5,
  },
  header: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    paddingHorizontal: 16,
    marginBottom: 16,
  },
  textLeft: {
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 20,
    color: '#8798CD',
  },
  textRight: {
    fontWeight: '500',
    fontSize: 14,
    lineHeight: 20,
    color: '#FFBE00',
  },
  block: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(15, 27, 61,0.4)',
  },
  fillCheck: {
    width: 24,
    height: 24,
    position: 'absolute',
    right: 2,
    top: 2,
  },
  date: {
    color: '#FFBE00',
    fontSize: 12,
    position: 'absolute',
    top: 6,
    right: 8,
  },
});

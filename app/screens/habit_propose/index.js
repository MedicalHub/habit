import React from 'react';
import {View, StyleSheet, ScrollView, RefreshControl} from 'react-native';
import {Colors} from 'app/constants';
import CategoryScroll from 'app/screens/habit_propose/category_scroll';
import CarouselHabit from 'app/screens/habit_propose/carousel';
import {getUserInfo} from 'app/selectors/user';
import {bindActionCreators} from 'redux';
import {getAllCategory} from 'app/actions/category';
import {connect} from 'react-redux';
import LoadingView from 'app/components/loading_view';

class HabitPropose extends React.PureComponent<Props> {
  state = {
    isRefreshing: false,
  };

  componentDidMount = () => {
    this.props.actions.getAllCategory(this.props.userId);
  };

  onRefresh = () => {
    this.props.actions.getAllCategory(this.props.userId);
  };

  render() {
    const {isLoading, data, navigation} = this.props;

    return (
      <View style={styles.container}>
        {isLoading && !data && <LoadingView />}
        {data && (
          <ScrollView
            style={styles.scrollView}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                tintColor={Colors.LIGHT}
                refreshing={this.state.isRefreshing}
                onRefresh={this.onRefresh}
              />
            }>
            <View style={styles.carousel}>
              <CarouselHabit data={data} navigation={navigation} />
            </View>
            {data.map(item => {
              return (
                <CategoryScroll
                  category={item.topics}
                  title={item.title}
                  key={item._id}
                  navigation={navigation}
                  categoryId={item._id}
                />
              );
            })}
          </ScrollView>
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const isLoading = state.category.loading;
  const error = state.category.error;
  const data = state.category.data;
  const user = getUserInfo(state);

  return {
    isLoading,
    error,
    data,
    userId: user._id,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getAllCategory: getAllCategory,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HabitPropose);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  scrollView: {
    paddingBottom: 10,
  },

  carousel: {
    height: 230,
  },
});

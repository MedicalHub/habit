import React, {Component} from 'react';
import {Dimensions, StyleSheet, Text, View} from 'react-native';
import Carousel, {Pagination} from 'react-native-snap-carousel';
import TouchableText from 'app/components/touchable_text';
import FastImage from 'react-native-fast-image';
import {Colors} from 'app/constants';
import API from 'app/saga/api';

const {width: viewportWidth} = Dimensions.get('window');

function wp(percentage) {
  const value = (percentage * viewportWidth) / 100;
  return Math.round(value);
}

const slideHeight = 170;
const slideWidth = wp(75);
const itemHorizontalMargin = wp(2);
export const sliderWidth = viewportWidth;
export const itemWidth = slideWidth + itemHorizontalMargin * 2;

class CarouselHabit extends Component {
  state = {
    activeSlide: 0,
    dataTopic: [],
  };

  componentDidMount() {
    let {data} = this.props; //data category
    if (!data) {
      return;
    }
    let topics = [];
    for (let i = 0; i < data.length; i++) {
      if (topics.length === 3) {
        break;
      }
      let _topics = data[i].topics; //mang topics
      if (!_topics) {
        return;
      }
      for (let j = 0; j < _topics.length; j++) {
        let topic = _topics[j];
        let {completed_count, item_count} = topic;
        if (completed_count < item_count) {
          topics.push(topic);
        }
      }
    }
    this.setState({
      dataTopic: topics,
    });
  }

  onDetailHabit = (title, id, thumbnail) => {
    const {navigation} = this.props;
    navigation.navigate('DetailHabit', {
      topicId: id,
      title: title,
      thumbnail: thumbnail,
    });
  };

  onSnapToItem = index => this.setState({activeSlide: index});

  _renderItem = ({item}) => {
    const {title, _id, thumbnail, completed_count, item_count} = item;
    return (
      <FastImage
        style={styles.slideInnerContainer}
        source={{uri: API.urlImage(thumbnail)}}
        resizeMode={'cover'}>
        <View style={styles.block} />
        <Text style={styles.title}>{title}</Text>
        <TouchableText
          label={'Bắt đầu ngay'}
          onPress={() => this.onDetailHabit(title, _id, thumbnail)}
          styleContainer={styles.styleContainer}
          style={styles.styleTitle}
        />
      </FastImage>
    );
  };

  render() {
    const {activeSlide, dataTopic} = this.state;
    return (
      <>
        <Carousel
          data={dataTopic}
          renderItem={this._renderItem}
          sliderWidth={sliderWidth}
          itemWidth={itemWidth}
          inactiveSlideScale={0.94}
          inactiveSlideOpacity={0.7}
          containerCustomStyle={styles.slider}
          contentContainerCustomStyle={styles.sliderContentContainer}
          onSnapToItem={this.onSnapToItem}
        />
        <Pagination
          dotsLength={3}
          activeDotIndex={activeSlide}
          dotStyle={styles.dotStyle}
          inactiveDotStyle={styles.inactiveDotStyle}
          containerStyle={styles.containerStyle}
        />
      </>
    );
  }
}

export default CarouselHabit;
const styles = StyleSheet.create({
  slideInnerContainer: {
    width: itemWidth,
    height: slideHeight,
    paddingHorizontal: itemHorizontalMargin,
    borderRadius: 4,
    overflow: 'hidden',
    justifyContent: 'center',
    alignItems: 'center',
  },
  sliderContentContainer: {
    paddingVertical: 10, // for custom animation
  },
  slider: {
    marginTop: 15,
    height: 300,
    overflow: 'visible', // for custom animations
  },
  title: {
    fontWeight: '500',
    fontSize: 22,
    lineHeight: 26,
    color: Colors.LIGHT,
    marginBottom: 20,
  },
  styleContainer: {
    backgroundColor: 'rgba(15, 27, 61, 0.2)',
    height: 40,
    width: 139,
    borderRadius: 8,
    borderWidth: 1,
    borderColor: '#FFBE00',
    justifyContent: 'center',
    alignItems: 'center',
  },
  styleTitle: {
    fontWeight: '500',
    fontSize: 16,
    lineHeight: 24,
    color: '#FFBE00',
  },
  dotStyle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
    backgroundColor: '#FF326F',
  },
  inactiveDotStyle: {
    width: 8,
    height: 8,
    borderRadius: 4,
    marginHorizontal: 0,
    backgroundColor: 'rgba(255, 255, 255, 0.3)',
  },
  containerStyle: {
    padding: 0,
    margin: 0,
    minHeight: 8,
    height: 32,
    paddingHorizontal: 0,
    paddingVertical: 0,
  },
  block: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: '#0F1B3D',
    opacity: 0.4,
  },
});

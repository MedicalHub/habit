import React from 'react';

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import AsyncStorage from '@react-native-community/async-storage';
import {getUserInfo} from 'app/selectors/user';

import API from 'app/saga/api';
import Loading from 'app/components/loading';
import {Colors} from 'app/constants';

class AuthLoading extends React.PureComponent {
  constructor(props) {
    super(props);
    this.bootstrapAsync();
  }

  checkFirstLauchApp = async () => {
    try {
      const value = await AsyncStorage.getItem('@FirstLogin');
      if (value !== null) {
        return false;
      }
      return true;
    } catch (e) {
      console.log('checkFirstLauchApp-error', e);
      return false;
    }
  };

  // TODO:
  bootstrapAsync = async () => {
    const {navigation, isLogin, token} = this.props;

    let screen = 'Login';

    // Kiểm tra lần đầu cài app mà chưa login thì vào màn hình splash intro
    const isFirstLaunch = await this.checkFirstLauchApp();

    if (isFirstLaunch) {
      screen = 'SplashIntro';
    } else if (isLogin) {
      API.setToken(token);

      screen = 'AppMain';
    }

    navigation.navigate(screen);
  };
  render() {
    return (
      <Loading
        isLoading={true}
        containerStyle={{backgroundColor: Colors.BACKGROUND_PRIMARY}}
        color={Colors.ACTIVE_TINT}
        size={100}
      />
    );
  }
}

const mapStateToProps = state => {
  const isLogin = state.user.isLogin;
  const token = getUserInfo(state).token;

  return {
    isLogin,
    token,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({}, dispatch),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(AuthLoading);

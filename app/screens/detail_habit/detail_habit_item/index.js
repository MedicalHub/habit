import React, {Component} from 'react';
import {StyleSheet, View, Text, TouchableOpacity, Alert} from 'react-native';
import {Colors} from 'app/constants';
import FastImage from 'react-native-fast-image';
import moment from 'moment';

class DetailHabitItem extends Component {
  onAudio = () => {
    const {
      audio,
      title,
      description,
      thumbnail,
      index,
      time,
      id,
      expiredTime,
    } = this.props;
    const isExpired = expiredTime
      ? new Date(expiredTime).getTime() > new Date().getTime()
        ? true
        : false
      : false;
    if (id > 1) {
      if (isExpired) {
        this.props.navigation.navigate('Audio', {
          audio: audio,
          title: title,
          description: description,
          thumbnail: thumbnail,
          index: index,
          time: time,
        });
      } else {
        this.props.showError && this.props.showError();
      }
    } else {
      this.props.navigation.navigate('Audio', {
        audio: audio,
        title: title,
        description: description,
        thumbnail: thumbnail,
        index: index,
        time: time,
      });
    }
  };

  render() {
    const {id, title, time, is_locked, is_completed} = this.props;
    return (
      <TouchableOpacity
        style={[
          styles.container,
          {
            opacity: is_locked ? 0.5 : 1,
          },
        ]}
        onPress={this.onAudio}
        disabled={!!is_locked}>
        <View style={styles.contentSTT}>
          <Text style={styles.stt}>{id}</Text>
        </View>
        <View style={styles.content}>
          <Text style={styles.title}>{title}</Text>
          <Text style={styles.time}>{moment(time * 1000).format('mm:ss')}</Text>
        </View>
        {is_completed && (
          <FastImage
            source={require('assets/images/check.png')}
            resizeMode="cover"
            style={styles.check}
          />
        )}
        {is_locked && (
          <FastImage
            source={require('assets/images/lock.png')}
            resizeMode="cover"
            style={styles.check}
          />
        )}
        <FastImage
          source={require('assets/images/small_play.png')}
          resizeMode="cover"
          style={styles.smallPlay}
        />
      </TouchableOpacity>
    );
  }
}

export default DetailHabitItem;

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    paddingBottom: 20,
    borderBottomWidth: 1,
    borderBottomColor: '#182651',
    alignItems: 'center',
  },
  contentSTT: {
    width: 44,
    height: 44,
    borderRadius: 8,
    backgroundColor: '#182651',
    justifyContent: 'center',
    alignItems: 'center',
  },
  stt: {
    color: '#FFBE00',
    fontWeight: 'bold',
    fontSize: 16,
    lineHeight: 19,
  },
  content: {
    marginLeft: 14,
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    lineHeight: 19,
    color: Colors.LIGHT,
  },
  time: {
    fontSize: 12,
    lineHeight: 14,
    color: '#8798CD',
    marginTop: 6,
  },
  check: {
    width: 24,
    height: 24,
  },
  smallPlay: {
    width: 32,
    height: 32,
    marginLeft: 12,
  },
});

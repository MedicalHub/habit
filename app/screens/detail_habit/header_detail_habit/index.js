import React, {Component} from 'react';
import {View, StyleSheet, TouchableOpacity, Text} from 'react-native';
import {Colors} from 'app/constants';
import Header from 'app/components/header';
import HeaderLeft from 'app/components/header_left';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import FastImage from 'react-native-fast-image';
import {Circle} from 'react-native-svg';
import HeaderRight from 'app/components/header_right';
import API from 'app/saga/api';

class HeaderDetailHabit extends Component {
  renderCap = ({center}) => {
    return <Circle cx={center.x} cy={center.y} r="4" fill={'#FFBE00'} />;
  };

  goBack = () => {
    this.props.navigation.goBack();
  };

  render() {
    const {title, thumbnail, data} = this.props;
    let number_complete = 0;
    let progress = 0;
    let length = 0;
    if (data && data.length !== 0) {
      for (let i = 0; i < data.length; i++) {
        if (data[i].is_completed === 1) {
          number_complete = number_complete + 1;
        }
      }
      progress = parseInt((number_complete / data.length) * 100);
      length = data.length;
    }
    return (
      <View style={styles.container}>
        <FastImage
          style={styles.image}
          source={{uri: API.urlImage(thumbnail)}}
          resizeMode={'cover'}
        />
        <FastImage
          style={styles.block}
          source={require('assets/images/Block.png')}
          resizeMode={'cover'}
        />
        <Header>
          <TouchableOpacity onPress={this.goBack}>
            <HeaderLeft
              source={require('assets/images/arrow-left.png')}
              color={Colors.LIGHT}
            />
          </TouchableOpacity>
          {/*<TouchableOpacity onPress={this.goBack}>*/}
          {/*  <HeaderRight*/}
          {/*    source={require('assets/images/share.png')}*/}
          {/*    color={Colors.LIGHT}*/}
          {/*  />*/}
          {/*</TouchableOpacity>*/}
        </Header>
        <View style={styles.info}>
          <View style={styles.content}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.days}>
              {number_complete}/{length} ngày
            </Text>
          </View>
          <AnimatedCircularProgress
            size={70}
            width={4}
            fill={progress}
            tintColor="#FFBE00"
            rotation={360}
            backgroundColor={Colors.PRIMARY_3}
            padding={10}
            renderCap={this.renderCap}>
            {fill => <Text style={styles.process}>{progress}%</Text>}
          </AnimatedCircularProgress>
        </View>
      </View>
    );
  }
}

export default HeaderDetailHabit;

const styles = StyleSheet.create({
  container: {
    height: 220,
    justifyContent: 'space-between',
  },
  content: {
    flex: 1,
  },
  title: {
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21,
    color: Colors.LIGHT,
  },
  days: {
    fontSize: 12,
    lineHeight: 14,
    color: '#FFBE00',
  },
  process: {
    fontSize: 12,
    fontWeight: 'bold',
    lineHeight: 14,
    color: '#FFC500',
  },
  info: {
    height: 100,
    flexDirection: 'row',
    paddingHorizontal: 16,
    alignItems: 'center',
    marginBottom: 40,
  },
  image: {
    width: '100%',
    height: '100%',
    position: 'absolute',
  },
  block: {
    width: '100%',
    height: 350,
    position: 'absolute',
  },
});

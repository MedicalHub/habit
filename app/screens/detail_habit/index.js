import React, {PureComponent} from 'react';
import {
  View,
  StyleSheet,
  Text,
  FlatList,
  InteractionManager,
  Image,
  RefreshControl,
  Platform,
} from 'react-native';
import {Colors} from 'app/constants';
import HeaderDetailHabit from 'app/screens/detail_habit/header_detail_habit';
import DetailHabitItem from 'app/screens/detail_habit/detail_habit_item';
import Separator from 'app/components/separator';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getItemTopic, resetItemTopic} from 'app/actions/item_topic';
import LoadingHolder from 'app/utils/loading';
import DropDownHolder from 'app/utils/dropdownelert';
import {getUserInfo} from 'app/selectors/user';
import TrackPlayer from 'react-native-track-player';
import reactotron from 'reactotron-react-native';
import ModalAlert from 'app/components/modal_alert';

class DetailHabit extends PureComponent {
  state = {
    isRefreshing: false,
  };

  componentDidMount() {
    const {topicId} = this.props.navigation.state.params;
    // console.warn(topicId);
    InteractionManager.runAfterInteractions(() => {
      this.props.actions.getItemTopic(topicId, this.props.userId);
    });
  }

  componentWillUnmount() {
    this.props.actions.resetItemTopic();
    TrackPlayer.reset();
  }

  async componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      LoadingHolder.stop();
      if (this.props.error) {
        DropDownHolder.alert('error', this.props.error);
      }
    }
  }

  onRefresh = () => {
    const {topicId} = this.props.navigation.state.params;
    this.props.actions.getItemTopic(topicId, this.props.userId);
  };

  renderItem = ({item, index}) => {
    return (
      <DetailHabitItem
        expiredTime={this.props.expiredTime}
        showError={this.showError}
        title={item.title}
        description={item.description}
        time={item.time}
        status={item.status}
        navigation={this.props.navigation}
        id={index + 1}
        audio={item.audio}
        is_locked={item.is_locked}
        is_completed={item.is_completed}
        thumbnail={item.thumbnail}
        index={index}
      />
    );
  };

  renderSeparator = () => <Separator height={24} />;

  headerList = () => {
    const {data} = this.props;
    const days = data?.length !== 0 ? data.length : null;
    if (days) {
      return <Text style={styles.days}>{days} NGÀY</Text>;
    }
    return null;
  };
  renderEmpty = () => {
    return (
      <View style={styles.emptyView}>
        <Image
          source={require('assets/images/empty.png')}
          style={styles.imgEmpty}
        />
      </View>
    );
  };
  keyExtractor = (item, index) => index.toString();

  showError = () => {
    this.ModalRef.onShow();
  };

  render() {
    const {navigation, data} = this.props;
    const {title, thumbnail} = navigation.state.params;
    const {isRefreshing} = this.state;
    return (
      <View style={styles.container}>
        <HeaderDetailHabit
          navigation={navigation}
          title={title}
          thumbnail={thumbnail}
          data={data}
        />
        <View style={styles.content}>
          {data && (
            <FlatList
              data={data}
              renderItem={this.renderItem}
              keyExtractor={this.keyExtractor}
              style={styles.containerFlatList}
              showsVerticalScrollIndicator={false}
              ItemSeparatorComponent={this.renderSeparator}
              contentContainerStyle={styles.contentContainer}
              ListHeaderComponent={this.headerList}
              ListEmptyComponent={this.renderEmpty}
              refreshControl={
                <RefreshControl
                  tintColor={Colors.LIGHT}
                  refreshing={isRefreshing}
                  onRefresh={this.onRefresh}
                />
              }
              bounces={false}
            />
          )}
        </View>
        <ModalAlert
          ref={ref => (this.ModalRef = ref)}
          title={'Thông báo'}
          type="fail"
          titleCancel={'Đặt hàng'}
          titleAccept={'Thoát'}
          content={'Bạn cần kích hoạt tài khoản để tiếp tục việc này'}
          showButton={true}
          navigation={navigation}
          onCancel={() => navigation.navigate('OrderInfo')}
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const isLoading = state.item_topic.loading;
  const error = state.item_topic.error;
  const data = state.item_topic.data;
  const dataAudio = state.item_topic.dataAudio;
  const user = getUserInfo(state);
  return {
    isLoading,
    error,
    data,
    userId: user._id,
    userRole: user.role,
    expiredTime: user?.expired_time || null,
    dataAudio,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getItemTopic: getItemTopic,
        resetItemTopic: resetItemTopic,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailHabit);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  content: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
    marginTop: -40,
    borderRadius: 24,
  },
  days: {
    color: '#8798CD',
    marginBottom: 24,
    fontWeight: '500',
  },
  contentContainer: {
    padding: 16,
  },
  containerFlatList: {
    flex: 1,
  },
  emptyView: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  imgEmpty: {
    width: 300,
    height: 150,
    resizeMode: 'cover',
  },
});

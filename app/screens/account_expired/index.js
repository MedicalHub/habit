import React from 'react';
import {View, StyleSheet, Image} from 'react-native';

import FormattedText from 'app/components/formatted_text';
import Separator from 'app/components/separator';
import Button from 'app/components/button';

import {Colors, Screen} from 'app/constants';

class AccountExpired extends React.PureComponent<Props> {
  render() {
    const {screenProps, navigation} = this.props;
    const {intl} = screenProps;
    return (
      <View style={styles.container}>
        <Image
          source={require('assets/images/acc-expired-bg.png')}
          resizeMode="cover"
        />
        <Separator height={32} />
        <FormattedText
          id={'accountExpired'}
          defaultMessage={'Tài khoản Hết Hạn'}
          style={styles.accountExpired}
        />
        <Separator height={16} />
        <FormattedText
          id={'accountExpiredNote'}
          defaultMessage={'Tài khoản Hết Hạn'}
          style={styles.accountExpiredNote}
        />
        <Button
          label={intl.formatMessage({
            id: 'order',
            defaultMessage: 'Tiến hành đặt hàng',
          })}
          onPress={() => navigation.replace('OrderInfo')}
          containerStyle={styles.button}
        />
      </View>
    );
  }
}
export default AccountExpired;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  accountExpired: {
    color: Colors.ACTIVE_TINT,
    fontSize: 22,
    fontWeight: '500',
    textAlign: 'center',
  },
  accountExpiredNote: {
    color: Colors.PRIMARY_TEXT,
    fontSize: 16,
    textAlign: 'center',
  },
  button: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    left: 24,
    right: 24,
  },
});

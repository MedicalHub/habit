import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';

import ProfileItem from './item';

import Separator from 'app/components/separator';
import Button from 'app/components/button';

import {Colors, General, Screen} from 'app/constants';
import {formatPhoneNumber} from 'app/utils/phone';

class ProfileInfo extends React.PureComponent<Props> {
  render() {
    const {screenProps, navigation, fullname, email, phone} = this.props;
    const {intl} = screenProps;

    let phoneNumber = '';
    if (phone) {
      phoneNumber = formatPhoneNumber(String(phone));
    }

    return (
      <>
        <ScrollView
          style={styles.container}
          contentContainerStyle={styles.content}>
          <ProfileItem
            label={intl.formatMessage({
              id: 'nickname',
              defaultMessage: 'Tên hiển thị',
            })}
            value={fullname || General.DEFAULT_NAME}
          />
          <Separator height={24} />
          <ProfileItem
            label={intl.formatMessage({
              id: 'form.email',
              defaultMessage: 'Email',
            })}
            value={email || ''}
          />
          <Separator height={24} />
          <ProfileItem
            label={intl.formatMessage({
              id: 'form.phone',
              defaultMessage: 'Số ĐT',
            })}
            value={phoneNumber}
          />
        </ScrollView>
        <Button
          label={intl.formatMessage({
            id: 'editProfileInfo',
            defaultMessage: 'Chỉnh sửa thông tin',
          })}
          onPress={() => navigation.navigate('EditProfileInfo')}
          containerStyle={styles.button}
        />
      </>
    );
  }
}
export default ProfileInfo;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  content: {
    paddingHorizontal: 24,
    paddingTop: 24,
  },
  button: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    left: 24,
    right: 24,
  },
});

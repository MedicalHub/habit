import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ProfileInfo from 'app/screens/profile_info/profile_info';

import {getUserInfo} from 'app/selectors/user';

const mapStateToProps = state => {
  const user = getUserInfo(state);

  return {
    fullname: user.fullname,
    email: user.email,
    phone: user.phone,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators({}, dispatch),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ProfileInfo);

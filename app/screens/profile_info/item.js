/* eslint-disable react-native/no-inline-styles */

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import PropTypes from 'prop-types';
import {Colors} from 'app/constants';

export default function ProfileItem(props) {
  const {label, value, noBorderBottom} = props;
  return (
    <View
      style={[
        styles.container,
        {
          borderBottomWidth: noBorderBottom ? 0 : 1,
        },
      ]}>
      <Text style={styles.label}>{label}</Text>
      <Text style={styles.value}>{value}</Text>
    </View>
  );
}

ProfileItem.propTypes = {
  label: PropTypes.string,
  value: PropTypes.any,
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    height: 46,
    borderBottomColor: Colors.BORDER_PRIMARY,
    borderBottomWidth: 1,
    justifyContent: 'space-between',
    paddingBottom: 24,
    alignItems: 'flex-end',
  },
  label: {
    color: Colors.PRIMARY_TEXT,
    fontSize: 14,
  },
  value: {
    flex: 1,
    color: Colors.LIGHT,
    fontSize: 16,
    textAlign: 'right',
    paddingLeft: 10,
  },
});

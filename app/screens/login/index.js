import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Login from 'app/screens/login/login';

import {login} from 'app/actions/user';

const mapStateToProps = state => {
  const isLoading = state.request.login.loading;
  const error = state.request.login.error;
  return {
    isLoading,
    error,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        login: login,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Login);

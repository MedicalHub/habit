import React from 'react';
import {
  View,
  StyleSheet,
  TouchableWithoutFeedback,
  Keyboard,
} from 'react-native';

import AsyncStorage from '@react-native-community/async-storage';
import validator from 'validator';

import FormattedText from 'app/components/formatted_text';
import InputForm from 'app/components/input_form';
import ThirdPartyLogin from 'app/components/third_party_login';
import DropDownHolder from 'app/utils/dropdownelert';
import TouchableText from 'app/components/touchable_text';
import Separator from 'app/components/separator';
import Button from 'app/components/button';

import {Colors, Screen} from 'app/constants';
import LoadingHolder from 'app/utils/loading';
import {RESPONSED_MSG} from 'app/constants/server';
import {getDeviceId} from 'app/utils/device';
import {verticalScale} from 'app/utils/screen';

class Login extends React.PureComponent {
  onRegister = () => {
    this.props.navigation.navigate('Register');
  };
  onForgotPassword = () => {
    this.props.navigation.navigate('ForgotPassword');
  };

  handleLogin = async () => {
    // check firstlauch
    try {
      const value = await AsyncStorage.getItem('@FirstLogin');
      if (!value) {
        await AsyncStorage.setItem('@FirstLogin', 'FirstLogin');
      }
    } catch (e) {
      console.log('checkFirstLauchApp-error', e);
    }
  };

  onLogin = async () => {
    const {screenProps, navigation} = this.props;
    const account = this.accountRef.state.value;
    const password = this.passwordRef.state.value;
    // check account and password field
    if (!account) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'accountRequired',
          defaultMessage: 'Vui lòng nhập email',
        }),
      );
    } else if (!password) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'passwordRequired',
          defaultMessage: 'Vui lòng nhập mật khẩu',
        }),
      );
    } else if (!validator.isEmail(account)) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'formatAccountFailed',
          defaultMessage: 'Tài khoản đã nhập khống đúng định dạng email',
        }),
      );
    } else if (password && String(password).length < 5) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'passwordRequiredLeast5Chars',
          defaultMessage: 'Mật khẩu phải có ít nhất 5 ký tự',
        }),
      );
    } else {
      // TODO: handle API
      const data = {
        email: String(account)
          .toLowerCase()
          .trim(),
        password: password,
        device_info: {
          device_id: getDeviceId(),
        },
      };
      // console.log('data', data);
      this.props.actions.login(data);
      this.handleLogin();
      // navigation.navigate('AppMain');
    }
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      const {intl} = this.props.screenProps;
      LoadingHolder.stop();
      //    Nếu đăng nhập có lỗi
      if (this.props.error) {
        // const messageError = RESPONSED_MSG[this.props.error];
        // DropDownHolder.alert(
        //   'error',
        //   intl.formatMessage({
        //     id: messageError,
        //     defaultMessage: 'Xảy ra lỗi, vui lòng thử lại sau.',
        //   }),
        // );
        DropDownHolder.alert('error', this.props.error);
      } else {
        // TODO: Đăng nhập thành công
        this.handleLogin();
        this.props.navigation.navigate('AppMain');
      }
    }
  }

  render() {
    const {screenProps, navigation} = this.props;
    const {intl} = screenProps;
    return (
      <TouchableWithoutFeedback
        style={styles.wrapContainer}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.contentContainer}>
          <View>
            <FormattedText
              id="login"
              defaultMessage="Đăng Nhập"
              style={styles.loginLabel}
            />
            <Separator height={verticalScale(60)} />
            <InputForm
              // defaultValue={'tranhadev95@gmail.com'}
              refs={ref => (this.accountRef = ref)}
              label={intl.formatMessage({
                id: 'form.email',
                defaultMessage: 'Email',
              })}
              placeholder={intl.formatMessage({
                id: 'form.email',
                defaultMessage: 'Email',
              })}
              keyboardType="email-address"
              textContentType="emailAddress"
              returnKeyType="next"
              onSubmitEditing={() => {
                this.passwordRef.focus();
              }}
            />
            <Separator height={24} />
            <InputForm
              // defaultValue={'123456'}
              refs={ref => (this.passwordRef = ref)}
              label={intl.formatMessage({
                id: 'form.password',
                defaultMessage: 'Mật khẩu',
              })}
              placeholder={intl.formatMessage({
                id: 'form.password',
                defaultMessage: 'Mật khẩu',
              })}
              isPasswordForm={true}
              returnKeyType="done"
              onSubmitEditing={this.onLogin}
            />
            <Separator height={16} />
            <TouchableText
              label={intl.formatMessage({
                id: 'forgotPassword',
                defaultMessage: 'Quên mật khẩu?',
              })}
              onPress={this.onForgotPassword}
              position={'flex-end'}
              style={styles.forgotPassword}
            />
            <Button
              label={intl.formatMessage({
                id: 'login',
                defaultMessage: 'Đăng Nhập',
              })}
              onPress={this.onLogin}
              containerStyle={styles.loginBtn}
            />
          </View>
          <Separator height={verticalScale(36)} />
          <ThirdPartyLogin
            intl={screenProps.intl}
            navigation={navigation}
            handleLogin={this.handleLogin}
          />
          <View style={styles.bottomText}>
            <FormattedText
              id="haventAnAccount"
              defaultMessage="Bạn chưa có tài khoản? "
              style={styles.haveAnAccount}
            />
            <TouchableText
              label={intl.formatMessage({
                id: 'registerNow',
                defaultMessage: 'Đăng ký ngay',
              })}
              onPress={this.onRegister}
              position={'flex-end'}
              style={styles.registerNow}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
export default Login;

const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentContainer: {
    flex: 1,
    paddingHorizontal: 24,
    // paddingTop: verticalScale(114),
    backgroundColor: Colors.BACKGROUND_PRIMARY,
    justifyContent: 'center',
    paddingBottom: Screen.PADDING_BOTTOM,
    // justifyContent: 'center',
  },
  loginLabel: {
    fontFamily: 'iCielHelveticaNowText-Medium',
    fontSize: 36,
    lineHeight: 53,
    alignSelf: 'center',
    fontWeight: '600',
    color: Colors.LIGHT,
    textTransform: 'capitalize',
  },
  formSeparator: {
    height: 16,
  },
  loginBtn: {
    marginTop: 43,
  },
  loginBtnText: {
    fontFamily: 'iCielHelveticaNowText-Medium',
    fontSize: 16,
    lineHeight: 24,
    color: Colors.PRIMARY_TEXT,
  },
  haveAnAccount: {
    fontSize: 16,
    color: Colors.PRIMARY_TEXT,
    marginRight: 5,
    lineHeight: 19,
  },
  forgotPassword: {
    textDecorationLine: 'underline',
    color: Colors.PRIMARY_TEXT,
    fontSize: 16,
    lineHeight: 19,
  },
  registerNow: {
    color: Colors.ACTIVE_TINT,
    fontSize: 16,
    textDecorationLine: 'underline',
    lineHeight: 19,
  },
  bottomText: {
    flexDirection: 'row',
    justifyContent: 'center',
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    left: 0,
    right: 0,
    textAlign: 'center',
  },
});

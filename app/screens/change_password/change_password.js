import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import InputForm from 'app/components/input_form';

import {Colors, Screen} from 'app/constants';
import Separator from 'app/components/separator';
import LoadingHolder from 'app/utils/loading';
import {RESPONSED_MSG} from 'app/constants/server';
import DropDownHolder from 'app/utils/dropdownelert';

import Button, {BUTTON_HEIGHT} from 'app/components/button';

class ChangePassword extends React.PureComponent<Props> {
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      const {intl} = this.props.screenProps;
      LoadingHolder.stop();
      //    Nếu đăng nhập có lỗi
      if (this.props.error) {
        DropDownHolder.alert('error', this.props.error);
      } else {
        DropDownHolder.alert(
          'success',
          intl.formatMessage({
            id: 'changePasswordSuccess',
            defaultMessage: 'Đổi mật khẩu thành công',
          }),
        );
        this.updatePassword = setTimeout(this.updatePasswordSuccess, 300);
      }
    }
  }

  componentWillUnmount() {
    if (this.updatePassword) {
      clearTimeout(this.updatePassword);
    }
  }

  updatePasswordSuccess = () => {
    this.props.navigation.goBack();
  };

  onChangePassword = () => {
    //TODO: check old password
    // check new password and confirm new password is equal or not
    // connect API
    const oldPassword = this.editOldPassword.state.value;
    const newPassword = this.editNewPassword.state.value;
    const confirmNewPassword = this.editConfirmNewPassword.state.value;

    const {screenProps, userId, actions} = this.props;
    if (!oldPassword) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'oldPasswordRequired',
          defaultMessage: 'Vui lòng nhâp mật khẩu hiện tại',
        }),
      );
    } else if (!newPassword) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'newPasswordRequired',
          defaultMessage: 'Vui lòng nhập mật khẩu mới',
        }),
      );
    } else if (!confirmNewPassword) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'confirmNewPasswordRequired',
          defaultMessage: 'Vui lòng nhập lại mật khẩu mới',
        }),
      );
    } else if (
      (oldPassword && String(oldPassword).length < 5) ||
      (newPassword && String(newPassword).length < 5) ||
      (confirmNewPassword && String(confirmNewPassword).length < 5)
    ) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'passwordRequiredLeast5Chars',
          defaultMessage: 'Mật khẩu phải có ít nhất 5 ký tự',
        }),
      );
    } else if (newPassword !== confirmNewPassword) {
      // Confirm password
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'confirmNewPasswordFailed',
          defaultMessage: 'Nhập lại mật khẩu mới không chính xác',
        }),
      );
    } else {
      const data = {
        _id: userId,
        old_password: oldPassword,
        new_password: newPassword,
      };
      // console.log('data', data);

      actions.updatePassword(data);
    }
  };
  render() {
    const {intl} = this.props.screenProps;
    return (
      <>
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={styles.contentContainer}
          keyboardShouldPersistTaps="handled">
          <View style={styles.content}>
            <InputForm
              refs={ref => (this.editOldPassword = ref)}
              placeholder={intl.formatMessage({
                id: 'form.oldPassword',
                defaultMessage: 'Mật khẩu cũ',
              })}
              blurOnSubmit={true}
              isPasswordForm={true}
              containerStyle={styles.formContainer}
            />
            <Separator height={32} />
            <Separator height={6} backgroundColor={Colors.PRIMARY_2} />
            <Separator height={24} />
            <InputForm
              refs={ref => (this.editNewPassword = ref)}
              placeholder={intl.formatMessage({
                id: 'form.newPassword',
                defaultMessage: 'Mật khẩu mới',
              })}
              blurOnSubmit={true}
              isPasswordForm={true}
              containerStyle={styles.formContainer}
            />
            <Separator height={24} />
            <InputForm
              refs={ref => (this.editConfirmNewPassword = ref)}
              placeholder={intl.formatMessage({
                id: 'form.confirmNewPassword',
                defaultMessage: 'Xác nhận mật khẩu mới',
              })}
              blurOnSubmit={true}
              isPasswordForm={true}
              containerStyle={styles.formContainer}
            />
          </View>
        </KeyboardAwareScrollView>
        <Button
          label={intl.formatMessage({
            id: 'title.save',
            defaultMessage: 'Lưu',
          })}
          onPress={this.onChangePassword}
          containerStyle={styles.button}
        />
      </>
    );
  }
}
export default ChangePassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },

  contentContainer: {
    paddingTop: 24,
    paddingBottom: Screen.PADDING_BOTTOM + BUTTON_HEIGHT + 16, // button
    // paddingHorizontal: 16,
  },
  formContainer: {
    paddingHorizontal: 24,
  },
  button: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    left: 24,
    right: 24,
  },
});

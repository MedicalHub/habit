import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {updatePassword} from 'app/actions/user';
import {getUserInfo} from 'app/selectors/user';

import ChangePassword from 'app/screens/change_password/change_password';

const mapStateToProps = state => {
  const user = getUserInfo(state);

  const isLoading = state.request.updatePassword.loading;
  const error = state.request.updatePassword.error;

  return {
    isLoading,
    error,
    userId: user._id,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        updatePassword: updatePassword,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ChangePassword);

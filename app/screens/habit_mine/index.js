import React from 'react';
import {View, StyleSheet, FlatList, RefreshControl} from 'react-native';
import {Colors} from 'app/constants';
import Separator from 'app/components/separator';
import ItemHabit from 'app/screens/habit_mine/item_habit';
import {getUserInfo} from 'app/selectors/user';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';
import {getAllCategory} from 'app/actions/category';

class HabitMine extends React.PureComponent<Props> {
  state = {
    dataTopic: [],
    isRefreshing: false,
  };

  componentDidMount() {
    let {data} = this.props; //data category
    if (!data) {
      return;
    }
    let topics = [];
    for (let i = 0; i < data.length; i++) {
      let _topics = data[i].topics; //mang topics
      if (!_topics) {
        return;
      }
      for (let j = 0; j < _topics.length; j++) {
        let topic = _topics[j];
        let {completed_count, item_count} = topic;
        if (completed_count < item_count && completed_count !== 0) {
          topics.push(topic);
        }
      }
    }
    this.setState({
      dataTopic: topics,
    });
  }

  onRefresh = () => {
    this.props.actions.getAllCategory(this.props.userId);
  };

  renderItem = ({item}) => {
    const {thumbnail, title, days, completed_count, item_count, _id} = item;
    // eslint-disable-next-line radix
    let process = parseInt((completed_count / item_count) * 100);
    return (
      <ItemHabit
        thumbnail={thumbnail}
        title={title}
        days={days}
        process={process}
        id={_id}
        navigation={this.props.navigation}
      />
    );
  };
  renderSeparator = () => <Separator height={16} />;
  keyExtractor = (item, index) => index.toString();

  render() {
    return (
      <View style={styles.container}>
        <FlatList
          data={this.state.dataTopic}
          renderItem={this.renderItem}
          keyExtractor={this.keyExtractor}
          contentContainerStyle={{padding: 16}}
          ItemSeparatorComponent={this.renderSeparator}
          refreshControl={
            <RefreshControl
              tintColor={Colors.LIGHT}
              refreshing={this.state.isRefreshing}
              onRefresh={this.onRefresh}
            />
          }
        />
      </View>
    );
  }
}

const mapStateToProps = state => {
  const isLoading = state.category.loading;
  const error = state.category.error;
  const data = state.category.data;
  const user = getUserInfo(state);

  return {
    isLoading,
    error,
    data,
    userId: user._id,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getAllCategory: getAllCategory,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(HabitMine);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentContainerStyle: {
    padding: 16,
  },
});

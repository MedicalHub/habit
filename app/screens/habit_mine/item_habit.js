import React, {Component} from 'react';
import {View, TouchableOpacity, StyleSheet, Text} from 'react-native';
import FastImage from 'react-native-fast-image';
import {AnimatedCircularProgress} from 'react-native-circular-progress';
import {Colors} from 'app/constants';
import {Circle} from 'react-native-svg';
import API from 'app/saga/api';

class ItemHabit extends Component {
  onDetailHabit = () => {
    const {title, id, thumbnail, navigation} = this.props;
    navigation.navigate('DetailHabit', {
      topicId: id,
      title: title,
      thumbnail: thumbnail,
    });
  };

  renderCap = ({center}) => (
    <Circle cx={center.x} cy={center.y} r="4" fill={'#FFBE00'} />
  );

  render() {
    const {thumbnail, title, days, process} = this.props;
    return (
      <TouchableOpacity style={styles.item} onPress={this.onDetailHabit}>
        <FastImage
          style={styles.image}
          source={{uri: API.urlImage(thumbnail)}}
          resizeMode={'cover'}>
          <View style={styles.block} />
          <View style={styles.content}>
            <Text style={styles.title}>{title}</Text>
            <Text style={styles.days}>{days}</Text>
          </View>
          <AnimatedCircularProgress
            size={58}
            width={4}
            fill={process}
            tintColor="#FFBE00"
            rotation={360}
            backgroundColor={Colors.PRIMARY_3}
            padding={10}
            renderCap={this.renderCap}>
            {fill => <Text style={styles.process}>{process}%</Text>}
          </AnimatedCircularProgress>
        </FastImage>
      </TouchableOpacity>
    );
  }
}
export default ItemHabit;
const styles = StyleSheet.create({
  item: {
    height: 110,
    borderRadius: 8,
    overflow: 'hidden',
  },
  block: {
    ...StyleSheet.absoluteFillObject,
    backgroundColor: 'rgba(15, 27, 61,0.4)',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    flexDirection: 'row',
    paddingHorizontal: 16,
  },
  content: {
    flex: 1,
  },
  title: {
    fontWeight: '500',
    fontSize: 18,
    lineHeight: 21,
    color: Colors.LIGHT,
  },
  days: {
    fontSize: 12,
    lineHeight: 14,
    color: '#FFBE00',
  },
  process: {
    fontSize: 12,
    fontWeight: 'bold',
    lineHeight: 14,
    color: '#FFC500',
  },
});

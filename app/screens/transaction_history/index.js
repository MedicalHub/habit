import React from 'react';
import {View, Text, StyleSheet, ScrollView} from 'react-native';
import {Colors, Screen} from 'app/constants';
import Item from 'app/screens/transaction_history/item';
import Separator from 'app/components/separator';

class TransactionHistory extends React.PureComponent<Props> {
  render() {
    return (
      <ScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}>
        <Item
          activeStatus={'Kích hoạt thành công'}
          activeTime={'18/02/2020 - 12/12/2020'}
        />
        <Separator height={24} />
        <Item
          activeStatus={'Kích hoạt thành công'}
          activeTime={'30/5/2020 - 23/12/2021'}
        />
        <Separator height={24} />
        <Item
          activeStatus={'Kích hoạt thành công'}
          activeTime={'30/5/2020 - 23/12/2021'}
        />
        <Separator height={24} />
        <Item
          activeStatus={'Kích hoạt thành công'}
          activeTime={'30/5/2020 - 23/12/2021'}
        />
      </ScrollView>
    );
  }
}
export default TransactionHistory;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentContainer: {
    padding: 16,
    paddingBottom: Screen.PADDING_BOTTOM,
  },
});

import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import PropTypes from 'prop-types';

import {Colors} from 'app/constants';
import Icon from 'app/components/icons';
import Separator from 'app/components/separator';
import AppIcon from 'app/components/app_icon';

class Item extends React.PureComponent<Props> {
  static propTypes = {
    activeStatus: PropTypes.string,
    activeTime: PropTypes.any,
  };
  render() {
    const {activeStatus, activeTime} = this.props;
    return (
      <View style={styles.container}>
        <View style={styles.iconContainer}>
          <AppIcon
            source={require('assets/images/check.png')}
            color={Colors.LIGHT}
            containerStyle={styles.iconCtn}
          />
        </View>
        <View style={styles.body}>
          <Text style={styles.activeStatus}>{activeStatus}</Text>
          <Separator height={4} />
          <Text style={styles.activeTime}>{activeTime}</Text>
        </View>
      </View>
    );
  }
}
export default Item;

const styles = StyleSheet.create({
  container: {
    height: 73,
    flexDirection: 'row',
    // paddingHorizontal: 16,
    // alignItems: 'center',
    borderBottomWidth: 1,
    borderBottomColor: Colors.BORDER_PRIMARY,
  },
  iconContainer: {
    height: 48,
    width: 48,
    borderRadius: 8,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: Colors.PRIMARY_2,
  },
  iconCtn: {
    backgroundColor: Colors.SUCCESS_PRIMARY,
    borderRadius: 12,
    height: 24,
    width: 24,
    padding: 2,
  },
  body: {
    height: '100%',
    flex: 1,
    // justifyContent: 'center',
    marginLeft: 24,
    paddingRight: 16,
  },
  activeStatus: {
    fontSize: 16,
    fontFamily: 'iCielHelveticaNowText-Medium',
    color: Colors.SUCCESS_PRIMARY,
  },
  activeTime: {
    fontSize: 12,
    fontFamily: 'iCielHelveticaNowText-Regular',
    color: Colors.TIME_ACTIVE_ACCOUNT,
  },
});

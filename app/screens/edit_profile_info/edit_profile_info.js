import React from 'react';
import {StyleSheet, View, Keyboard} from 'react-native';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import validator from 'validator';

import InputForm from 'app/components/input_form';
import Separator from 'app/components/separator';

import {Colors, Screen, General} from 'app/constants';
import Button, {BUTTON_HEIGHT} from 'app/components/button';
import DropDownHolder from 'app/utils/dropdownelert';
import LoadingHolder from 'app/utils/loading';
import {formatPhoneNumber} from 'app/utils/phone';

class EditProfileInfo extends React.PureComponent<Props> {
  validate = () => {
    const {screenProps} = this.props;
    const nickname = this.editInfoNicknameRef.state.value;
    const phone = this.editInfoPhoneRef.state.value;
    // check nickname and password field
    if (!nickname) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'nicknameRequired',
          defaultMessage: 'Vui lòng nhập tên',
        }),
      );
      return false;
    } else if (!phone) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'phoneRequired',
          defaultMessage: 'Vui lòng nhập số điện thoại',
        }),
      );
      return false;
    } else if (!validator.isMobilePhone(phone)) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'formatPhoneRequired',
          defaultMessage: 'Vui lòng đúng định dạng số điện thoại',
        }),
      );
      return false;
    }
    return true;
  };

  onSave = () => {
    if (this.validate()) {
      // handle update user info
      const {actions, userId} = this.props;
      const nickname = this.editInfoNicknameRef.state.value;
      const phone = this.editInfoPhoneRef.state.value;

      const data = {
        _id: userId,
        fullname: nickname,
        phone: phone,
      };
      // console.log('data', data);

      actions.updateProfile(data);
    }
    return;
  };

  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      LoadingHolder.stop();
      if (this.props.error) {
        DropDownHolder.alert('error', this.props.error);
      } else {
        this.props.navigation.goBack();
      }
    }
  }

  render() {
    const {screenProps, fullname, phone} = this.props;
    const {intl} = screenProps;

    let phoneNumber = '';
    if (phone) {
      phoneNumber = formatPhoneNumber(String(phone));
    }

    return (
      <View style={styles.wrapContainer}>
        <KeyboardAwareScrollView
          style={styles.container}
          contentContainerStyle={styles.content}
          keyboardDismissMode={'on-drag'}
          keyboardShouldPersistTaps="handled">
          <InputForm
            defaultValue={fullname || General.DEFAULT_NAME}
            refs={ref => (this.editInfoNicknameRef = ref)}
            placeholder={intl.formatMessage({
              id: 'nickname',
              defaultMessage: 'Tên hiển thị',
            })}
            returnKeyType="next"
            onSubmitEditing={() => {
              this.editInfoPhoneRef.focus();
            }}
          />
          <Separator height={24} />
          <InputForm
            defaultValue={phoneNumber}
            refs={ref => (this.editInfoPhoneRef = ref)}
            placeholder={intl.formatMessage({
              id: 'sdt',
              defaultMessage: 'Số điện thoại',
            })}
            keyboardType="number-pad"
            returnKeyType="done"
            onSubmitEditing={() => {
              Keyboard.dismiss();
            }}
          />
        </KeyboardAwareScrollView>
        <Button
          label={intl.formatMessage({
            id: 'title.save',
            defaultMessage: 'Lưu',
          })}
          onPress={this.onSave}
          containerStyle={styles.button}
        />
      </View>
    );
  }
}
export default EditProfileInfo;

const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  content: {
    paddingHorizontal: 24,
    paddingTop: 30,
    paddingBottom: Screen.PADDING_BOTTOM + BUTTON_HEIGHT + 16, // button
  },
  button: {
    position: 'absolute',
    bottom: Screen.PADDING_BOTTOM,
    left: 24,
    right: 24,
  },
});

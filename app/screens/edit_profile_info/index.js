import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import EditProfileInfo from './edit_profile_info';

import {updateProfile} from 'app/actions/user';
import {getUserInfo} from 'app/selectors/user';

const mapStateToProps = state => {
  const user = getUserInfo(state);

  const isLoading = state.request.updateProfile.loading;
  const error = state.request.updateProfile.error;

  return {
    isLoading,
    error,
    fullname: user.fullname,
    phone: user.phone,
    userId: user._id,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        updateProfile: updateProfile,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(EditProfileInfo);

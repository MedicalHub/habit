import React from 'react';
import {View, Text, StyleSheet, TouchableOpacity} from 'react-native';

import PropTypes from 'prop-types';

import API from 'app/saga/api';
import FastImage from 'react-native-fast-image';
import Separator from 'app/components/separator';
import AppIcon from 'app/components/app_icon';
import {Colors} from 'app/constants';
import FormattedText from 'app/components/formatted_text';

class MedalItem extends React.PureComponent<Props> {
  onDetailHabit = () => {
    const {title, id, imageSource} = this.props;

    this.props.navigation.navigate('DetailHabit', {
      topicId: id,
      title: title,
      thumbnail: imageSource,
    });
  };

  render() {
    const {
      imageSource,
      title,
      completedDay,
      totalDay,
      isCompleted,
    } = this.props;

    let Bottom = (
      <Text style={styles.description} numberOfLines={2}>
        <Text
          style={{
            color: isCompleted ? Colors.SUCCESS_PRIMARY : Colors.NOT_COMPLETED,
          }}>
          {completedDay + '/' + totalDay}{' '}
        </Text>
        <FormattedText id="date" defaultMessage="ngày" />
      </Text>
    );

    let imageCup = require('assets/images/cup_inactive.png');

    if (isCompleted) {
      Bottom = (
        <Text style={styles.description} numberOfLines={2}>
          <FormattedText
            style={{
              color: isCompleted
                ? Colors.SUCCESS_PRIMARY
                : Colors.NOT_COMPLETED,
            }}
            id="completed"
            defaultMessage="Đã hoàn thành"
          />
        </Text>
      );
      imageCup = require('assets/images/cup.png');
    }
    return (
      <TouchableOpacity style={styles.container} onPress={this.onDetailHabit}>
        <FastImage
          source={{uri: API.urlImage(imageSource)}}
          resizeMode="cover"
          style={styles.image}
        />
        <View style={styles.body}>
          <Text style={styles.title} numberOfLines={1}>
            {title}
          </Text>
          <Separator height={8} />
          {Bottom}
        </View>
        <AppIcon
          color={null}
          source={imageCup}
          containerStyle={styles.cupStyle}
        />
      </TouchableOpacity>
    );
  }
}
export default MedalItem;

MedalItem.propTypes = {
  imageSource: PropTypes.any,
  title: PropTypes.string,
  completedDay: PropTypes.string,
};

MedalItem.defaultProps = {
  imageSource: require('assets/images/medal-item-1.png'),
  title: 'Thói quen đọc sách',
  completedDay: '12/30',
};

const styles = StyleSheet.create({
  container: {
    flexDirection: 'row',
    backgroundColor: Colors.PRIMARY_2,
    height: 95,
    borderRadius: 8,
    alignItems: 'center',
  },
  image: {
    height: '100%',
    width: 80,
    borderRadius: 8,
  },
  body: {
    height: '100%',
    flex: 1,
    paddingHorizontal: 16,
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    color: Colors.LIGHT,
    fontWeight: '500',
  },
  description: {
    fontSize: 12,
    color: Colors.PRIMARY_TEXT,
  },
  cupStyle: {
    height: 48,
    width: 48,
    marginRight: 12,
  },
});

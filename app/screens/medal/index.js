import React, {useEffect, useState} from 'react';
import {View, Text, StyleSheet, FlatList, RefreshControl} from 'react-native';
import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import {getAllCategory} from 'app/actions/category';
import {Colors} from 'app/constants';
import HeaderRight from 'app/components/header_right';
import MedalItem from 'app/screens/medal/medal_item';
import Separator from 'app/components/separator';
import {getUserInfo} from 'app/selectors/user';

// const dataFake = [
//   {
//     id: '1',
//     title: 'Thói quen đọc sáng',
//     completedDay: '30/30',
//     imageSource: require('assets/images/medal-item-1.png'),
//     isCompleted: true,
//   },
//   {
//     id: '2',
//     title: 'Thói quen luôn luôn đặt mục tiêu',
//     completedDay: '12/30',
//     imageSource: require('assets/images/medal-item-2.png'),
//   },
//   {
//     id: '3',
//     title: 'Thói quen quản lý thời gian',
//     completedDay: '12/30',
//     imageSource: require('assets/images/medal-item-3.png'),
//   },
// ];

const Medal = React.memo(props => {
  const {data, navigation, actions, userId} = props;

  // for (let i = 0; i < data.length; i++) {
  //   let _topics = data[i].topics; //mang topics
  //   if (!_topics) {
  //     return;
  //   }
  //   topics = [...topics, ..._topics];
  // }

  const [dataTopic, setDataTopic] = useState([]);
  const [refreshing, setRefreshing] = useState(true);

  useEffect(() => {
    let topics = [];

    for (let i = 0; i < data.length; i++) {
      let _topics = data[i].topics; //mang topics
      if (!_topics) {
        return;
      }
      topics = [...topics, ..._topics];
    }

    setDataTopic(topics.filter(e => e.completed_count > 0));
    setRefreshing(false);
    console.warn(topics);
  }, [data]);

  const onRefresh = () => {
    setRefreshing(true);
    actions.getAllCategoryProps(userId);
  };

  const renderItem = ({item}) => {
    return (
      <MedalItem
        id={item._id}
        title={item.title}
        completedDay={item.completed_count}
        totalDay={item.item_count}
        imageSource={item.thumbnail}
        isCompleted={item.completed_count - item.item_count === 0}
        navigation={navigation}
      />
    );
  };

  return (
    <View style={styles.container}>
      <FlatList
        data={dataTopic}
        extraData={dataTopic}
        keyExtractor={item => item._id.toString()}
        renderItem={renderItem}
        contentContainerStyle={styles.contentContainer}
        refreshControl={
          <RefreshControl
            tintColor={Colors.LIGHT}
            colors={[Colors.LIGHT]}
            refreshing={refreshing}
            onRefresh={onRefresh}
          />
        }
        ItemSeparatorComponent={() => <Separator height={16} />}
      />
    </View>
  );
});

Medal.navigationOptions = ({navigation, screenProps}) => {
  const headerRight = () => (
    <HeaderRight
      onPress={() => {}}
      color={Colors.PRIMARY_TEXT}
      source={require('assets/images/share.png')}
    />
  );
  return {
    headerRight,
  };
};

const mapStateToProps = state => {
  const data = state.category.data;
  const user = getUserInfo(state);

  return {
    data,
    userId: user._id,
  };
};

const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getAllCategoryProps: getAllCategory,
      },
      dispatch,
    ),
  };
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Medal);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },
  contentContainer: {
    paddingHorizontal: 16,
    paddingVertical: 16,
  },
});

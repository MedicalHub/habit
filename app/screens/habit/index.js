import React, {Component} from 'react';
import {View, StyleSheet} from 'react-native';
import HeaderRight from 'app/components/header_right';
import {Colors} from 'app/constants';
import HabitTopTab from 'app/navigation/habit_top_tab';

class Habit extends Component {
  // static navigationOptions = ({navigation, screenProps}) => {
  //   const headerRight = () => (
  //     <HeaderRight
  //       onPress={() => {
  //         if (navigation.state.params && navigation.state.params.onShare) {
  //           navigation.state.params.onShare();
  //         }
  //       }}
  //       color={Colors.PRIMARY_TEXT}
  //       source={require('assets/images/search.png')}
  //     />
  //   );
  //   return {
  //     headerRight,
  //   };
  // };

  render() {
    return (
      <View style={styles.container}>
        <HabitTopTab />
      </View>
    );
  }
}

export default Habit;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },
});

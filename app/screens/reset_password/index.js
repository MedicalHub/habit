import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import ResetPassword from 'app/screens/reset_password/reset_password';
import {resetPassword} from 'app/actions/user';
import {getUserInfo} from 'app/selectors/user';

const mapStateToProps = state => {
  const isLoading = state.request.resetPassword.loading;
  const error = state.request.resetPassword.error;

  const user = getUserInfo(state);
  const userId = user._id;

  return {
    isLoading,
    error,
    userId,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        resetPassword,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(ResetPassword);

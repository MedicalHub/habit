import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import TouchableText from 'app/components/touchable_text';

import {Colors} from 'app/constants';
import LoadingHolder from 'app/utils/loading';
import {RESPONSED_MSG} from 'app/constants/server';
import DropDownHolder from 'app/utils/dropdownelert';
import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';
import InputForm from 'app/components/input_form';
import Separator from 'app/components/separator';

class ResetPassword extends React.PureComponent<Props> {
  static navigationOptions = ({navigation, screenProps}) => {
    const headerRight = () => (
      <TouchableText
        label={screenProps.intl.formatMessage({
          id: 'title.save',
          defaultMessage: 'Lưu',
        })}
        onPress={() => {
          if (
            navigation.state.params &&
            navigation.state.params.onChangePassword
          ) {
            navigation.state.params.onChangePassword();
          }
        }}
        style={{marginRight: 16, color: Colors.LIGHT}}
      />
    );
    return {
      headerRight,
    };
  };

  constructor(props) {
    super(props);
    this.resetNewPassword = null;
    this.confirmNewPassword = null;
  }
  componentDidMount() {
    this.props.navigation.setParams({
      onChangePassword: this.onChangePassword,
    });
  }
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      const {intl} = this.props.screenProps;
      LoadingHolder.stop();
      //    Nếu đăng nhập có lỗi
      if (this.props.error) {
        const messageError =
          RESPONSED_MSG[this.props.error] || RESPONSED_MSG.UNDEFINED_ERROR;

        const message = intl.formatMessage({
          id: messageError,
          defaultMessage: 'Xảy ra lỗi, vui lòng thử lại sau.',
        });

        DropDownHolder.alert('error', message);
      } else {
        DropDownHolder.alert(
          'success',
          intl.formatMessage({
            id: 'changePasswordSuccess',
            defaultMessage: 'Đổi mật khẩu thành công',
          }),
        );
        this.updatePassword = setTimeout(this.resetPasswordSuccess, 200);
      }
    }
  }

  componentWillUnmount() {
    if (this.updatePassword) {
      clearTimeout(this.updatePassword);
    }
  }

  resetPasswordSuccess = () => {
    const {navigation} = this.props;
    navigation.navigate('AppMain');
  };

  onChangePassword = () => {
    // check new password and confirm new password is equal or not
    // connect API
    const newPassword = this.resetNewPassword.state.value;
    const confirmNewPassword = this.resetConfirmNewPassword.state.value;

    const {screenProps, actions, navigation} = this.props;
    if (!newPassword) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'newPasswordRequired',
          defaultMessage: 'Vui lòng nhập mật khẩu mới',
        }),
      );
    } else if (!confirmNewPassword) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'confirmNewPasswordRequired',
          defaultMessage: 'Vui lòng nhập lại mật khẩu mới',
        }),
      );
    } else if (
      (newPassword && String(newPassword).length < 5) ||
      (confirmNewPassword && String(confirmNewPassword).length < 5)
    ) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'passwordRequiredLeast5Chars',
          defaultMessage: 'Mật khẩu phải có ít nhất 5 ký tự',
        }),
      );
    } else if (newPassword !== confirmNewPassword) {
      // Confirm password
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'confirmNewPasswordFailed',
          defaultMessage: 'Nhập lại mật khẩu mới không chính xác',
        }),
      );
    } else {
      const data = {
        new_password: newPassword,
        token: navigation.getParam('token'),
      };
      // console.log('data', data);

      actions.resetPassword(data);
    }
  };

  onReturnLoginScreen = () => {
    this.props.navigation.popToTop('Login');
  };

  render() {
    const {intl} = this.props.screenProps;
    return (
      <KeyboardAwareScrollView
        style={styles.container}
        contentContainerStyle={styles.contentContainer}
        keyboardShouldPersistTaps="handled">
        <View style={styles.content}>
          <InputForm
            refs={ref => (this.resetNewPassword = ref)}
            label={intl.formatMessage({
              id: 'form.newPassword',
              defaultMessage: 'Mật khẩu mới',
            })}
            placeholder={intl.formatMessage({
              id: 'form.newPassword',
              defaultMessage: 'Nhập mật khẩu mới',
            })}
            blurOnSubmit={false}
            isPasswordForm={true}
            onSubmitEditing={() => {
              this.resetConfirmNewPassword.focus();
            }}
            returnKeyType="next"
            autoFocus={true}
            textContentType={'newPassword'}
          />
          <Separator height={16} />
          <InputForm
            refs={ref => (this.resetConfirmNewPassword = ref)}
            label={intl.formatMessage({
              id: 'form.confirmNewPassword',
              defaultMessage: 'Mật khẩu mới',
            })}
            placeholder={intl.formatMessage({
              id: 'form.confirmNewPassword',
              defaultMessage: 'Nhập lại mật khẩu mới',
            })}
            blurOnSubmit={true}
            isPasswordForm={true}
            returnKeyType="done"
            textContentType={'newPassword'}
            onSubmitEditing={() => {
              this.onChangePassword();
            }}
          />
          <TouchableText
            style={styles.returnLoginScreen}
            onPress={this.onReturnLoginScreen}
            label={intl.formatMessage({
              id: 'returnLoginScreen',
              defaultMessage: 'Quay về trang Đăng Nhập',
            })}
            position={'center'}
          />
        </View>
      </KeyboardAwareScrollView>
    );
  }
}
export default ResetPassword;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  contentContainer: {
    paddingTop: 24,
    paddingHorizontal: 16,
  },
  returnLoginScreen: {
    fontFamily: 'iCielHelveticaNowText-Medium',
    fontSize: 14,
    textDecorationLine: 'underline',
    textAlign: 'center',
    color: Colors.PRIMARY,
    marginTop: 32,
  },
});

import React, {PureComponent} from 'react';
import {
  StyleSheet,
  View,
  Text,
  ImageBackground,
  TouchableOpacity,
} from 'react-native';
import {Colors} from 'app/constants';
import PropTypes from 'prop-types';
import API from 'app/saga/api';
class DetailCategoryItem extends PureComponent {
  onDetailHabit = () => {
    const {title, id, thumbnail} = this.props;
    this.props.navigation.navigate('DetailHabit', {
      topicId: id,
      title: title,
      thumbnail: thumbnail,
    });
  };

  render() {
    const {title, thumbnail} = this.props;
    return (
      <TouchableOpacity onPress={this.onDetailHabit}>
        <ImageBackground
          source={{uri: API.urlImage(thumbnail)}}
          resizeMode="cover"
          style={styles.container}>
          <View style={styles.rectangle}>
            <Text style={styles.title}>{title}</Text>
          </View>
        </ImageBackground>
      </TouchableOpacity>
    );
  }
}

export default DetailCategoryItem;

DetailCategoryItem.propTypes = {
  title: PropTypes.string,
};

DetailCategoryItem.defaultProps = {
  title: 'Học tập',
};

const styles = StyleSheet.create({
  container: {
    height: 100,
    borderRadius: 8,
    overflow: 'hidden',
  },
  image: {
    flex: 1,
    justifyContent: 'center',
  },
  title: {
    fontSize: 16,
    color: Colors.LIGHT,
    fontWeight: '500',
    marginHorizontal: 16,
  },
  rectangle: {
    backgroundColor: 'rgba(15, 27, 61,0.35)',
    flex: 1,
    justifyContent: 'center',
  },
});

import React from 'react';
import {
  View,
  StyleSheet,
  FlatList,
  RefreshControl,
  ScrollView,
  Text,
  Image,
} from 'react-native';
import {Colors} from 'app/constants';
import DetailCategoryItem from 'app/screens/detail_category/detail_category_item';
import Separator from 'app/components/separator';
import {bindActionCreators} from 'redux';
import {connect} from 'react-redux';
import {getTopicByCategory, resetTopic} from 'app/actions/topic';
import LoadingView from 'app/components/loading_view';

class DetailCategory extends React.PureComponent {
  static navigationOptions = ({navigation}) => {
    let title =
      (navigation.state.params && navigation.state.params.title) || 'Học tập';
    return {
      title: title,
    };
  };

  constructor(props) {
    super(props);
    this.state = {
      isRefreshing: false,
    };
  }

  componentDidMount() {
    const {categoryId, title} = this.props.navigation.state.params;
    this.props.navigation.setParams({
      title: title,
    });
    this.props.actions.getTopicByCategory(categoryId);
  }

  componentWillUnmount() {
    this.props.actions.resetTopic();
  }

  onRefresh = () => {
    const {categoryId} = this.props.navigation.state.params;
    this.props.actions.getTopicByCategory(categoryId);
  };
  renderItem = ({item}) => {
    return (
      <DetailCategoryItem
        thumbnail={item.thumbnail}
        title={item.title}
        navigation={this.props.navigation}
        id={item._id}
      />
    );
  };

  renderEmpty = () => {
    return (
      <View style={styles.emptyView}>
        <Image
          source={require('assets/images/empty.png')}
          style={styles.imgEmpty}
        />
      </View>
    );
  };

  keyExtractor = (item, index) => index.toString();

  renderSeparator = () => <Separator height={16} />;

  render() {
    const {isLoading, error, data} = this.props;

    const {isRefreshing} = this.state;
    return (
      <View style={styles.container}>
        {isLoading && <LoadingView />}
        {data && (
          <FlatList
            data={data}
            keyExtractor={this.keyExtractor}
            renderItem={this.renderItem}
            ItemSeparatorComponent={this.renderSeparator}
            style={styles.containerFlatList}
            contentContainerStyle={{flex: 1}}
            showsVerticalScrollIndicator={false}
            refreshControl={
              <RefreshControl
                tintColor={Colors.LIGHT}
                refreshing={isRefreshing}
                onRefresh={this.onRefresh}
              />
            }
            ListEmptyComponent={this.renderEmpty}
          />
        )}
      </View>
    );
  }
}

const mapStateToProps = state => {
  const isLoading = state.topic.loading;
  const error = state.topic.error;
  const data = state.topic.data;
  return {
    isLoading,
    error,
    data,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        getTopicByCategory: getTopicByCategory,
        resetTopic: resetTopic,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(DetailCategory);

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: Colors.PRIMARY_1,
  },
  containerFlatList: {
    marginHorizontal: 16,
    paddingTop: 16,
  },

  emptyView: {
    alignItems: 'center',
    justifyContent: 'center',
    flex: 1,
  },
  imgEmpty: {
    width: 300,
    height: 150,
    resizeMode: 'cover',
  },
});

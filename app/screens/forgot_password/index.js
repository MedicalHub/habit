import React from 'react';
import {
  View,
  TouchableWithoutFeedback,
  StyleSheet,
  Keyboard,
} from 'react-native';

import validator from 'validator';

import FormattedText from 'app/components/formatted_text';
import InputForm from 'app/components/input_form';
import Button from 'app/components/button';

import DropDownHolder from 'app/utils/dropdownelert';
import {Colors} from 'app/constants';
import TouchableText from 'app/components/touchable_text';
import api from 'app/saga/api';
import {RESPONSED_MSG} from 'app/constants/server';
import LoadingHolder from 'app/utils/loading';

class ForgotPassword extends React.PureComponent<Props> {
  onContinue = () => {
    const {screenProps, navigation} = this.props;
    // check error
    const email = this.forgotPasswordAcccountRef.state.value;
    if (!email) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'accountRequired',
          defaultMessage: 'Vui lòng nhập email',
        }),
      );
    } else if (!validator.isEmail(email)) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'formatAccountFailed',
          defaultMessage: 'Vui lòng nhập đúng định dạng email',
        }),
      );
    } else {
      // alert('comming soon');
      this.requestForgotPassword(email);
      // navigation.navigate('VerifyOtp', {
      //   email,
      // });
    }
  };

  requestForgotPassword = async email => {
    try {
      LoadingHolder.start();
      const data = {
        email: String(email)
          .toLowerCase()
          .trim(),
      };
      const result = await api.callApi('user/forgot-password', 'POST', data);
      // console.log('result', result);
      if (result.token && result.otp) {
        this.props.navigation.navigate('VerifyOtp', {
          email,
          otp: result.otp,
          token: result.token,
        });
      } else if (result.error) {
        const {intl} = this.props.screenProps;
        const messageError =
          RESPONSED_MSG[result.errors[0]] || RESPONSED_MSG.UNDEFINED_ERROR;
        const message = intl.formatMessage({
          id: messageError,
          defaultMessage: 'Xảy ra lỗi, vui lòng thử lại sau.',
        });
        DropDownHolder.alert('error', message);
      }
    } catch (error) {
      // handle error
      console.log('error-requestForgotPassword', error);
    } finally {
      LoadingHolder.stop();
    }
  };

  onReturnLoginScreen = () => {
    this.props.navigation.navigate('Login');
  };
  render() {
    const {intl} = this.props.screenProps;
    return (
      <TouchableWithoutFeedback
        style={styles.wrapContainer}
        onPress={() => {
          Keyboard.dismiss();
        }}>
        <View style={styles.container}>
          <FormattedText
            id={'forgotPasswordTitle'}
            defaultMessage={
              'Vui lòng nhập email mà bạn\nđã dùng đăng ký tài khoản'
            }
            style={styles.forgotPasswordTitle}
          />
          <View style={styles.contentContainer}>
            <InputForm
              // defaultValue={'tranhadev95@gmail.com'}
              label={intl.formatMessage({
                id: 'form.account',
                defaultMessage: 'Tài khoản',
              })}
              placeholder={intl.formatMessage({
                id: 'form.account.placeholder',
                defaultMessage: 'Nhập email',
              })}
              keyboardType="email-address"
              textContentType="emailAddress"
              returnKeyType="next"
              refs={ref => (this.forgotPasswordAcccountRef = ref)}
              // blurOnSubmit={true}
            />
            <Button
              label={intl.formatMessage({
                id: 'title.continue',
                defaultMessage: 'Tiếp tục',
              })}
              onPress={this.onContinue}
              containerStyle={styles.continueBtn}
            />
            <TouchableText
              style={styles.returnLoginScreen}
              onPress={this.onReturnLoginScreen}
              label={intl.formatMessage({
                id: 'returnLoginScreen',
                defaultMessage: 'Quay về trang Đăng Nhập',
              })}
              position={'center'}
            />
          </View>
        </View>
      </TouchableWithoutFeedback>
    );
  }
}
export default ForgotPassword;

const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
  },
  container: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
    paddingTop: 24,
  },
  forgotPasswordTitle: {
    textAlign: 'center',
    fontSize: 16,
    color: Colors.LIGHT,
    marginBottom: 32,
  },
  contentContainer: {
    paddingHorizontal: 24,
  },
  continueBtn: {
    marginTop: 24,
    marginBottom: 32,
  },
  returnLoginScreen: {
    fontFamily: 'iCielHelveticaNowText-Medium',
    fontSize: 14,
    textDecorationLine: 'underline',
    textAlign: 'center',
    color: Colors.PRIMARY,
  },
});

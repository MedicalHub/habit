import React from 'react';
import {View, Text, StyleSheet} from 'react-native';

import validator from 'validator';
import AsyncStorage from '@react-native-community/async-storage';

import {KeyboardAwareScrollView} from 'react-native-keyboard-aware-scroll-view';

import FormattedText from 'app/components/formatted_text';
import InputForm from 'app/components/input_form';
import ThirdPartyLogin from 'app/components/third_party_login';
import {Colors, Screen} from 'app/constants';
import DropDownHolder from 'app/utils/dropdownelert';

import TouchableText from 'app/components/touchable_text';
import Button from 'app/components/button';
import LoadingHolder from 'app/utils/loading';
import {RESPONSED_MSG} from 'app/constants/server';
import {getDeviceId} from 'app/utils/device';
import Separator from 'app/components/separator';
import {verticalScale} from 'app/utils/screen';

class Register extends React.PureComponent {
  handleLogin = async () => {
    // check firstlauch
    try {
      const value = await AsyncStorage.getItem('@FirstLogin');
      if (!value) {
        await AsyncStorage.setItem('@FirstLogin', 'FirstLogin');
      }
    } catch (e) {
      console.log('checkFirstLauchApp-error', e);
    }
  };

  onRegister = () => {
    const {screenProps, actions, navigation} = this.props;
    const account = this.registerAcountRef.state.value;
    const password = this.registerPasswordRef.state.value;
    const confirmPassword = this.confirmRegisterPasswordRef.state.value;

    // check required field
    if (!account) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'accountRequired',
          defaultMessage: 'Vui lòng nhập email',
        }),
      );
    } else if (!validator.isEmail(account)) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'formatAccountFailed',
          defaultMessage: 'Vui lòng nhập đúng định dạng email',
        }),
      );
    } else if (!password) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'passwordRequired',
          defaultMessage: 'Vui lòng nhập mật khẩu',
        }),
      );
    } else if (!confirmPassword) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'confirmPasswordRequired',
          defaultMessage: 'Vui lòng nhập lại mật khẩu',
        }),
      );
    } else if (password && String(password).length < 5) {
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'passwordRequiredLeast5Chars',
          defaultMessage: 'Mật khẩu phải có ít nhất 5 ký tự',
        }),
      );
    } else if (password !== confirmPassword) {
      // Confirm password
      DropDownHolder.alert(
        'error',
        screenProps.intl.formatMessage({
          id: 'confirmPasswordFailed',
          defaultMessage: 'Nhập lại mật khẩu không chính xác',
        }),
      );
    } else {
      // handle API
      const data = {
        email: String(account)
          .toLowerCase()
          .trim(),
        password: password,
        device_info: {
          device_id: getDeviceId(),
        },
      };
      this.props.actions.register(data);
    }
  };
  onLogin = () => {
    this.props.navigation.navigate('Login');
  };
  componentDidUpdate(prevProps, prevState) {
    if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === true
    ) {
      LoadingHolder.start();
    } else if (
      this.props.isLoading !== prevProps.isLoading &&
      this.props.isLoading === false
    ) {
      LoadingHolder.stop();
      //    Nếu đăng nhập có lỗi
      if (this.props.error) {
        DropDownHolder.alert('error', this.props.error);
      } else {
        this.handleLogin();
        this.props.navigation.navigate('AppMain');
      }
    }
  }

  onTermOfUse = () => {
    this.props.navigation.navigate('TermOfUse');
  };

  onPrivacyPolicy = () => {
    this.props.navigation.navigate('PrivacyPolicy');
  };
  render() {
    const {intl} = this.props.screenProps;
    return (
      <View style={styles.wrapContainer}>
        <KeyboardAwareScrollView
          contentContainerStyle={styles.container}
          //showsVerticalScrollIndicator={'false'}
          keyboardShouldPersistTaps="handled">
          <View style={styles.contentContainer}>
            <View>
              <FormattedText
                id="title.register"
                defaultMessage="Đăng ký"
                style={styles.registerLabel}
              />
              <Separator height={verticalScale(30)} />

              <InputForm
                // defaultValue={'Test1@gmail.com'}
                label={intl.formatMessage({
                  id: 'form.inputEmail',
                  defaultMessage: 'Nhập địa chỉ email',
                })}
                placeholder={intl.formatMessage({
                  id: 'form.inputEmail',
                  defaultMessage: 'Nhập địa chỉ email',
                })}
                keyboardType="email-address"
                textContentType="emailAddress"
                returnKeyType="next"
                refs={ref => (this.registerAcountRef = ref)}
                onSubmitEditing={() => {
                  this.registerPasswordRef.focus();
                }}
              />
              <Separator height={verticalScale(26)} />
              <InputForm
                // defaultValue={'12345'}
                refs={ref => (this.registerPasswordRef = ref)}
                label={intl.formatMessage({
                  id: 'form.password',
                  defaultMessage: 'Mật khẩu',
                })}
                placeholder={intl.formatMessage({
                  id: 'form.password',
                  defaultMessage: 'Mật khẩu',
                })}
                isPasswordForm={true}
                onSubmitEditing={() => {
                  this.confirmRegisterPasswordRef.focus();
                }}
                returnKeyType="next"
              />
              <Separator height={verticalScale(26)} />
              <InputForm
                // defaultValue={'12345'}
                refs={ref => (this.confirmRegisterPasswordRef = ref)}
                label={intl.formatMessage({
                  id: 'form.confirmPassword',
                  defaultMessage: 'Xác nhận mật khẩu',
                })}
                placeholder={intl.formatMessage({
                  id: 'form.confirmPassword',
                  defaultMessage: 'Xác nhận mật khẩu',
                })}
                isPasswordForm={true}
                returnKeyType="done"
                onSubmitEditing={() => {
                  this.onRegister();
                }}
              />
              <Button
                label={intl.formatMessage({
                  id: 'title.continue',
                  defaultMessage: 'Tiếp Tục',
                })}
                onPress={this.onRegister}
                containerStyle={styles.continueBtn}
              />
            </View>
            <Separator height={verticalScale(24)} />
            <ThirdPartyLogin />
          </View>
          <View>
            <Text style={styles.confirmTermAndPolicy}>
              <Text>{'Tiếp tục có nghĩa bạn đã đồng ý với'}</Text>
              <Text onPress={this.onTermOfUse} style={styles.termAndPolicy}>
                {' điều khoản sử dụng'}
              </Text>
              <Text>{' và '}</Text>
              <Text onPress={this.onPrivacyPolicy} style={styles.termAndPolicy}>
                {'chính sách bảo mật'}
              </Text>
              <Text>{' của Kinh Dịch.'}</Text>
            </Text>
            <View style={styles.bottomText}>
              <FormattedText
                id="title.haveAnAccount"
                defaultMessage="Bạn đã có tài khoản? "
                style={styles.haveAnAccount}
              />
              <TouchableText
                label={intl.formatMessage({
                  id: 'title.loginNow',
                  defaultMessage: 'Đăng nhập ngay',
                })}
                onPress={this.onLogin}
                position={'flex-end'}
                style={styles.loginNow}
              />
            </View>
          </View>
        </KeyboardAwareScrollView>
      </View>
    );
  }
}
export default Register;

const styles = StyleSheet.create({
  wrapContainer: {
    flex: 1,
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  },
  container: {
    flex: 1,
    paddingBottom: Screen.PADDING_BOTTOM,
  },
  contentContainer: {
    flex: 1,
    paddingHorizontal: 24,
    justifyContent: 'center',
  },
  registerLabel: {
    fontFamily: 'iCielHelveticaNowText-Medium',
    fontSize: verticalScale(36),
    lineHeight: verticalScale(53),
    alignSelf: 'center',
    fontWeight: '600',
    color: Colors.LIGHT,
    textTransform: 'capitalize',
  },
  continueBtn: {
    marginTop: verticalScale(30),
  },
  haveAnAccount: {
    fontSize: 16,
    color: Colors.PRIMARY_TEXT,
    marginRight: 5,
    lineHeight: 19,
  },
  loginNow: {
    fontSize: 16,
    lineHeight: 19,
    textDecorationLine: 'underline',
  },
  bottomText: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginBottom: verticalScale(24),
    // position: 'absolute',
    // bottom: isIPhoneX() ? 0 : 10,
    // left: 0,
    // right: 0,
  },
  confirmTermAndPolicy: {
    fontSize: 12,
    color: Colors.INACTIVE_TINT,
    textAlign: 'center',
    paddingBottom: 10,
    paddingHorizontal: 24,
  },
  termAndPolicy: {
    fontSize: 13,
    color: Colors.LIGHT,
    textDecorationLine: 'underline',
  },
});

import {connect} from 'react-redux';
import {bindActionCreators} from 'redux';

import Register from 'app/screens/register/register';

import {register} from 'app/actions/user';

const mapStateToProps = state => {
  const isLoading = state.request.register.loading;
  const error = state.request.register.error;
  return {
    isLoading,
    error,
  };
};
const mapDispatchToProps = dispatch => {
  return {
    actions: bindActionCreators(
      {
        register: register,
      },
      dispatch,
    ),
  };
};
export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(Register);

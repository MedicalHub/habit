import keyMirror from 'app/utils/key_mirror';

export default keyMirror({
  //GET DETAIL TOPIC
  GET_TOPIC_DETAIL_REQUEST: null,
  GET_TOPIC_DETAIL_SUCCESS: null,
  GET_TOPIC_DETAIL_FAILURE: null,

  //GET TOPIC ITEM
  GET_TOPIC_REQUEST: null,
  GET_TOPIC_SUCCESS: null,
  GET_TOPIC_FAILURE: null,
  RESET_TOPIC: null,

  //GET ALL TOPIC ITEM
  GET_ALL_TOPIC_ITEM_REQUEST: null,
  GET_ALL_TOPIC_ITEM_SUCCESS: null,
  GET_ALL_TOPIC_ITEM_FAILURE: null,
});

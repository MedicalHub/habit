import keyMirror from 'app/utils/key_mirror';

export default keyMirror({
  WEBSOCKET_REQUEST: null,
  WEBSOCKET_SUCCESS: null,
  WEBSOCKET_FAILURE: null,
  WEBSOCKET_CLOSED: null,

  STORAGE_DEVICE_TOKEN: null,
});

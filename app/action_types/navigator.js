import keyMirror from 'app/utils/key_mirror';

export default keyMirror({
  NAVIGATOR: null,
});

import keyMirror from 'app/utils/key_mirror';

export default keyMirror({
  //GET ITEM OF TOPIC ITEM
  GET_ITEM_TOPIC_REQUEST: null,
  GET_ITEM_TOPIC_SUCCESS: null,
  GET_ITEM_TOPIC_FAILURE: null,
  RESET_ITEM_TOPIC: null,
});

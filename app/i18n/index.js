/*************************
 * Copyright (c) 2019-present thientran2910. All Rights Reserved
 *************************/
import 'intl';
import 'date-time-format-timezone';

import en from 'assets/i18n/en.json';
import vi from 'assets/i18n/vi.json';

const TRANSLATIONS = {en};

export const DEFAULT_LOCALE = 'en';

export const languages = {
  vi: {
    value: 'vi',
    name: 'Việt Nam',
    url: vi,
  },
  en: {
    value: 'en',
    name: 'English',
    url: en,
  },
};

function loadTranslation(locale) {
  try {
    switch (locale) {
      case 'vi':
        TRANSLATIONS[locale] = require('assets/i18n/vi.json');
        if (!Intl.RelativeTimeFormat) {
          require('@formatjs/intl-relativetimeformat/polyfill');
          require('@formatjs/intl-relativetimeformat/dist/locale-data/vi'); // Add locale data for de
        }

        break;
      case 'en':
        TRANSLATIONS[locale] = require('assets/i18n/en.json');
        if (!Intl.RelativeTimeFormat) {
          require('@formatjs/intl-relativetimeformat/polyfill');
          require('@formatjs/intl-relativetimeformat/dist/locale-data/en'); // Add locale data for de
        }
        break;
    }
  } catch (e) {
    console.error('NO Translation found', e); //eslint-disable-line no-console
  }
}

export function getTranslations(locale) {
  if (!TRANSLATIONS[locale]) {
    loadTranslation(locale);
  }
  return TRANSLATIONS[locale] || TRANSLATIONS[DEFAULT_LOCALE];
}

export function getLocalizedMessage(locale, id) {
  const translations = getTranslations(locale);

  return translations[id] || TRANSLATIONS[DEFAULT_LOCALE][id];
}

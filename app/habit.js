import React from 'react';

import {Provider} from 'react-redux';

import Main from 'app/main';

import {store} from 'app/store';

class Habit extends React.PureComponent<Props> {
  render() {
    return (
      <Provider store={store}>
        <Main />
      </Provider>
    );
  }
}

export default Habit;

import {CategoryTypes} from 'app/action_types';

export const getAllCategory = userId => {
  return {
    type: CategoryTypes.GET_ALL_CATEGORY_REQUEST,
    userId,
  };
};

import {UserTypes} from 'app/action_types';

export const login = data => {
  return {
    type: UserTypes.LOGIN_REQUEST,
    data,
  };
};

export const loginWithSocial = data => {
  return {
    type: UserTypes.LOGIN_SOCIAL_REQUEST,
    data,
  };
};

export const register = data => {
  return {
    type: UserTypes.REGISTER_REQUEST,
    data,
  };
};

export const getProfileInfo = userId => {
  return {
    type: UserTypes.GET_PROFILE_INFO_REQUEST,
    userId: userId,
  };
};

export const updateProfile = data => {
  return {
    type: UserTypes.UPDATE_PROFILE_REQUEST,
    data,
  };
};

export const updatePassword = data => {
  return {
    type: UserTypes.UPDATE_PASSWORD_REQUEST,
    data,
  };
};

export const logout = () => {
  return {
    type: UserTypes.LOGOUT_SUCCESS,
  };
};

export const updateAvatar = data => {
  return {
    type: UserTypes.UPDATE_AVATAR_SUCCESS,
    data,
  };
};

export const resetPassword = data => {
  return {
    type: UserTypes.RESET_PASSWORD_REQUEST,
    data,
  };
};

export const getStatistics = userId => {
  return {
    type: UserTypes.GET_STATISTICS_REQUEST,
    userId,
  };
};

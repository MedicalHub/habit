import {ItemTopicTypes} from 'app/action_types';

export const getItemTopic = (topicId, userId) => {
  return {
    type: ItemTopicTypes.GET_ITEM_TOPIC_REQUEST,
    topicId,
    userId,
  };
};
export const resetItemTopic = () => {
  return {
    type: ItemTopicTypes.RESET_ITEM_TOPIC,
  };
};

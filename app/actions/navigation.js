import {NavigatorTypes} from 'app/action_types';

export const determineNavigator = (prevScreen, currentScreen, action) => {
  return {
    type: NavigatorTypes.NAVIGATOR,
    prevScreen,
    currentScreen,
    action,
  };
};

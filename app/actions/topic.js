import {TopicTypes} from 'app/action_types';

export const getTopicByCategory = categoryId => {
  return {
    type: TopicTypes.GET_TOPIC_REQUEST,
    categoryId,
  };
};

export const resetTopic = () => {
  return {
    type: TopicTypes.RESET_TOPIC,
  };
};

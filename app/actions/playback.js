import TrackPlayer from 'react-native-track-player';
import reactotron from 'reactotron-react-native';
// import errorReporter from '../utils/ErrorReporter';

export const setCurrentTrack = currentTrack => async dispatch => {
  try {
    dispatch({type: 'current_track', payload: currentTrack});
    reactotron.log('currentTrack', currentTrack);
    await TrackPlayer.pause();
    await TrackPlayer.skip(currentTrack.id);
    await TrackPlayer.play();
    dispatch({type: 'set_playback', payload: true});
  } catch (e) {
    console.log('e', e, currentTrack);
    // errorReporter(e);
  }
};

export const _setCurrentTrack = currentTrack => async dispatch => {
  //set redux
  dispatch({type: 'current_track', payload: currentTrack});
};

export const setPlayback = isPlaying => {
  isPlaying ? TrackPlayer.play() : TrackPlayer.pause();
  return {type: 'set_playback', payload: isPlaying};
};

export const setLoop = isLoop => {
  return {type: 'set_loop', payload: isLoop};
};

export const setShuffle = isShuffle => {
  return {type: 'set_shuffle', payload: isShuffle};
};

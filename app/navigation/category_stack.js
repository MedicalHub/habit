import {createStackNavigator} from 'react-navigation-stack';
import {defaultNavigationOptions} from 'app/navigation/config';
import {Colors} from 'app/constants';
import Category from 'app/screens/category';

export default createStackNavigator(
  {
    Category: {
      screen: Category,
    },
  },
  {
    initialRouteName: 'Category',
    defaultNavigationOptions: {
      ...defaultNavigationOptions,
      headerStyle: {
        backgroundColor: Colors.PRIMARY_1,
        borderBottomWidth: 0,
        elevation: 0,
        shadowColor: 'transparent',
      },
      headerTitleStyle: {
        marginLeft: 24,
        fontSize: 22,
        color: Colors.LIGHT,
      },
      headerTitleContainerStyle: {
        justifyContent: 'center',
        alignItems: 'flex-start',
        width: '100%',
      },
    },
  },
);

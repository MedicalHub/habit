import {createAppContainer, createSwitchNavigator} from 'react-navigation';

import AuthLoading from 'app/screens/auth_loading';
import LoginStack from 'app/navigation/login_stack';
import SplashIntroStack from 'app/navigation/splash_intro_stack';
import RootStack from 'app/navigation/modal_root_stack';

export default createAppContainer(
  createSwitchNavigator(
    {
      AuthLoading,
      AppMain: RootStack,
      Login: LoginStack,
      SplashIntro: SplashIntroStack,
    },
    {
      initialRouteName: 'AuthLoading',
    },
  ),
);

import {createStackNavigator} from 'react-navigation-stack';

import {defaultNavigationOptions} from 'app/navigation/config';

import SplashIntro from 'app/screens/splash_intro';
import {Colors} from 'app/constants';

const SplashIntroStack = createStackNavigator(
  {
    SplashIntro: {
      screen: SplashIntro,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'title.splashIntro',
            defaultMessage: 'Giới thiệu',
          }),
          headerStyle: {
            ...defaultNavigationOptions.headerStyle,
            backgroundColor: Colors.PRIMARY_1,
          },
        };
      },
    },
  },
  {
    initialRouteName: 'SplashIntro',
    defaultNavigationOptions,
  },
);
export default SplashIntroStack;

import {createStackNavigator} from 'react-navigation-stack';
import Profile from 'app/screens/profile';
import {defaultNavigationOptions} from 'app/navigation/config';
import {Colors} from 'app/constants';

const ProfileStack = createStackNavigator(
  {
    Profile: {
      screen: Profile,
      navigationOptions: ({screenProps}) => {
        const {intl} = screenProps;
        return {
          title: intl.formatMessage({
            id: 'tabBar.profile',
            defaultMessage: 'Tài khoản',
          }),
        };
      },
    },
  },
  {
    initialRouteName: 'Profile',
    defaultNavigationOptions: {
      ...defaultNavigationOptions,
      headerStyle: {
        backgroundColor: Colors.PRIMARY_1,
        borderBottomWidth: 0,
        elevation: 0,
        shadowColor: 'transparent',
      },
    },
  },
);

export default ProfileStack;

import {createStackNavigator} from 'react-navigation-stack';

import {defaultNavigationOptions} from 'app/navigation/config';
import {Colors} from 'app/constants';

import Register from 'app/screens/register';
import Login from 'app/screens/login';
import PrivacyPolicy from 'app/screens/privacy_policy';
import TermOfUse from 'app/screens/term_of_use';
import ForgotPassword from 'app/screens/forgot_password';
import VerifyOtp from 'app/screens/verify_otp';
import ResetPassword from 'app/screens/reset_password';

const LoginStack = createStackNavigator(
  {
    Login: {
      screen: Login,
      navigationOptions: {
        headerShown: false,
      },
    },
    Register: {
      screen: Register,
      navigationOptions: ({screenProps}) => {
        return {
          // headerTitle: screenProps.intl.formatMessage({
          //   id: 'title.register',
          //   defaultMessage: 'Đăng ký',
          // }),
          headerTitle: '',
          headerBackTitleVisible: false,
          headerStyle: {
            shadowColor: 'transparent',
            borderBottomWidth: 0,
            backgroundColor: Colors.PRIMARY_1,
            elevation: 0,
          },
        };
      },
    },
    ForgotPassword: {
      screen: ForgotPassword,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'forgotPassword',
            defaultMessage: 'Quên mật khẩu',
          }),
          headerBackTitleVisible: false,
          headerStyle: {
            shadowColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            backgroundColor: Colors.PRIMARY_1,
          },
        };
      },
    },
    VerifyOtp: {
      screen: VerifyOtp,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'verifyOtp',
            defaultMessage: 'Xác minh mã OTP',
          }),
          gestureEnabled: false,
          headerBackTitleVisible: false,
          headerStyle: {
            shadowColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            backgroundColor: Colors.PRIMARY_1,
          },
        };
      },
    },
    ResetPassword: {
      screen: ResetPassword,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'resetPassword',
            defaultMessage: 'Đặt lại mật khẩu',
          }),
          headerLeft: () => {
            return null;
          },
          gestureEnabled: false,
          headerBackTitleVisible: false,
          headerStyle: {
            shadowColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            backgroundColor: Colors.PRIMARY_1,
          },
        };
      },
    },
    TermOfUse: {
      screen: TermOfUse,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'title.termOfUse',
            defaultMessage: 'Điều khoản sử dụng',
          }),
          headerBackTitleVisible: false,
          headerStyle: {
            shadowColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            backgroundColor: Colors.PRIMARY_1,
          },
        };
      },
    },
    PrivacyPolicy: {
      screen: PrivacyPolicy,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'title.privacyPolicy',
            defaultMessage: 'Chính sách bảo mật',
          }),
          headerBackTitleVisible: false,
          headerStyle: {
            shadowColor: 'transparent',
            borderBottomWidth: 0,
            elevation: 0,
            backgroundColor: Colors.PRIMARY_1,
          },
        };
      },
    },
  },
  {
    initialRouteName: 'Login',
    defaultNavigationOptions: defaultNavigationOptions,
  },
);
export default LoginStack;

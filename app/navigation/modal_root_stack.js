import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';

// Screen | Stack

import AppMain from 'app/navigation/app_main';
import ActiveCodeStack from 'app/navigation/active_code_stack';

// config
import {defaultNavigationOptions} from 'app/navigation/config';
import HeaderLeft from 'app/components/header_left';
import {Colors} from 'app/constants';
import OrderStack from './order_stack';

const RootStack = createStackNavigator(
  {
    AppMain: {
      screen: AppMain,
      navigationOptions: {
        headerShown: false,
      },
    },
    ActiveCode: {
      screen: ActiveCodeStack,
      navigationOptions: ({screenProps}) => {
        return {
          headerTransparent: true,
          headerTitle: '',
          headerBackImage: () => (
            <HeaderLeft
              color={Colors.LIGHT}
              source={require('assets/images/exit.png')}
            />
          ),
        };
      },
    },
    OrderStack: {
      screen: OrderStack,
      navigationOptions: ({screenProps}) => {
        return {
          headerTransparent: true,
          headerTitle: '',
          gestureEnabled: false,
          headerBackImage: () => (
            <HeaderLeft
              color={Colors.PRIMARY_TEXT}
              source={require('assets/images/exit.png')}
            />
          ),
        };
      },
    },
  },

  {
    initialRouteName: 'AppMain',
    mode: 'modal',
    defaultNavigationOptions: {
      ...defaultNavigationOptions,
      headerStyle: {
        ...defaultNavigationOptions.headerStyle,
      },
    },
  },
);

export default RootStack;

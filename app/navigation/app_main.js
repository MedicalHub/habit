import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';

// screen | stack
import AppTab from 'app/navigation/app_tab';
import ProfileInfo from 'app/screens/profile_info';
import EditProfileInfo from 'app/screens/edit_profile_info';
import ChangePassword from 'app/screens/change_password';
import TransactionHistory from 'app/screens/transaction_history';

// config
import {Colors} from 'app/constants';
import {defaultNavigationOptions} from 'app/navigation/config';
import DetailCategory from 'app/screens/detail_category';
import SearchCategory from 'app/screens/search_category';
import DetailHabit from 'app/screens/detail_habit';
import Audio from 'app/screens/audio';
import {
  CongratulationDay,
  CongratulationMonth,
} from 'app/screens/congratulation';
import OverView from 'app/screens/over_view';
import SearchItemTopic from 'app/screens/search_item_topic';

const AppMain = createStackNavigator(
  {
    AppTab: {
      screen: AppTab,
      navigationOptions: {
        headerShown: false,
      },
    },
    ProfileInfo: {
      screen: ProfileInfo,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'profileInfo',
            defaultMessage: 'Thông tin cá nhân',
          }),
        };
      },
    },
    EditProfileInfo: {
      screen: EditProfileInfo,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'editProfileInfo',
            defaultMessage: 'Chỉnh sửa thông tin',
          }),
        };
      },
    },
    ChangePassword: {
      screen: ChangePassword,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'changePassword',
            defaultMessage: 'Đổi mật khẩu',
          }),
        };
      },
    },
    TransactionHistory: {
      screen: TransactionHistory,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'transactionHistory',
            defaultMessage: 'Lịch sử giao dịch',
          }),
        };
      },
    },
    DetailCategory: {
      screen: DetailCategory,
    },
    SearchCategory: {
      screen: SearchCategory,
      navigationOptions: {
        headerShown: false,
      },
    },
    SearchItemTopic: {
      screen: SearchItemTopic,
      navigationOptions: {
        headerShown: false,
      },
    },
    DetailHabit: {
      screen: DetailHabit,
      navigationOptions: {
        headerShown: false,
      },
    },
    Audio: {
      screen: Audio,
      navigationOptions: {
        headerShown: false,
        gestureEnabled: false,
      },
    },
    CongratulationDay: {
      screen: CongratulationDay,
      navigationOptions: {
        headerShown: false,
      },
    },
    CongratulationMonth: {
      screen: CongratulationMonth,
      navigationOptions: {
        headerShown: false,
      },
    },
    OverView: {
      screen: OverView,
      navigationOptions: {
        headerShown: false,
      },
    },
  },
  {
    initialRouteName: 'AppTab',
    defaultNavigationOptions: {
      ...defaultNavigationOptions,
      headerStyle: {
        ...defaultNavigationOptions.headerStyle,
        backgroundColor: Colors.PRIMARY_2,
      },
    },
  },
);

export default AppMain;

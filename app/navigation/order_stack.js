import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from 'react-navigation-stack';

// Screen
import AccountExpired from 'app/screens/account_expired';
import OrderInfo from 'app/screens/order_info';
import ActiveSuccess from 'app/screens/active_success';
import HeaderLeft from 'app/components/header_left';

import {Colors} from 'app/constants';
import {defaultNavigationOptions} from './config';

const OrderStack = createStackNavigator(
  {
    AccountExpired: {
      screen: AccountExpired,
      navigationOptions: ({screenProps, navigation}) => {
        return {
          headerTitle: '',
          headerTransparent: true,
          gestureEnabled: false,
          cardStyleInterpolator: CardStyleInterpolators.forNoAnimation,
        };
      },
    },
    OrderInfo: {
      screen: OrderInfo,
      navigationOptions: ({screenProps, navigation}) => {
        return {
          headerTitle: screenProps.intl.formatMessage({
            id: 'orderHeader',
            defaultMessage: 'Đặt hàng',
          }),
          headerStyle: {
            ...defaultNavigationOptions.headerStyle,
            backgroundColor: Colors.PRIMARY_1,
          },
          gestureEnabled: false,
          cardStyleInterpolator: CardStyleInterpolators.forNoAnimation,
          headerBackImage: () => (
            <HeaderLeft
              color={Colors.PRIMARY_TEXT}
              source={require('assets/images/exit.png')}
            />
          ),
        };
      },
    },
    OrderSuccess: {
      screen: ActiveSuccess,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: '',
          headerTransparent: true,
          gestureEnabled: false,
          cardStyleInterpolator: CardStyleInterpolators.forNoAnimation,
          headerBackImage: () => (
            <HeaderLeft
              color={Colors.PRIMARY_TEXT}
              source={require('assets/images/exit.png')}
            />
          ),
        };
      },
    },
  },
  {
    initialRouteName: 'AccountExpired',
    defaultNavigationOptions: {
      ...defaultNavigationOptions,
      headerStyle: {
        ...defaultNavigationOptions.headerStyle,
        backgroundColor: Colors.PRIMARY_1,
      },
      // headerTransparent: true,
    },
  },
);

export default OrderStack;

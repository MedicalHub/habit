/* eslint-disable react-native/no-inline-styles */
import React from 'react';
import {Platform, View} from 'react-native';
import {HeaderStyleInterpolators} from 'react-navigation-stack';

import Icon from 'app/components/icons';
import HeaderLeft from 'app/components/header_left';

import {Colors} from 'app/constants';

const headerTitleContainerStyle = {
  justifyContent: 'center',
  alignItems: 'center',
  width: '100%',
};

if (Platform.OS === 'android') {
  headerTitleContainerStyle.left = 0;
}

const headerStyle = {
  backgroundColor: Colors.PRIMARY_2,
  borderBottomWidth: 0,
  elevation: 0,
  shadowColor: 'rgba(0,0,0,0)',
};

const headerTitleStyle = {
  color: Colors.LIGHT,
  fontFamily: 'iCielHelveticaNowText-Medium',
  fontSize: 18,
  textAlign: 'center',
};

export const BackButton = props => {
  return (
    <View
      style={{
        height: Platform.OS === 'ios' ? '100%' : 30,
        width: Platform.OS === 'ios' ? 56 : 30,
        justifyContent: 'center',
        alignItems: 'center',
      }}>
      <Icon
        type={props.type || 'System'}
        name={props.name || 'left'}
        size={24}
        color={props.color || Colors.INACTIVE_TINT}
      />
    </View>
  );
};

const defaultNavigationOptions = {
  // gesturesEnabled: false,
  headerTruncatedBackTitle: '',
  headerBackTitleVisible: false,
  headerBackAllowFontScaling: true,
  headerTitleAllowFontScaling: true,
  headerBackTitle: 'Quay lại',
  headerBackTitleStyle: {
    color: Colors.INACTIVE_TINT,
  },
  headerTintColor: Colors.INACTIVE_TINT,
  headerTitleStyle,
  headerTitleContainerStyle,
  headerStyle,
};

if (Platform.OS === 'ios') {
  defaultNavigationOptions.headerStyleInterpolator =
    HeaderStyleInterpolators.forUIKit;
  defaultNavigationOptions.headerBackImage = () => <HeaderLeft />;
}
if (Platform.OS === 'android') {
  defaultNavigationOptions.cardStyle = {
    backgroundColor: Colors.BACKGROUND_PRIMARY,
  };
}

export {defaultNavigationOptions};

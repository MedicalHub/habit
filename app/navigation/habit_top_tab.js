// eslint-disable-next-line no-unused-vars
import React from 'react';
import {createMaterialTopTabNavigator} from 'react-navigation-tabs';
import HabitPropose from 'app/screens/habit_propose';
import HabitMine from 'app/screens/habit_mine';
import {Colors} from 'app/constants';
// import {createAppContainer} from 'react-navigation';

const HabitTopTab = createMaterialTopTabNavigator(
  {
    HabitPropose: {
      screen: HabitPropose,
      navigationOptions: () => {
        return {
          title: 'ĐỀ XUẤT',
        };
      },
    },
    HabitMine: {
      screen: HabitMine,
      navigationOptions: () => {
        return {
          title: 'CỦA BẠN',
        };
      },
    },
  },
  {
    tabBarOptions: {
      activeTintColor: Colors.ACTIVE_TINT,
      inactiveTintColor: Colors.INACTIVE_TINT,
      indicatorStyle: {
        backgroundColor: Colors.ACTIVE_TINT,
        height: 2,
      },
      style: {
        backgroundColor: Colors.PRIMARY_2,
      },
    },
    lazy: true,
  },
);

export default HabitTopTab;

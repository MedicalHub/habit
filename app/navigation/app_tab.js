import React from 'react';
import {createBottomTabNavigator} from 'react-navigation-tabs';

import Category from 'app/navigation/category_stack';
import MedalStack from 'app/navigation/medal_stack';
import ProfileStack from 'app/navigation/profile_stack';

import AppIcon from 'app/components/app_icon';

import {isIPhoneX} from 'app/utils/screen';
import {Colors} from 'app/constants';
import HabitStack from 'app/navigation/habit_stack';

const AppTab = createBottomTabNavigator(
  {
    HabitStack: {
      screen: HabitStack,
      navigationOptions: ({screenProps}) => {
        const {intl} = screenProps;
        return {
          title: intl.formatMessage({
            id: 'tabBar.home',
            defaultMessage: 'Thói quen',
          }),
          tabBarIcon: ({tintColor}) => {
            return (
              <AppIcon
                source={require('assets/images/habit.png')}
                color={tintColor}
              />
            );
          },
        };
      },
    },
    Category: {
      screen: Category,
      navigationOptions: ({screenProps}) => {
        const {intl} = screenProps;
        return {
          title: intl.formatMessage({
            id: 'tabBar.category',
            defaultMessage: 'Danh mục',
          }),
          tabBarIcon: ({tintColor}) => {
            return (
              <AppIcon
                source={require('assets/images/category.png')}
                color={tintColor}
              />
            );
          },
        };
      },
    },
    Medal: {
      screen: MedalStack,
      navigationOptions: ({screenProps}) => {
        const {intl} = screenProps;
        return {
          title: intl.formatMessage({
            id: 'tabBar.medal',
            defaultMessage: 'Huy chương',
          }),
          tabBarIcon: ({tintColor}) => {
            return (
              <AppIcon
                source={require('assets/images/medal.png')}
                color={tintColor}
              />
            );
          },
        };
      },
    },
    Profile: {
      screen: ProfileStack,
      navigationOptions: ({screenProps}) => {
        const {intl} = screenProps;
        return {
          title: intl.formatMessage({
            id: 'tabBar.profile',
            defaultMessage: 'Tài khoản',
          }),
          tabBarIcon: ({tintColor}) => {
            return (
              <AppIcon
                source={require('assets/images/user.png')}
                color={tintColor}
              />
            );
          },
        };
      },
    },
  },
  {
    // initialRouteName: 'Profile',
    initialRouteName: 'HabitStack',
    tabBarOptions: {
      inactiveTintColor: Colors.INACTIVE_TINT,
      activeTintColor: Colors.ACTIVE_TINT,
      showLabel: true,
      style: {
        backgroundColor: Colors.PRIMARY_2,
        borderTopWidth: 0,
        shadowColor: 'rgba(2,3,9,0.08)',
        shadowOffset: {
          width: 0,
          height: 0,
        },
        shadowOpacity: 1,
        shadowRadius: 5,
        elevation: 30,
        height: isIPhoneX() ? 56 : 50,
      },
      allowFontScaling: false,
    },
  },
);

export default AppTab;

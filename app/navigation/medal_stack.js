import {createStackNavigator} from 'react-navigation-stack';

import Medal from 'app/screens/medal';

import {defaultNavigationOptions} from './config';

const MedalStack = createStackNavigator(
  {
    Medal: {
      screen: Medal,
      navigationOptions: ({screenProps, navigation}) => {
        const {intl} = screenProps;
        return {
          title: intl.formatMessage({
            id: 'tabBar.medal',
            defaultMessage: 'Huy chương',
          }),
        };
      },
    },
  },
  {
    initialRouteName: 'Medal',
    defaultNavigationOptions: defaultNavigationOptions,
  },
);

export default MedalStack;

import React from 'react';
import {
  createStackNavigator,
  CardStyleInterpolators,
} from 'react-navigation-stack';

// Screen
import ScanQRCode from 'app/screens/scan_qr_code';
import InputActiveCode from 'app/screens/input_active_code';
import ActiveSuccess from 'app/screens/active_success';
import HeaderLeft from 'app/components/header_left';
import {Colors} from 'app/constants';

const ActiveCodeStack = createStackNavigator(
  {
    ScanQRCode: {
      screen: ScanQRCode,
      navigationOptions: ({screenProps, navigation}) => {
        return {
          headerTitle: '',
          headerTransparent: true,
          gestureEnabled: false,
          cardStyleInterpolator: CardStyleInterpolators.forNoAnimation,
        };
      },
    },
    InputActiveCode: {
      screen: InputActiveCode,
      navigationOptions: ({screenProps, navigation}) => {
        return {
          headerTitle: '',
          // headerTransparent: true,
          gestureEnabled: false,
          cardStyleInterpolator: CardStyleInterpolators.forNoAnimation,
          headerBackImage: () => (
            <HeaderLeft
              color={Colors.PRIMARY_TEXT}
              source={require('assets/images/exit.png')}
            />
          ),
        };
      },
    },
    ActiveSuccess: {
      screen: ActiveSuccess,
      navigationOptions: ({screenProps}) => {
        return {
          headerTitle: '',
          headerTransparent: true,
          gestureEnabled: false,
          cardStyleInterpolator: CardStyleInterpolators.forNoAnimation,
        };
      },
    },
  },
  {
    initialRouteName: 'ScanQRCode',
    defaultNavigationOptions: {
      headerTransparent: true,
    },
  },
);

export default ActiveCodeStack;

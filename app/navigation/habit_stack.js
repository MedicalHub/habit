import React from 'react';
import {createStackNavigator} from 'react-navigation-stack';
import {Colors} from 'app/constants';
import {defaultNavigationOptions} from './config';
import HabitTopTab from 'app/navigation/habit_top_tab';
import HeaderRight from 'app/components/header_right';

const HabitStack = createStackNavigator(
  {
    Habit: {
      screen: HabitTopTab,
      navigationOptions: ({navigation, screenProps}) => {
        const {intl} = screenProps;
        const headerRight = () => (
          <HeaderRight
            onPress={() => {
              navigation.navigate('SearchItemTopic');
            }}
            color={Colors.PRIMARY_TEXT}
            source={require('assets/images/search.png')}
          />
        );
        return {
          headerRight,
          title: intl.formatMessage({
            id: 'tabBar.home',
            defaultMessage: 'Thói quen',
          }),
        };
      },
    },
  },
  {
    initialRouteName: 'Habit',
    defaultNavigationOptions: defaultNavigationOptions,
  },
);

export default HabitStack;

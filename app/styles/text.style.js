import {StyleSheet} from 'react-native';
import {Colors} from 'app/constants';

const textStyle = StyleSheet.create({
  section: {
    fontSize: 14,
    fontFamily: 'iCielHelveticaNowText-Bold',
    color: Colors.PRIMARY_TEXT,
  },
});
export default textStyle;

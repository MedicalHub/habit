export default {
  AUTHORIZED: 'authorized',
  DENIED: 'denied',
  UNDETERMINED: 'undetermined',
};

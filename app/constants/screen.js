import {Dimensions, StatusBar, Platform} from 'react-native';
import {isIPhoneX} from 'app/utils/screen';

let STATUS_BAR_HEIGHT = isIPhoneX() ? 44 : 20;

if (Platform.OS === 'android') {
  STATUS_BAR_HEIGHT = StatusBar.currentHeight;
}

export default {
  ANDROID_TOP_LANDSCAPE: 46,
  ANDROID_TOP_PORTRAIT: 56,
  IOS_TOP_LANDSCAPE: 32,
  IOS_TOP_PORTRAIT: isIPhoneX() ? 88 : 64,
  STATUS_BAR_HEIGHT: STATUS_BAR_HEIGHT,
  SCREEN_WIDTH: Dimensions.get('window').width,
  SCREEN_HEIGHT: Dimensions.get('window').height,
  FONT_SCALE: Dimensions.get('window').fontScale,
  PADDING_BOTTOM: isIPhoneX() ? 34 : 16,
  I_PHONE_X_INDICATOR_HEIGHT: isIPhoneX() ? 34 : 0,
  TAB_BAR_HEIGHT: isIPhoneX() ? 56 : 50,
  TOP_TAB_HEIGHT: 44,
  ABSOULUTE_BUTTON_PADDING: isIPhoneX() ? 64 + 34 + 16 : 64 + 16,
};

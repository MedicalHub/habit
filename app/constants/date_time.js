const DateTime = {
  DAY_0: 'CN',
  DAY_1: 'T.Hai',
  DAY_2: 'T.Ba',
  DAY_3: 'T.Tư',
  DAY_4: 'T.Năm',
  DAY_5: 'T.Sáu',
  DAY_6: 'T.Bảy',
};

export default DateTime;

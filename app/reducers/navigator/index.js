// import {UserTypes} from 'app/action_types';
// import NavigationService from 'app/navigation/NavigationService';

import {NavigatorTypes, UserTypes} from 'app/action_types';

const initState = {
  prevScreen: '',
  currentScreen: '',
  action: '',
};

export default function(state = initState, action) {
  switch (action.type) {
    case NavigatorTypes.NAVIGATOR: {
      return {
        prevScreen: action.prevScreen,
        currentScreen: action.currentScreen,
        action: action.action,
      };
    }
    case UserTypes.LOGOUT_SUCCESS:
      return initState;
  }

  return state;
}

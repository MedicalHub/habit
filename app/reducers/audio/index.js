import {combineReducers} from 'redux';

import blacklistedPlayback from './blacklistedPlayback';
import playback from './playback';

export default combineReducers({
  playback,
  blacklistedPlayback,
});

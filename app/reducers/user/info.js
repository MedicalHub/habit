import {UserTypes} from 'app/action_types';

export default function info(state = {}, action) {
  switch (action.type) {
    case UserTypes.LOGIN_SUCCESS:
      return action.data;
    case UserTypes.LOGIN_SOCIAL_SUCCESS:
      return action.data;
    case UserTypes.REGISTER_SUCCESS:
      return action.data;
    case UserTypes.GET_PROFILE_INFO_SUCCESS: {
      return {
        ...state,
        ...action.data,
      };
    }
    case UserTypes.RESET_PASSWORD_SUCCESS: {
      return action.data;
    }
    case UserTypes.UPDATE_PROFILE_SUCCESS: {
      return {
        ...state,
        ...action.data,
      };
    }
    case UserTypes.UPDATE_AVATAR_SUCCESS: {
      return {
        ...state,
        avatar: action.data,
      };
    }
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

import {combineReducers} from 'redux';

import info from './info';
import isLogin from './login';
import statistics from './statistics';

export default combineReducers({
  info,
  isLogin,
  statistics,
});

import {UserTypes} from 'app/action_types';

export default function statistics(state = {}, action) {
  switch (action.type) {
    case UserTypes.GET_STATISTICS_SUCCESS:
      return action.data;
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

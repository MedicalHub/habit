import {UserTypes} from 'app/action_types';

export default function isLogin(state = false, action) {
  switch (action.type) {
    case UserTypes.LOGIN_SUCCESS:
      return true;
    case UserTypes.REGISTER_SUCCESS:
      return true;
    case UserTypes.LOGIN_SOCIAL_SUCCESS:
      return true;
    case UserTypes.RESET_PASSWORD_SUCCESS:
      return true;
    case UserTypes.LOGOUT_SUCCESS:
      return false;
  }
  return state;
}

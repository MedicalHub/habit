import {CategoryTypes} from 'app/action_types';

const initState = {};

export default function(state = initState, action) {
  switch (action.type) {
    case CategoryTypes.GET_ALL_CATEGORY_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case CategoryTypes.GET_ALL_CATEGORY_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };
    case CategoryTypes.GET_ALL_CATEGORY_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    default:
      return state;
  }
}

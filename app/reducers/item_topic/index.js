import {ItemTopicTypes} from 'app/action_types';

const initialState = {
  dataAudio: [],
};
export default function(state = initialState, action) {
  switch (action.type) {
    case ItemTopicTypes.GET_ITEM_TOPIC_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case ItemTopicTypes.GET_ITEM_TOPIC_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
        dataAudio: action.dataAudio,
      };
    case ItemTopicTypes.GET_ITEM_TOPIC_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case ItemTopicTypes.RESET_ITEM_TOPIC:
      return initialState;
    default:
      return state;
  }
}

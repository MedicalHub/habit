import {TopicTypes} from 'app/action_types';

const initialState = {};

export default function(state = initialState, action) {
  switch (action.type) {
    case TopicTypes.GET_TOPIC_REQUEST:
      return {
        ...state,
        loading: true,
      };
    case TopicTypes.GET_TOPIC_SUCCESS:
      return {
        ...state,
        loading: false,
        data: action.data,
      };
    case TopicTypes.GET_TOPIC_FAILURE:
      return {
        ...state,
        loading: false,
        error: action.error,
      };
    case TopicTypes.RESET_TOPIC:
      return initialState;
    default:
      return state;
  }
}

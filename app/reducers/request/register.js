import {UserTypes} from 'app/action_types';

export default function register(state = {}, action) {
  switch (action.type) {
    case UserTypes.REGISTER_REQUEST:
      return {
        loading: true,
      };
    case UserTypes.REGISTER_SUCCESS:
      return {
        loading: false,
        data: action.data,
      };
    case UserTypes.REGISTER_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

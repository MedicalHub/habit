import {combineReducers} from 'redux';

import login from './login';
import register from './register';
import updateProfile from './update_profile';
import updatePassword from './update_password';
import loginWithSocial from './login_with_social';
import getProfileInfo from './get_profile_info';
import resetPassword from './reset_password';

export default combineReducers({
  login,
  register,
  updateProfile,
  updatePassword,
  loginWithSocial,
  getProfileInfo,
  resetPassword,
});

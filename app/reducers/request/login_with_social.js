import {UserTypes} from 'app/action_types';

export default function loginWithSocial(state = {}, action) {
  switch (action.type) {
    case UserTypes.LOGIN_SOCIAL_REQUEST:
      return {
        loading: true,
      };
    case UserTypes.LOGIN_SOCIAL_SUCCESS:
      return {
        loading: false,
        data: action.data,
      };
    case UserTypes.LOGIN_SOCIAL_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

import {UserTypes} from 'app/action_types';

export default function login(state = {}, action) {
  switch (action.type) {
    case UserTypes.LOGIN_REQUEST:
      return {
        loading: true,
      };
    case UserTypes.LOGIN_SUCCESS:
      return {
        loading: false,
        data: action.data,
      };
    case UserTypes.LOGIN_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

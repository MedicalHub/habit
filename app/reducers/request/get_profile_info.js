import {UserTypes} from 'app/action_types';

export default function getProfileInfo(state = {}, action) {
  switch (action.type) {
    case UserTypes.GET_PROFILE_INFO_REQUEST:
      return {
        loading: true,
      };
    case UserTypes.GET_PROFILE_INFO_SUCCESS:
      return {
        loading: false,
        data: action.data,
      };
    case UserTypes.GET_PROFILE_INFO_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

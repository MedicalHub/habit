import {UserTypes} from 'app/action_types';

export default function resetPassword(state = {}, action) {
  switch (action.type) {
    case UserTypes.RESET_PASSWORD_REQUEST:
      return {
        loading: true,
      };
    case UserTypes.RESET_PASSWORD_SUCCESS:
      return {
        loading: false,
        data: action.data,
      };
    case UserTypes.RESET_PASSWORD_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

import {UserTypes} from 'app/action_types';

export default function updateProfile(state = {}, action) {
  switch (action.type) {
    case UserTypes.UPDATE_PROFILE_REQUEST:
      return {
        loading: true,
      };
    case UserTypes.UPDATE_PROFILE_SUCCESS:
      return {
        loading: false,
        data: action.data,
      };
    case UserTypes.UPDATE_PROFILE_FAILURE:
      return {
        loading: false,
        error: action.error,
      };
    case UserTypes.LOGOUT_SUCCESS:
      return {};
    default:
      return state;
  }
}

import {GeneralTypes} from 'app/action_types';

export default function deviceToken(state = '', action) {
  switch (action.type) {
    case GeneralTypes.STORAGE_DEVICE_TOKEN:
      return action.deviceToken;
  }
  return state;
}

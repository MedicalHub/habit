import {combineReducers} from 'redux';

import connection from './connection';
import websocket from './websocket';
import info from './info';
import deviceToken from './device_token';

export default combineReducers({
  deviceToken,
  info,
  connection,
  websocket,
});

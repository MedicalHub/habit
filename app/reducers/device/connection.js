import {UserTypes} from 'app/action_types';
import {DeviceTypes} from 'app/action_types';

export default function connection(state = true, action) {
  switch (action.type) {
    case DeviceTypes.CONNECTION_CHANGED:
      return action.data;

    case UserTypes.LOGOUT_SUCCESS:
      return state;
  }

  return state;
}

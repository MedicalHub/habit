import {combineReducers} from 'redux';

import app from './app';
import device from './device';
import navigator from './navigator';
import user from './user';
import request from './request';
import category from './category';
import topic from './topic';
import item_topic from './item_topic';
import audio from './audio';

export default combineReducers({
  app,
  device,
  navigator,
  user,
  request,
  category,
  topic,
  item_topic,
  audio,
});

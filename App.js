import React from 'react';
import {
  Text,
  StatusBar,
  Platform,
  UIManager,
} from 'react-native';

import {IntlProvider} from 'react-intl';
import {SafeAreaProvider} from 'react-native-safe-area-context';

import Habit from 'app/habit';

import Device from 'app/utils/device';
import {getTranslations} from 'app/i18n';
import {General} from 'app/constants';


// import Config from 'react-native-config';

// console.log('Config', Config);
if (
  Platform.OS === 'android' &&
  UIManager.setLayoutAnimationEnabledExperimental
) {
  UIManager.setLayoutAnimationEnabledExperimental(true);
}

class App extends React.PureComponent {
  render() {
    return (
      <SafeAreaProvider>
        <StatusBar
          translucent={true}
          backgroundColor={'transparent'}
          barStyle={'light-content'}
        />
        <IntlProvider
          locale={General.DEFAULT_LANGUAGE}
          timeZone={Device.timeZone}
          messages={getTranslations(General.DEFAULT_LANGUAGE)}
          textComponent={Text}>
          <Habit />
        </IntlProvider>
      </SafeAreaProvider>
    );
  }
}
export default App;
